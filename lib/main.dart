import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:viaje_seguro/providers/appData.dart';
import 'package:viaje_seguro/providers/cart_provider.dart';
import 'package:viaje_seguro/providers/user.dart';
import 'package:viaje_seguro/screen/historyScreen.dart';
import 'package:viaje_seguro/screen/login.dart';
import 'package:viaje_seguro/screen/profileScreen.dart';
import 'package:viaje_seguro/screen/ratingView.dart';
import 'package:viaje_seguro/screen/registration.dart';
import 'package:viaje_seguro/screen/rideRequestScreen.dart';
import 'package:viaje_seguro/screen/splash.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:viaje_seguro/services/auth_service.dart';

import 'Theme.dart'; // Importa esta línea

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await initializeDateFormatting('es'); // Inicializa localizaciones en español
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    const FontWeight _bold = FontWeight.w700;
    const FontWeight _semiBold = FontWeight.w600;
    const FontWeight _regular = FontWeight.w400;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>(
          create: (context) => UserProvider(),
        ),
        ChangeNotifierProvider<AppData>(
          create: (context) => AppData(
            Provider.of<UserProvider>(context, listen: false), // Obtener la instancia de UserProvider
          ),
        ),
        StreamProvider(
          create: (context) => context.read<AuthService>().myUser, initialData: null,
        ),
        ChangeNotifierProvider(
          create: (BuildContext context) => CartProvider(),
        ),


      ],
      child: MaterialApp(
        title: 'Everything',
        // theme: ThemeData(
        //   primarySwatch: Colors.yellow,
        //   visualDensity: VisualDensity.adaptivePlatformDensity,
        //   appBarTheme: AppBarTheme(
        //     iconTheme: const IconThemeData(
        //       color: Colors.black,  // Cambia esto al color que prefieras
        //     ), toolbarTextStyle: const TextTheme(
        //     titleLarge: TextStyle(
        //       color: Colors.black,  // Cambia esto al color que prefieras
        //     ),
        //   ).bodyMedium,
        //     titleTextStyle: const TextTheme(
        //
        //     titleLarge: TextStyle(
        //         color: Colors.black,
        //         fontSize: 20,
        //         fontWeight: FontWeight.w600// Cambia esto al color que prefieras
        //     ),
        //     titleMedium: TextStyle(
        //         fontSize: 18.0, fontWeight: _bold, color: Colors.black),
        //     headlineSmall: TextStyle(
        //         fontSize: 20.0, fontWeight: _semiBold, color: Colors.black),
        //     bodyLarge: TextStyle(
        //         fontSize: 16.0, fontWeight: _semiBold, color: Colors.black),
        //     bodyMedium: TextStyle(
        //         fontSize: 14.0, fontWeight: _regular, color: Colors.black),
        //     headlineMedium: TextStyle(
        //         fontSize: 16.0, fontWeight: _bold, color: Colors.black),
        //     displaySmall: TextStyle(
        //         fontSize: 12.0, fontWeight: _semiBold, color: Colors.black),
        //     displayMedium: TextStyle(
        //         fontSize: 18.0, fontWeight: _semiBold, color: Colors.black),
        //     displayLarge: TextStyle(
        //         fontSize: 14.0, fontWeight: _semiBold, color: Color(0xFF707070)),
        //     labelLarge: TextStyle(
        //         fontSize: 16.0, fontWeight: _regular, color: Colors.white),
        //   ).titleLarge,
        //
        //   ),
        // ),
        theme: basicTheme(),
        initialRoute: Splash.idScreen,
        routes: {
          RegistrationScreen.idScreen: (context) => RegistrationScreen(),
          LoginScreen.idScreen: (context) => LoginScreen(),
          Splash.idScreen: (context) => Splash(),
          RideRequestScreen.idScreen: (context) => RideRequestScreen(),
          HistoryScreen.idScreen: (context) => HistoryScreen(),
          RatingView.idScreen: (context) => RatingView(),
          ProfileScreen.idScreen: (context) => ProfileScreen(),
        },
        debugShowCheckedModeBanner: false,
        home: Consumer<UserProvider>(
          builder: (context, userProvider, _) {
            switch (userProvider.status) {
              case Status.Uninitialized:
                return Splash();
              case Status.Unauthenticated:
              case Status.Authenticating:
                return LoginScreen();
              case Status.Authenticated:
                return RideRequestScreen();
              default:
                return LoginScreen();
            }
          },
        ),
      ),
    );
  }
}
