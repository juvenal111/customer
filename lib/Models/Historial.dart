import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

class Historial {
  final String docId;
  final Map<String, dynamic> rideInfoMapCopy;
  StreamSubscription<DocumentSnapshot<Map<String, dynamic>>>? _listenerRide;

  Historial({required this.docId, required this.rideInfoMapCopy}) {
    // Obtener una copia de rideInfoMap
    final rideInfoMap = Map<String, dynamic>.from(rideInfoMapCopy['rideInfoMap']);
    rideInfoMap.remove('conductores');

    // Guardar la copia en la colección historial
    FirebaseFirestore.instance.collection('historial').doc(docId).set({
      'rideInfoMap': rideInfoMap, // Mantener los datos originales
      'status': 'aceptada',        // Agregar el campo 'status' con el valor 'aceptada'
    });
    // await historyRef.set({
    //   'rideInfoMap': rideInfoMap, // Mantener los datos originales
    //   'status': 'aceptada',        // Agregar el campo 'status' con el valor 'aceptada'
    // });

    // // Guardar la copia en la colección historial
    // FirebaseFirestore.instance.collection('historial').doc(docId).set(rideInfoMap).then((value) {
    //   print("Historial guardado con éxito!");
    //   // Iniciar un listener para el documento recién creado
    //   _listenerRide = FirebaseFirestore.instance.collection('historial').doc(docId).snapshots().listen((documentSnapshot) {
    //     // Aquí puedes manejar los cambios en el documento, por ejemplo, imprimir su contenido
    //     print('El documento con id ${documentSnapshot.id} tiene el contenido ${documentSnapshot.data()}');
    //   });
    // }).catchError((error) {
    //   print("Error al guardar historial: $error");
    // });
  }
  StreamSubscription<DocumentSnapshot<Map<String, dynamic>>>? get listenerRide => _listenerRide;

  static Future<DocumentSnapshot<Map<String, dynamic>>?> getHistorial(String docId) async {
    try {
      final snapshot = await FirebaseFirestore.instance.collection('historial').doc(docId).get();
      return snapshot;
    } catch (e) {
      print('Error al obtener el historial: $e');
      return null;
    }
  }
  void dispose() {
    _listenerRide?.cancel();
  }
}
