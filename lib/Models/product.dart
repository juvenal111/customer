import '../data_model/extras_item.dart';

class Product {
  final String productId;
  final String productName;
  final String category;
  final String companyId;
  final String description;
  final String id;
  final List<String> images;
  final double price;
  final int status;
  final List<ExtrasItem>? extrasList;

  Product({
    required this.productId,
    required this.productName,
    required this.category,
    required this.companyId,
    required this.description,
    required this.id,
    required this.images,
    required this.price,
    required this.status,
    required this.extrasList,
  });

  List<String> listExtrasNames() {
    List<String> extrasNames = [];
    if (extrasList == null) return extrasNames;
    for (ExtrasItem extrasItem in extrasList!) {
      extrasNames.add(extrasItem.name);
    }
    return extrasNames;
  }

  factory Product.fromMap(Map<String, dynamic> map) {
    return Product(
      productId: map['id'],
      productName: map['name'],
      category: map['category'],
      companyId: map['company_id'],
      description: map['description'],
      id: map['id'],
      images: List<String>.from(map['images'] ?? []),
      price: (map['price'] ?? 0).toDouble(),
      status: map['status'] ?? 0,
      extrasList: map['extrasList']==null ? [] : ExtrasItem.decodeExtrasList(map['extrasList']),
    );
  }
}
