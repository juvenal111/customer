import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

class Oferta {
  String conductorId;
  String conductorNombre;
  String conductorModeloVehiculo;
  String conductorColorVehiculo;
  double tarifaOfertada;
  String tiempoEstimado;
  Timestamp creationTime;
  int durationInSeconds; // Duración de la oferta en segundos
  Timer? _timer; // Temporizador asociado a la oferta
  bool _timerActive = false; // Estado del temporizador

  Oferta({
    required this.conductorId,
    required this.conductorNombre,
    required this.conductorModeloVehiculo,
    required this.conductorColorVehiculo,
    required this.tarifaOfertada,
    required this.tiempoEstimado,
    required this.creationTime,
    required this.durationInSeconds,
  });

  factory Oferta.fromJson(Map<String, dynamic> json) {
    return Oferta(
      conductorId: json['conductor_id'],
      conductorNombre: json['conductor_nombre'],
      conductorModeloVehiculo: json['conductor_modelo_vehiculo'],
      conductorColorVehiculo: json['conductor_color_vehiculo'],
      tarifaOfertada: json['tarifa_ofertada'].toDouble(),
      tiempoEstimado: json['tiempo_estimado'],
      creationTime: Timestamp.fromMillisecondsSinceEpoch(
        DateTime.parse(json['creation_time']).millisecondsSinceEpoch,
      ),
      durationInSeconds: json['duration_in_seconds'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'conductor_id': conductorId,
      'conductor_nombre': conductorNombre,
      'conductor_modelo_vehiculo': conductorModeloVehiculo,
      'conductor_color_vehiculo': conductorColorVehiculo,
      'tarifa_ofertada': tarifaOfertada,
      'tiempo_estimado': tiempoEstimado,
      'creation_time': creationTime, // Asegúrate de agregar la marca de tiempo aquí
      'duration_in_seconds': durationInSeconds, // Asegúrate de agregar la duración aquí
    };
  }

  bool get isTimerActive => _timerActive;
  bool _expired = false; // Nuevo campo para indicar si la oferta ha expirado

  bool get isExpired => _expired;

  void setExpired() {
    _expired = true;
  }
  void setExpiredFalse() {
    _expired = false;
  }

  // Iniciar el temporizador
  void startTimer(Function onTimeout) {
    _timer = Timer(Duration(seconds: durationInSeconds), () {
      _timerActive = false;
      onTimeout();
    });
    _timerActive = true;
  }

  // Cancelar el temporizador
  void cancelTimer() {
    _timer?.cancel();
    _timerActive = false;
  }
}
