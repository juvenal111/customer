class RideRequest {
  String driverId;
  String paymentMethod;
  Map<String, String> pickUpLocMap;
  Map<String, String> dropOffLocMap;
  String createdAt;
  String riderName;
  String riderPhone;
  String pickupAddress;
  String dropoffAddress;
  String distanceText;
  String durationText;
  String tipoVehiculo;
  String subtipoVehiculo;
  String carrerasTomadas;
  String tarifaSugerida;
  String tarifaOfertada;
  double precioAceptado;
  String deseo;
  String status;
  List<dynamic> conductores;
  String tipoCarrera;

  RideRequest({
    required this.driverId,
    required this.paymentMethod,
    required this.pickUpLocMap,
    required this.dropOffLocMap,
    required this.createdAt,
    required this.riderName,
    required this.riderPhone,
    required this.pickupAddress,
    required this.dropoffAddress,
    required this.distanceText,
    required this.durationText,
    required this.tipoVehiculo,
    required this.subtipoVehiculo,
    required this.carrerasTomadas,
    required this.tarifaSugerida,
    required this.tarifaOfertada,
    required this.deseo,
    required this.status,
    required this.conductores,
    required this.tipoCarrera,
    required this.precioAceptado,
  });

  factory RideRequest.fromMap(Map<String, dynamic> map) {
    return RideRequest(
      driverId: map['driver_id'],
      paymentMethod: map['payment_method'],
      pickUpLocMap: Map<String, String>.from(map['pickup']),
      dropOffLocMap: Map<String, String>.from(map['dropOff']),
      createdAt: map['created_at'],
      riderName: map['rider_name'],
      riderPhone: map['rider_phone'],
      pickupAddress: map['pickup_address'],
      dropoffAddress: map['dropoff_address'],
      distanceText: map['distanceText'],
      durationText: map['durationText'],
      tipoVehiculo: map['tipo_vehiculo'],
      subtipoVehiculo: map['subtipo_vehiculo'],
      carrerasTomadas: map['carreras_tomadas'],
      tarifaSugerida: map['tarifa_sugerida'],
      tarifaOfertada: map['tarifaOfertada'],
      deseo: map['deseo'],
      status: map['status'],
      conductores: List<dynamic>.from(map['conductores']),
      tipoCarrera: map['tipo_carrera'],
      precioAceptado: map['precio_aceptado'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'driver_id': driverId,
      'payment_method': paymentMethod,
      'pickup': pickUpLocMap,
      'dropOff': dropOffLocMap,
      'created_at': createdAt,
      'rider_name': riderName,
      'rider_phone': riderPhone,
      'pickup_address': pickupAddress,
      'dropoff_address': dropoffAddress,
      'distanceText': distanceText,
      'durationText': durationText,
      'tipo_vehiculo': tipoVehiculo,
      'subtipo_vehiculo': subtipoVehiculo,
      'carreras_tomadas': carrerasTomadas,
      'tarifa_sugerida': tarifaSugerida,
      'tarifaOfertada': tarifaOfertada,
      'deseo': deseo,
      'status': status,
      'conductores': List<dynamic>.from(conductores),
      'tipoCarrera': tipoCarrera,
      'precio_aceptado': precioAceptado,
    };
  }
}
