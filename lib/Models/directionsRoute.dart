class DirectionsRoute {
  final Distance distance;
  final Duration duration;
  final String polylinePoints;

  DirectionsRoute({
    required this.distance,
    required this.duration,
    required this.polylinePoints,
  });

  factory DirectionsRoute.fromMap(Map<String, dynamic> map) {
    final legs = map['legs'] as List<dynamic>;
    final distance = Distance(
      text: legs[0]['distance']['text'] as String,
      value: legs[0]['distance']['value'] as int,
    );
    final duration = Duration(
      text: legs[0]['duration']['text'] as String,
      value: legs[0]['duration']['value'] as int,
    );
    final polylinePoints = map['overview_polyline']['points'] as String;
    return DirectionsRoute(
      distance: distance,
      duration: duration,
      polylinePoints: polylinePoints,
    );
  }
}

class Distance {
  final String text;
  final int value;

  Distance({required this.text, required this.value});
}

class Duration {
  final String text;
  final int value;

  Duration({required this.text, required this.value});
}
