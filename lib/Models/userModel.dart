import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  static const ID = "id";
  static const NAME = "name";
  static const EMAIL = "email";
  static const PHONE = "phone";
  static const VOTES = "votes";
  static const TRIPS = "trips";
  static const RATING = "rating";
  static const TOKEN = "token";
  static const RIDES = "rides";

  String? _id;
  String? _name;
  String? _email;
  String? _phone;
  String? _token;
  int? _votes;
  int? _trips;
  double? _rating; // Changed to double
  Map<String, bool>? _rides;

  String? get name => _name;

  String? get email => _email;

  String? get id => _id;

  String? get token => _token;

  String? get phone => _phone;

  int? get votes => _votes;

  int? get trips => _trips;

  double? get rating => _rating; // Changed to double
  Map<String, bool>? get rides => _rides;

  UserModel.fromSnapshot(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
    _name = data[NAME] as String? ?? "N/A";
    _email = data[EMAIL] as String? ?? "N/A";
    _id = data[ID] as String? ?? "N/A";
    _token = data[TOKEN] as String? ?? "N/A";
    _phone = data[PHONE] as String? ?? "N/A";
    _votes = data[VOTES] as int? ?? 0;
    _trips = data[TRIPS] as int? ?? 0;
    _rating = double.tryParse(data[RATING]?.toString() ?? "0.0"); // Convert to double
    _rides = data[RIDES] != null ? Map<String, bool>.from(data[RIDES] as Map<dynamic, dynamic>) : null;
  }

  UserModel.fromMap(Map<String, dynamic> map) {
    _id = map[ID] as String? ?? "N/A";
    _name = map[NAME] as String? ?? "N/A";
    _email = map[EMAIL] as String? ?? "N/A";
    _phone = map[PHONE] as String? ?? "N/A";
    _token = map[TOKEN] as String? ?? "N/A";
    _votes = map[VOTES] as int? ?? 0;
    _trips = map[TRIPS] as int? ?? 0;
    _rating = double.tryParse(map[RATING]?.toString() ?? "0.0"); // Convert to double
    _rides = map[RIDES] != null ? Map<String, bool>.from(map[RIDES] as Map<dynamic, dynamic>) : null;
  }

  // Método para serializar UserModel a JSON
  Map<String, dynamic> toJson() {
    return {
      ID: _id,
      NAME: _name,
      EMAIL: _email,
      PHONE: _phone,
      TOKEN: _token,
      VOTES: _votes,
      TRIPS: _trips,
      RATING: _rating,
      RIDES: _rides,
    };
  }

  // Constructor para deserializar UserModel desde JSON
  UserModel.fromJson(Map<String, dynamic> json) {
    _id = json[ID] as String? ?? "N/A";
    _name = json[NAME] as String? ?? "N/A";
    _email = json[EMAIL] as String? ?? "N/A";
    _phone = json[PHONE] as String? ?? "N/A";
    _token = json[TOKEN] as String? ?? "N/A";
    _votes = json[VOTES] as int? ?? 0;
    _trips = json[TRIPS] as int? ?? 0;
    _rating = double.tryParse(json[RATING]?.toString() ?? "0.0");
    _rides = json[RIDES] != null ? Map<String, bool>.from(json[RIDES] as Map<dynamic, dynamic>) : null;
  }
}
