import 'dart:core';

class DirectionDetails {
  double? distanceValue;
  int? durationValue;
  String? distanceText;
  String? durationText;
  String? encodePoints;

  DirectionDetails(
      {this.distanceValue, this.durationValue, this.distanceText, this.durationText, this.encodePoints});
}