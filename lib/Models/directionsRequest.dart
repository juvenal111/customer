import 'package:geolocator/geolocator.dart';

class DirectionsRequest {
  Position origin;
  Position destination;
  String travelMode;

  DirectionsRequest({
    required this.origin,
    required this.destination,
    this.travelMode = 'driving',
  });

  Map<String, dynamic> toJson() => {
    'origin': '${origin.latitude},${origin.longitude}',
    'destination': '${destination.latitude},${destination.longitude}',
    'travelMode': travelMode,
  };
}