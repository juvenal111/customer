import 'package:viaje_seguro/Models/product.dart';

class Company {
  late String logo;
  late String headerImage;
  late String address;
  late String companyName;
  late String contactPerson;
  late String email;
  late String tipo;
  late String phoneNumber;
  late List<Map<String, dynamic>> sucursales;
  List<Product>? productos;

  Company({
    required this.logo,
    required this.headerImage,
    required this.address,
    required this.companyName,
    required this.contactPerson,
    required this.email,
    required this.tipo,
    required this.phoneNumber,
    required this.sucursales,
    this.productos,
  });

  factory Company.fromMap(Map<String, dynamic> map) {
    return Company(
      logo: map['logo'] ?? '',
      headerImage: map['header_image'] ?? '',
      address: map['address'] ?? '',
      companyName: map['companyName'] ?? '',
      contactPerson: map['contactPerson'] ?? '',
      email: map['email'] ?? '',
      tipo: map['tipo'] ?? '',
      phoneNumber: map['phoneNumber'] ?? '',
      sucursales: List<Map<String, dynamic>>.from(map['sucursales'] ?? []),
      productos: [],
    );
  }
}
