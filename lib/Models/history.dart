import 'package:cloud_firestore/cloud_firestore.dart';

class HistoryModel {
  final String carrerasTomadas;
  final String createdAt;
  final String deseo;
  final String distanceText;
  final String distanciaConductorPasajero;
  final String driverId;
  Map<String, String> dropOff;
  final String dropOffAddress;
  final String durationText;
  final String paymentMethod;
  Map<String, String> pickup;
  final String pickupAddress;
  final double precioAceptado;
  final String riderName;
  final String riderPhone;
  final String status;
  final String subtipoVehiculo;
  final double tarifaOfertada;
  final double tarifaSugerida;
  final String tipoCarrera;
  final String tipoVehiculo;
  final String driverCode;

  HistoryModel({
    required this.carrerasTomadas,
    required this.createdAt,
    required this.deseo,
    required this.distanceText,
    required this.distanciaConductorPasajero,
    required this.driverId,
    required this.dropOff,
    required this.dropOffAddress,
    required this.durationText,
    required this.paymentMethod,
    required this.pickup,
    required this.pickupAddress,
    required this.precioAceptado,
    required this.riderName,
    required this.riderPhone,
    required this.status,
    required this.subtipoVehiculo,
    required this.tarifaOfertada,
    required this.tarifaSugerida,
    required this.tipoCarrera,
    required this.tipoVehiculo,
    required this.driverCode,
  });

  factory HistoryModel.fromSnapshot(DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final data = snapshot.data();
    return HistoryModel(
      carrerasTomadas: data?['carreras_tomadas'] ?? '',
      createdAt: data?['created_at'] ?? '',
      deseo: data?['deseo'] ?? '',
      distanceText: data?['distanceText'] ?? '',
      distanciaConductorPasajero: data?['distancia_conductor_pasajero'] ?? '',
      driverId: data?['driver_id'] ?? '',
      dropOff: Map<String, String>.from(data?['dropOff'] ?? {}),
      dropOffAddress: data?['dropoff_address'] ?? '',
      durationText: data?['durationText'] ?? '',
      paymentMethod: data?['payment_method'] ?? '',
      pickup: Map<String, String>.from(data?['pickup'] ?? {}),
      pickupAddress: data?['pickup_address'] ?? '',
      precioAceptado: parseDouble(data?['precio_aceptado']),
      riderName: data?['rider_name'] ?? '',
      riderPhone: data?['rider_phone'] ?? '',
      status: data?['status'] ?? '',
      subtipoVehiculo: data?['subtipo_vehiculo'] ?? '',
      tarifaOfertada: parseDouble(data?['tarifaOfertada']),
      tarifaSugerida: parseDouble(data?['tarifa_sugerida']),
      tipoCarrera: data?['tipo_carrera'] ?? '',
      tipoVehiculo: data?['tipo_vehiculo'] ?? '',
      driverCode: data?['driver_code'] ?? '',
    );
  }

  static double parseDouble(dynamic value) {
    if (value is double) {
      return value;
    } else if (value is int) {
      return value.toDouble();
    } else if (value is String) {
      if (double.tryParse(value) != null) {
        return double.parse(value);
      } else {
        // Valor no numérico, retornar un valor predeterminado o lanzar una excepción según tu lógica
        return 0.0; // Valor predeterminado
        // throw Exception('Valor no numérico: $value');
      }
    } else {
      // Valor no numérico, retornar un valor predeterminado o lanzar una excepción según tu lógica
      return 0.0; // Valor predeterminado
      // throw Exception('Valor no numérico: $value');
    }
  }
  factory HistoryModel.fromMap(Map<String, dynamic> map) {
    return HistoryModel(
      carrerasTomadas: map['carreras_tomadas'] ?? '',
      createdAt: map['created_at'] ?? '',
      deseo: map['deseo'] ?? '',
      distanceText: map['distanceText'] ?? '',
      distanciaConductorPasajero: map['distancia_conductor_pasajero'] ?? '',
      driverId: map['driver_id'] ?? '',
      dropOff: Map<String, String>.from(map['dropOff'] ?? {}),
      dropOffAddress: map['dropoff_address'] ?? '',
      durationText: map['durationText'] ?? '',
      paymentMethod: map['payment_method'] ?? '',
      pickup: Map<String, String>.from(map['pickup'] ?? {}),
      pickupAddress: map['pickup_address'] ?? '',
      precioAceptado: parseDouble(map['precio_aceptado']),
      riderName: map['rider_name'] ?? '',
      riderPhone: map['rider_phone'] ?? '',
      status: map['status'] ?? '',
      subtipoVehiculo: map['subtipo_vehiculo'] ?? '',
      tarifaOfertada: parseDouble(map['tarifaOfertada']),
      tarifaSugerida: parseDouble(map['tarifa_sugerida']),
      tipoCarrera: map['tipo_carrera'] ?? '',
      tipoVehiculo: map['tipo_vehiculo'] ?? '',
      driverCode: map['driver_code'] ?? '',
    );
  }

}

class Location {
  final String latitude;
  final String longitude;

  Location({required this.latitude, required this.longitude});
}
