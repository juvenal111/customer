class DriverData {
  String carModel;
  String carNumber;
  String tipoMoto;
  String carColor;
  String tipoTaxi;
  String type;
  int code;
  String phone;
  String name;
  bool isBlocked;
  String email;
  bool status;
  int carrerasRealizadas;

  DriverData({
    required this.carModel,
    required this.carNumber,
    required this.tipoMoto,
    required this.carColor,
    required this.tipoTaxi,
    required this.type,
    required this.code,
    required this.phone,
    required this.name,
    required this.isBlocked,
    required this.email,
    required this.status,
    required this.carrerasRealizadas,
  });

  factory DriverData.fromJson(Map<String, dynamic> json) {
    final carData = json['info_car'] ?? {};
    return DriverData(
      carModel: carData['car_model'] ?? '',
      carNumber: carData['car_number'] ?? '',
      tipoMoto: carData['tipo_moto'] ?? '',
      carColor: carData['car_color'] ?? '',
      tipoTaxi: carData['tipo_taxi'] ?? '',
      type: carData['type'] ?? '',
      code: json['code'] ?? 0,
      phone: json['phone'] ?? 0,
      carrerasRealizadas: json['carreras_realizadas'] ?? 0,
      name: json['name'] ?? '',
      isBlocked: json['isBlocked'] ?? false,
      email: json['email'] ?? '',
      status: json['status'] ?? false,
    );
  }
}
