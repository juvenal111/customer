import 'package:google_maps_flutter/google_maps_flutter.dart';

class Marcador {
  final String id;
  LatLng posicion;

  Marcador({required this.id, required this.posicion});

  // Constructor fromMap para crear un Marcador desde un Map
  Marcador.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        posicion = LatLng(map['posicion']['latitude'], map['posicion']['longitude']);

  // Método toMap para convertir un Marcador a un Map
  Map<String, dynamic> toMap() => {
    'id': id,
    'posicion': {
      'latitude': posicion.latitude,
      'longitude': posicion.longitude,
    },
  };

  void mover(LatLng nuevaPosicion) {
    posicion = nuevaPosicion;
  }
  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Marcador && other.id == id;
  }

  @override
  int get hashCode => id.hashCode;
}
