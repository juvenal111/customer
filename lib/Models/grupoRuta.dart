import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'marcador.dart';

class PuntoGeografico {
  final double latitud;
  final double longitud;

  PuntoGeografico({required this.latitud, required this.longitud});
}
class Ruta {
  final String nombre;
  Marcador inicio;
  Marcador fin;
  List<LatLng> polylineCoordinates = [];
  double distanceToOrigin = 0;
  double distanceToDestination = 0;
  Ruta({required this.nombre, required this.inicio, required this.fin});

}
class Grupo {
  final String id;
  final String descripcion;
  final String nombre;
  late final List<Ruta> rutas;

  Grupo({required this.id, required this.descripcion, required this.nombre, required this.rutas});
}
