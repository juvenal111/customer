import 'package:animate_do/animate_do.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:viaje_seguro/pages/screen_elements/buttons/cart_fab.dart';
import 'package:viaje_seguro/pages/screen_elements/cards/column_card.dart';
import 'package:viaje_seguro/pages/screen_elements/carousel/carousel.dart';
import 'package:viaje_seguro/pages/screen_elements/carousel/category_card_deck.dart';
import 'package:viaje_seguro/pages/screen_elements/standard_drawer.dart';
import 'package:viaje_seguro/pages/screen_elements/title.dart';
import '../Models/company.dart';
import '../data_model/meal_item.dart';
import '../data_search.dart';
import '../services/category_service.dart';
import '../services/company_services.dart';
import '../temp_data/temp_more_choices_lists.dart';
import '../temp_data/temp_restaurants_lists.dart';
import '../temp_data/temp_top_choices_lists.dart';

// const List categories = [
//   {"img": "images/images/pickup.svg", "name": "Pickup"},
//   {"img": "images/images/groceries.svg", "name": "Grocery"},
//   {"img": "images/images/essentials.svg", "name": "Essentials"},
//   {"img": "images/images/fruit.svg", "name": "Fruit"},
//   {"img": "images/images/alcohols.svg", "name": "Alcohol"},
//   {"img": "images/images/deals.svg", "name": "Deals"},
//   {"img": "images/images/discount.svg", "name": "Discount"},
// ];

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int activeMenu = 0;
  List<Map<String, dynamic>> categories = [];
  CompanyServices firestoreService = CompanyServices();
  List<Company> listaCompany = [];
  Map<String, List<Company>> companiesByType = {};
  bool isDataLoaded = false; // Track whether data is loaded
  @override
  void initState() {
    super.initState();
    fetchCategories();
    fetchCompany();
  }

  Future<void> fetchCompany() async {
    Map<String, List<Company>> companiesByTyper = await firestoreService.getCompaniesWithProducts();
    print('longitud');
    print(companiesByTyper);
    if (companiesByTyper.isNotEmpty) {
      setState(() {
        companiesByType = companiesByTyper;
        isDataLoaded = true; // Mark data as loaded
      });
      print(companiesByType.length);
    } else {
      print('No hay compañías encontradas');
    }
  }

  Future<void> fetchCategories() async {
    List<Map<String, dynamic>> fetchedCategories = await fetchCategoriesFromFirestore();

    setState(() {
      categories = fetchedCategories;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final ThemeData themeStyle = Theme.of(context);
    final ScrollController _controller = ScrollController(); // Crea una instancia de ScrollController
    const List menu = ["Delivery", "Pickup", "Dine-In"];

    return Scaffold(
      appBar: AppBar(title: Image.asset("images/Logo.png", height: 45, width: 45), centerTitle: true, elevation: 0, actions: <Widget>[
        IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(context: context, delegate: DataSearch());
            })
      ]),
      body: SafeArea(
          child: Container(
              color: Colors.white,
              child: ListView(
                  controller: _controller, // Asigna el ScrollController aquí
                  children: <Widget>[
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        menu.length,
                        (index) {
                          return Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  activeMenu = index;
                                });
                              },
                              child: activeMenu == index
                                  ? ElasticIn(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: Colors.black,
                                          borderRadius: BorderRadius.circular(30),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.only(left: 15, right: 15, bottom: 8, top: 8),
                                          child: Row(
                                            children: [
                                              Text(
                                                menu[index],
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  : Container(
                                      decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 15, right: 15, bottom: 8, top: 8),
                                        child: Row(
                                          children: [
                                            Text(
                                              menu[index],
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(height: 15),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 15),
                          height: 45,
                          width: size.width - 70,
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.15),
                            borderRadius: BorderRadius.circular(30),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.all(12),
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      "images/images/pin_icon.svg",
                                      width: 20,
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      "La Guardia",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(5),
                                child: Container(
                                  height: 35,
                                  decoration: BoxDecoration(color: Color(0xFFFFFFFF), borderRadius: BorderRadius.circular(30)),
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 15, right: 15),
                                    child: Row(
                                      children: [
                                        SvgPicture.asset("images/images/time_icon.svg", width: 20),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        Text(
                                          "Ahora",
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        SizedBox(width: 2),
                                        Icon(Icons.keyboard_arrow_down)
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            child: SvgPicture.asset("images/images/filter_icon.svg"),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 24),

                    title("Let's", "Chop"),
                    SizedBox(height: 24),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        margin: EdgeInsets.only(left: 30),
                        child: Row(
                          children: List.generate(
                            categories.length,
                            (index) {
                              return Padding(
                                padding: const EdgeInsets.only(right: 30),
                                child: Column(
                                  children: [
                                    Image.network(
                                      categories[index]['picture'], // Reemplaza esto con la URL de la imagen de Internet
                                      width: 60,
                                    ),
                                    Text(
                                      categories[index]['name'],
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),

                    //CategoryCardDeck(),
                    SizedBox(height: 16),
                    isDataLoaded
                        ? CarouselTab(
                            deckTitle: "Negocios",
                            itemList: companiesByType,
                            isMealCard: false,
                          )
                        : Center(child: CircularProgressIndicator()),
                    SizedBox(height: 26),
                    Carousel(
                      deckTitle: "Top Choices",
                      itemList: tempTopChoicesList.mealItems,
                      isMealCard: true,
                      //controller: _controller1,
                    ),
                    SizedBox(height: 26),
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(left: 16),
                      child: Text("More Choices", style: themeStyle.textTheme.titleMedium),
                    ),
                    SizedBox(height: 18),
                    Container(
                      child: Column(
                        children: <Widget>[
                          for (MealItem mealItem in tempMoreChoicesList.mealItems) ColumnCard(mealItem: mealItem),
                          SizedBox(
                            height: 50,
                          ),
                        ],
                      ),
                    )
                  ]))),
      floatingActionButton: CartFAB(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
