import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:viaje_seguro/pages/screen_elements/buttons/bottom_buttons.dart';
import 'package:viaje_seguro/pages/screen_elements/buttons/pop_arrow_button.dart';
import 'package:viaje_seguro/pages/screen_elements/display_restaurant_info.dart';
import 'package:viaje_seguro/pages/screen_elements/header_and_logo.dart';
import 'package:viaje_seguro/pages/screen_elements/my_custom_draggable_sheet.dart';
import 'package:viaje_seguro/pages/screen_elements/tiles/checkbox_extras_tile.dart';
import '../data_model/cart_item.dart';
import '../data_model/extras_item.dart';
import '../data_model/meal_item.dart';

class MealProfilePage extends StatefulWidget {
  final MealItem? mealItem;
  final CartItem? cartItem;

  MealProfilePage({Key? key, this.title, this.mealItem, this.cartItem}) : super(key: key);
  final String? title;

  @override
  _MealProfilePageState createState() => _MealProfilePageState();
}

class _MealProfilePageState extends State<MealProfilePage> {
  late MealItem _mealItem;
  late List<ExtrasItem> _selectedExtras;
  late double _currentMealBasePrice;
  late double _mealBasePrice;
  late double _totalExtrasPrice;
  bool checkBox1 = false;
  Map<String, dynamic> cantidadExtra= {};

  @override
  void initState() {
    if (widget.mealItem != null) {
      _mealItem = widget.mealItem!;
      _selectedExtras = <ExtrasItem>[];
      _totalExtrasPrice = 0;
      _mealBasePrice = _mealItem.basePrice;
    } else {
      _mealItem = widget.cartItem!.mealItem;
      _selectedExtras = widget.cartItem!.selectedExtras;
      _totalExtrasPrice = widget.cartItem!.getTotalExtrasPrice();
      _mealBasePrice = widget.cartItem!.mealItem.basePrice;
    }
    _currentMealBasePrice = _mealBasePrice + _totalExtrasPrice;
    super.initState();
  }

  //Getters


  @override
  Widget build(BuildContext context) {
    final ThemeData themeStyle = Theme.of(context);
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          leading: PopArrowButton(),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Stack(
          children: <Widget>[
            HeaderAndLogo(
              mealItem: _mealItem,
            ),
            MyCustomDraggableSheet(
              child: Container(
                color: Colors.white,
                margin: EdgeInsets.only(top: 2),
                padding: EdgeInsets.fromLTRB(24, 16, 24, 16),
                child: Column(children: <Widget>[
                  DisplayRestaurantInfo(mealItem: _mealItem),
                  SizedBox(height: 32),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text("Extras", style: themeStyle.textTheme.subtitle1?.copyWith(decoration: TextDecoration.underline)),
                  ),
                  Container(
                    height: 200,
                    child: ListView.builder(
                      itemCount: _mealItem.listExtrasNames().length,
                      itemBuilder: (context, index) {

                        return CheckboxExtrasTile(
                          checkbox: Checkbox(
                            value: _selectedExtras.map((extra) => extra.name).contains(_mealItem.listExtrasNames()[index]),
                            onChanged: (bool? value) {
                              setState(() {
                                if (value!) {
                                  _totalExtrasPrice += _mealItem.extrasList![index].price * cantidadExtra[_mealItem.extrasList![index].name];
                                  //_selectedExtras.add(_mealItem.listExtrasNames()[index]);
                                  _selectedExtras.add(ExtrasItem(
                                      imageUrl: _mealItem.extrasList![index].imageUrl,
                                      name: _mealItem.extrasList![index].name,
                                      price: _mealItem.extrasList![index].price));
                                } else {
                                  _totalExtrasPrice -= _mealItem.extrasList![index].price;
                                  String selectedExtrasName = _mealItem.listExtrasNames()[index];
                                  _selectedExtras.removeWhere((element) => element.name == selectedExtrasName);
                                }
                                _currentMealBasePrice = _mealBasePrice + _totalExtrasPrice;
                              });
                            },
                          ),
                          text: Text(_mealItem.listExtrasNames()[index]),
                          price: _mealItem.extrasList![index].price,
                          imageUrl: _mealItem.extrasList![index].imageUrl!,
                          onQuantityChanged: handleQuantityChanged,
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ]),
              ),
            ),
          ],
        ),
        floatingActionButton: widget.mealItem != null
            ? BottomButtons(
                mealItem: _mealItem,
                selectedExtras: _selectedExtras,
                currentMealBasePrice: _currentMealBasePrice,
              )
            : BottomButtons(
                cartItem: widget.cartItem,
                selectedExtras: _selectedExtras,
                currentMealBasePrice: _currentMealBasePrice,
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterDocked,
      ),
    );
  }
  handleQuantityChanged(int quantity, String nombre) {
    print(quantity);
    print(nombre);
    //_selectedExtras.map((extra) => extra.name).contains(nombre);
    cantidadExtra[nombre]=quantity;

    // Maneja la cantidad cambiada aquí
    // Actualiza el estado según sea necesario
    // Puedes usar esta información para actualizar el precio total, etc.
  }
  List<ExtrasItem> _getSelectedExtras(List<String> checked) {
    List<ExtrasItem> temp = [];
    for (var i = 0; i < _mealItem.extrasList!.length; i++) if (checked.contains(_mealItem.extrasList?[i].name)) temp.add(_mealItem.extrasList![i]);

    return temp;
  }
}
