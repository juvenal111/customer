import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:viaje_seguro/helpers/style.dart';
import '../../../data_model/meal_item.dart';
import '../../meal_profile_page.dart';

class MealCardTile extends StatelessWidget {
  final MealItem mealItem;

  MealCardTile({required this.mealItem});

  @override
  Widget build(BuildContext context) {
    double textWidth = MediaQuery.of(context).size.width * 0.35;
    String mealName = mealItem.name;
    String? restaurantName = mealItem.restaurantName;
    String imageURL = mealItem.image;
    double price = mealItem.basePrice;

    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MealProfilePage(
                      mealItem: mealItem,
                    )));
      },
      child: Container(
        height: 80,
        width: 312,
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          color: secundary,
          boxShadow: [
            BoxShadow(
                color: Color(0x33000000),
                blurRadius: 8.0,
                spreadRadius: 5.0,
                offset: Offset(0.0, 3.0))
          ],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5.0),
                      bottomLeft: Radius.circular(5.0)),
                  // Replace Image.asset with Image.network
                  child: Image.network(
                    imageURL, // Assuming mealItem.image is the URL string
                    height: 80,
                    width: 80,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(width: 16),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 20),
                    Container(
                      width: textWidth,
                      child: Text(
                        mealName,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                          color: terciary, // Puedes cambiar "Colors.blue" al color que desees
                        ),
                      ),
                    ),
                    Container(
                      width: textWidth,
                      child: Text(restaurantName!,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.displayLarge),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 4, right: 16),
              child: Text("\$" + price.toStringAsFixed(2),
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600, color: terciary)),
            )
          ],
        ),
      ),
    );
  }
}
