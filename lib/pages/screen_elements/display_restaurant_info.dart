import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:viaje_seguro/helpers/style.dart';

import '../../data_model/meal_item.dart';
import '../../data_model/restaurant_item.dart';

class DisplayRestaurantInfo extends StatelessWidget {
  final MealItem? mealItem;
  final RestaurantItem? restaurantItem;

  DisplayRestaurantInfo({
    this.mealItem,
    this.restaurantItem,
  });

  @override
  Widget build(BuildContext context) {
    ThemeData themeStyle = Theme.of(context);
    String? bigTitleName;
    double? basePrice;
    String? description;
    int? estDeliveryTime;
    int? deliveryFee;

    if (mealItem != null) {
      bigTitleName = mealItem!.name;
      basePrice = mealItem!.basePrice;
      description = mealItem!.description;
      estDeliveryTime = mealItem!.estDeliveryTime;
      deliveryFee = mealItem!.deliveryFee;
    } else {
      bigTitleName = restaurantItem?.name;
      description = restaurantItem?.description;
      estDeliveryTime = restaurantItem?.estimatedDeliveryTime;
      deliveryFee = restaurantItem?.deliveryFee;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width * 0.65,
              alignment: Alignment.centerLeft,
              child: Text(
                bigTitleName ?? "",
                style: themeStyle.textTheme.headline6,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Visibility(
              visible: basePrice != null,
              child: Container(
                alignment: Alignment.centerRight,
                child: Text(
                  basePrice == null ? " " : "\$${basePrice.toStringAsFixed(2)}",
                  style: themeStyle.textTheme.subtitle1
                      ?.copyWith(color: Color(0xFF535353)),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 8),
        Container(
          alignment: Alignment.topLeft,
          child: Text(
            description ?? "",
            style: themeStyle.textTheme.bodyText2,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        SizedBox(height: 12),
        Row(
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(color: primary),
              child: Text(
                "Time: ${estDeliveryTime ?? 0} mins",
                style: themeStyle.textTheme.headlineSmall,
              ),
            ),
            SizedBox(width: 12),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(color: primary),
              child: Text("Delivery Fee: Ghs ${deliveryFee ?? 0}",
                  style: themeStyle.textTheme.headlineSmall),
            ),
          ],
        ),
      ],
    );
  }
}
