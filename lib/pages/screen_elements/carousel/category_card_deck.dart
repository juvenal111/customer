import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../cards/category_card.dart';

class CategoryCardDeck extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 106.0,
        child: ListView(
            padding: EdgeInsets.only(bottom: 10),
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              CategoryCard(
                  imageDirName: 'images/Burger.jpg', categoryName: "Burgers"),
              CategoryCard(
                  imageDirName: 'images/Chicken.jpg', categoryName: "Chicken"),
              CategoryCard(
                  imageDirName: 'images/Rice.jpg', categoryName: "Rice"),
              CategoryCard(
                  imageDirName: 'images/Local.jpg', categoryName: "Local"),
              CategoryCard(
                  imageDirName: 'images/Noodles.jpg', categoryName: "More...")
            ]
        )
    );
  }
}