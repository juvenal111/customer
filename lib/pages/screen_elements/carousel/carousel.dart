import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:viaje_seguro/data_model/meal_item.dart';
import 'package:viaje_seguro/data_model/restaurant_item.dart';
import '../../../Models/company.dart';
import '../../../custom_scroll_physics.dart';
import '../cards/meal_card.dart';
import '../cards/restuarant_card.dart'; // Corregido el nombre de la importación

// class Carousel extends StatefulWidget {
//   final String deckTitle;
//   final List itemList;
//   final bool isMealCard;
//   final ScrollController controller;
//
//   Carousel({
//     Key? key,
//     required this.deckTitle,
//     required this.itemList,
//     required this.isMealCard,
//     required this.controller,
//   }) : super(key: key);
//
//   @override
//   _CarouselState createState() => _CarouselState();
// }
//
// class _CarouselState extends State<Carousel> {
//   late String _deckTitle;
//   late List _itemList;
//   late bool _isMealCard;
//   late ScrollController _controller; // Cambiado de final a variable no inicializada
//   CustomScrollPhysics? _physics; // Agregada declaración de _physics
//
//   @override
//   void initState() {
//     super.initState();
//     _deckTitle = widget.deckTitle;
//     _itemList = widget.itemList;
//     _isMealCard = widget.isMealCard;
//     _controller = widget.controller;
//     print('longitud');
//     print(_itemList.length);
//     _controller.addListener(() {
//       if (_controller.position.haveDimensions && _physics == null) {
//         setState(() {
//           var dimension =
//               _controller.position.maxScrollExtent / (_itemList.length - 1);
//           _physics = CustomScrollPhysics(itemDimension: dimension);
//         });
//       }
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: <Widget>[
//         Container(
//           alignment: Alignment.centerLeft,
//           margin: EdgeInsets.only(left: 16),
//           child: Text(
//             _deckTitle,
//             style: Theme.of(context).textTheme.titleMedium,
//           ),
//         ),
//         SizedBox(height: 8),
//         Container(
//           height: _isMealCard ? 220 : 158,
//           child: ListView.builder(
//             controller: _controller,
//             physics: _physics,
//             scrollDirection: Axis.horizontal,
//             padding: EdgeInsets.symmetric(horizontal: 8),
//             itemCount: _itemList.length, // Cambiado de pages.length a _itemList.length
//             itemBuilder: (BuildContext context, int index) {
//               if (_isMealCard) {
//                 return Center(
//                   child: MealCard(
//                     mealItem: _itemList[index],
//                   ),
//                 );
//               } else {
//                 return Center(
//                   child: RestaurantCard(
//                     restaurantItem: _itemList[index],
//                   ),
//                 );
//               }
//             },
//           ),
//
//         ),
//       ],
//     );
//   }
// }




class Carousel extends StatefulWidget {
  final String deckTitle;
  final List itemList;
  final bool isMealCard;

  Carousel(
      {Key? key,
        required this.deckTitle,
        required this.itemList,
        required this.isMealCard,})
      : super(key: key);

  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  String? _deckTitle;
  List? _itemList;
  bool? _isMealCard;
  final _controller = ScrollController();
  List? pages;
  ScrollPhysics? _physics;

  @override
  void initState() {
    super.initState();
    _deckTitle = widget.deckTitle;
    _itemList = widget.itemList;
    pages = _itemList;
    _isMealCard = widget.isMealCard;

    _controller.addListener(() {
      if (_controller.position.haveDimensions && _physics == null) {
        setState(() {
          var dimension =
              _controller.position.maxScrollExtent / (pages!.length - 1);
          _physics = CustomScrollPhysics(itemDimension: dimension);
        });
      }
    });
  }

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(left: 16),
          child: Text(
            _deckTitle!,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        SizedBox(height: 8),
        Container(
          //different sizes required to not crop the corresponding cards.
          height: _isMealCard! ? 220 : 158,
          child: ListView.builder(
            controller: _controller,
            physics: _physics,
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.symmetric(horizontal: 8),
            itemCount: pages?.length,
            itemBuilder: (BuildContext context, int index) {
              if (_isMealCard!) {
                return Center(
                  child: MealCard(
                    mealItem: _itemList![index],
                  ),
                );
              }
              else {
                return Center(
                  child: RestaurantCard(
                    restaurantItem: _itemList![index],
                  ),
                );
              }
            },
          ),
        ),
      ],
    );
  }
}



class CarouselTab extends StatefulWidget {
  final String deckTitle;
  final Map<String, List<Company>> itemList;
  final bool isMealCard;

  CarouselTab({
    Key? key,
    required this.deckTitle,
    required this.itemList,
    required this.isMealCard,
  }) : super(key: key);

  @override
  _CarouselTabState createState() => _CarouselTabState();
}

class _CarouselTabState extends State<CarouselTab> {
  late String _deckTitle;
  late Map<String, List<Company>> _itemList;
  late bool _isMealCard;
  final _controller = ScrollController();
  ScrollPhysics? _physics;

  @override
  void initState() {
    super.initState();
    _deckTitle = widget.deckTitle;
    _itemList = widget.itemList;
    _isMealCard = widget.isMealCard;

    _controller.addListener(() {
      if (_controller.position.haveDimensions && _physics == null) {
        setState(() {
          var dimension = _controller.position.maxScrollExtent / (_itemList.values.map((list) => list.length).reduce((a, b) => a + b) - 1);
          _physics = CustomScrollPhysics(itemDimension: dimension);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(left: 16),
          child: Text(
            _deckTitle,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        SizedBox(height: 8),
        DefaultTabController(
          length: _itemList.keys.length, // Número de pestañas
          child: Column(
            children: [
              TabBar(
                tabs: _itemList.keys.map((key) => Tab(text: key)).toList(),
              ),
              SizedBox(
                height: _isMealCard ? 220 : 158,
                child: TabBarView(
                  children: _itemList.entries.map((entry) {
                    return ListView.builder(
                      controller: _controller,
                      physics: _physics,
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      itemCount: entry.value.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Center(
                          child: _isMealCard! ? MealCard(
                            mealItem: MealItem(
                                name: '' ,description: '',
                                basePrice: 20,
                                id: 1,
                                image: '',
                                deliveryFee: 1,
                                estDeliveryTime: 10,
                                extrasList: [],
                                restaurantName: '',
                                sizes: []),
                          )  :RestaurantCard(
                            restaurantItem: RestaurantItem(
                                id: 1,
                                headerImage: entry.value[index].headerImage,
                                deliveryFee: 1,
                                description: '',
                                estimatedDeliveryTime: 10,
                                logoImage: entry.value[index].logo,
                                name: entry.value[index].companyName,
                                contactNumber: entry.value[index].phoneNumber,
                                restaurantMenu: entry.value[index].productos),
                          ),

                        );
                      },
                    );
                  }).toList(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
