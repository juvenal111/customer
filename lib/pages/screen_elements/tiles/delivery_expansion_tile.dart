import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../helpers/style.dart';
import '../../../providers/cart_provider.dart';

class DeliveryExpansionTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CartProvider _cartProvider = Provider.of<CartProvider>(context);

    return ExpansionTile(
      iconColor: secundary,
      textColor: secundary,
      leading: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 6.0),
            child: Text(
              "Entregar a:",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
            ),
          ),
        ],
      ),
      title: Center(
        child: Text(
          "${_cartProvider.roomNumber} ${_cartProvider.hostelName}",
          style: TextStyle(fontSize: 16),
        ),
      ),
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(8),
          color: Colors.black12,
          child: Column(
            children: <Widget>[
              // ... Otros campos existentes ...

              // Agregar un campo para seleccionar la ubicación en el mapa
              Container(
                padding: EdgeInsets.all(8),
                child: ElevatedButton(
                  onPressed: () {
                    _openMapScreen(context);
                  },
                  child: Text("Seleccionar ubicación en el mapa"),
                ),
              ),

              // Mostrar la lista de direcciones guardadas
              if (_cartProvider.savedAddresses.isNotEmpty)
                Column(
                  children: <Widget>[
                    SizedBox(height: 10),
                    Text(
                      "Direcciones Guardadas:",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 5),
                    Column(
                      children: _cartProvider.savedAddresses
                          .map((address) => ListTile(
                        title: Text(address),
                        // Puedes agregar lógica para usar la dirección seleccionada
                        onTap: () {
                          // Lógica para usar la dirección seleccionada
                        },
                      ))
                          .toList(),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ],
    );
  }

  void _openMapScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MapScreen()),
    );
  }
}

class MapScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Seleccionar ubicación en el mapa"),
      ),
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(0, 0),
          zoom: 15.0,
        ),
      ),
    );
  }
}
