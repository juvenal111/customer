import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import '../../../data_model/cart_item.dart';
import '../../../providers/cart_provider.dart';
import '../../meal_profile_page.dart';

class CartMealTile extends StatelessWidget {
  final CartItem cartItem;

  CartMealTile({
    required this.cartItem,
  });

  @override
  Widget build(BuildContext context) {

    final ThemeData themeStyle = Theme.of(context);
    double textWidth = MediaQuery.of(context).size.width * 0.35;

    var cartProvider = Provider.of<CartProvider>(context);
    String mealName = cartItem.mealItem.name;
    String? restaurantName = cartItem.mealItem.restaurantName;
    double price = cartItem.totalMealPrice;

    return Slidable(

      key: const ValueKey(0),
      // The start action pane is the one at the left or the top side.
      startActionPane: ActionPane(
        // A motion is a widget used to control how the pane animates.
        motion: const ScrollMotion(),

        // A pane can dismiss the Slidable.
        dismissible: DismissiblePane(onDismissed: () {}),

        // All actions are defined in the children parameter.
        children: [
          // A SlidableAction can have an icon and/or a label.
          SlidableAction(
            onPressed: (_){
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MealProfilePage(
                      cartItem: cartItem,
                    )),
              );
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text("Edit"),
                  duration: Duration(seconds: 1),
                ),
              );

              Slidable.of(context)?.close();
            },
            backgroundColor: Color(0xFFFE4A49),
            foregroundColor: Colors.white,
            icon: Icons.delete,
            label: 'Edit',
          ),
          SlidableAction(
            onPressed: edit(),
            backgroundColor: Color(0xFF21B7CA),
            foregroundColor: Colors.white,
            icon: Icons.share,
            label: 'Delete',
          ),
        ],
      ),

      // The end action pane is the one at the right or the bottom side.
      endActionPane: ActionPane(
        motion: ScrollMotion(),
        children: [
          SlidableAction(
            // An action can be bigger than the others.
            flex: 2,
            onPressed: edit(),
            backgroundColor: Color(0xFF7BC043),
            foregroundColor: Colors.white,
            icon: Icons.archive,
            label: 'Archive',
          ),
          SlidableAction(
            onPressed: edit(),
            backgroundColor: Color(0xFF0392CF),
            foregroundColor: Colors.white,
            icon: Icons.save,
            label: 'Save',
          ),
        ],
      ),
      child: Container(
        height: 68,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 37),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  height: 32,
                  width: 32,
                  decoration: BoxDecoration(
                    color: themeStyle.primaryColor,
                    borderRadius: BorderRadius.circular(7),
                  ),
                  child: Center(
                    child: Text(
                      "${cartItem.quantity}",
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          ?.copyWith(color: Colors.white),
                    ),
                  ),
                ),
                SizedBox(width: 30),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: textWidth,
                      child: Text(
                        mealName,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                    ),
                    Container(
                      width: textWidth,
                      child: Text(restaurantName!,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .headline1
                              ?.copyWith(color: themeStyle.primaryColor)),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(left: 4),
              child: Text(
                "\$${price.toStringAsFixed(2)}",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
            ),
          ],
        ),
      ),
    );
  }

  edit() {

  }

}
