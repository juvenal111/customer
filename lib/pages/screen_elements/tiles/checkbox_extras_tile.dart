import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:viaje_seguro/helpers/style.dart';

class CheckboxExtrasTile extends StatefulWidget {
  final Checkbox checkbox;
  final Text text;
  final double price;
  final String imageUrl;
  final Function(int,String) onQuantityChanged;
  CheckboxExtrasTile({
    required this.checkbox,
    required this.text,
    this.price = 0,
    required this.imageUrl,
    required this.onQuantityChanged,
  });

  @override
  _CheckboxExtrasTileState createState() => _CheckboxExtrasTileState();
}

class _CheckboxExtrasTileState extends State<CheckboxExtrasTile> {
  int quantity = 1; // Inicializar la cantidad en 1

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.all(0),
      title: Row(
        children: [
          Container(
            padding: EdgeInsets.all(8.0),
            child: CircleAvatar(
              radius: 20,
              backgroundImage: NetworkImage(widget.imageUrl),
            ),
          ),
          SizedBox(width: 10,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(widget.text.data!, style: Theme.of(context).textTheme.bodyMedium),
                Text(
                  "\$" + (widget.price * quantity).toStringAsFixed(2),
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: Color(0xFF535353)),
                ),
              ],
            ),
          ),
          Row(
            children: [
              IconButton(
                icon: Icon(Icons.remove),
                onPressed: () {
                  if (quantity > 1) {
                    setState(() {
                      quantity--;
                      widget.onQuantityChanged(quantity,widget.text.data!);
                    });
                  }
                },
              ),
              Text(quantity.toString()),
              IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  setState(() {
                    quantity++;
                    widget.onQuantityChanged(quantity, widget.text.data!);
                  });
                },
              ),
              Checkbox(
                value: widget.checkbox.value,
                onChanged: widget.checkbox.onChanged,
                activeColor: secundary,
              ),
            ],
          ),
        ],
      ),
      dense: true,
      onTap: widget.checkbox.onChanged != null
          ? () {
        widget.checkbox.onChanged!(!widget.checkbox.value!);
      }
          : null,
    );
  }
}
