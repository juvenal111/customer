import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:viaje_seguro/screen/login.dart';
import 'package:viaje_seguro/screen/rideRequestScreen.dart';
import 'package:viaje_seguro/widgets/pantallaCarga.dart';
import '../providers/user.dart';


class Splash extends StatefulWidget {
  static const String idScreen = "splash";
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    //checkIfLoggedIn();
  }


  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    print('status splash: ${userProvider.status}');
    return getScreen(userProvider.status);
  }



  Widget getScreen(Status status) {
    switch (status) {
      case Status.Uninitialized:
        return PantallaCarga();
      case Status.Authenticated:
        return RideRequestScreen();
      case Status.Unauthenticated:
        return LoginScreen(); // Pantalla de inicio de sesión si el usuario no está autenticado
      default:
        return PantallaCarga();
    // Pantalla de inicio si el usuario está autenticado
    }
  }


}
