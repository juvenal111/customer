import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating_null_safety/smooth_star_rating_null_safety.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:viaje_seguro/helpers/style.dart';

import '../providers/appData.dart';
import '../providers/user.dart';
import '../widgets/simpleDialogItem.dart';

class ProfileScreen extends StatefulWidget {
  static const String idScreen = "profileScreen";

  @override
  ProfileScreenState createState() => ProfileScreenState();
}

class ProfileScreenState extends State<ProfileScreen> {
  String tipoSuscripcion = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);
    final userModel = userProvider.userModel;

    final SimpleDialog dialogPreguntasFrequentes = SimpleDialog(
      title: Text('Preguntas frecuentes'),
      children: [
        SimpleDialogItem(
          color: Colors.orange,
          text: '+59177603358',
          onPressed: () {
            Navigator.pop(context);
            openwhatsappNumber('+59177603358');
          },
        ),
      ],
    );

    final SimpleDialog dialogContactarSoporte = SimpleDialog(
      title: Text('Contactar soporte'),
      children: [
        SimpleDialogItem(
          color: Colors.orange,
          text: '+59177603358',
          onPressed: () {
            Navigator.pop(context);
            openwhatsappNumber('+59177603358');
          },
        ),
      ],
    );

    final SimpleDialog dialogReportarProblemas = SimpleDialog(
      title: Text('Reportar problemas'),
      children: [
        SimpleDialogItem(
          color: Colors.orange,
          text: '+59177603358',
          onPressed: () {
            Navigator.pop(context);
            openwhatsappNumber('+59177603358');
          },
        ),
      ],
    );

    return Scaffold(
      backgroundColor: secundary,
      appBar: AppBar(
        title: Text('Mi cuenta'),
        backgroundColor: primary,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              child: Container(
                child: Text(
                  userModel?.name?.toUpperCase() ?? '',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 40.0,
                    color: primary,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Brand Bold',
                  ),
                ),
              ),
            ),
            Text(
              "Pasajero",
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.blueGrey[200],
                letterSpacing: 2.5,
                fontWeight: FontWeight.bold,
                fontFamily: 'Brand-Regular',
              ),
            ),
            SizedBox(
              height: 20,
              width: 200,
              child: Divider(
                color: Colors.white,
              ),
            ),
            SizedBox(height: 10.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Icon(Icons.star, color: Colors.amber),
                // SizedBox(width: 5.0),
                SmoothStarRating(
                  rating: userModel?.rating ?? 0.0,
                  color: Colors.amber,
                  allowHalfRating: false,
                  starCount: 5,
                  size: 20,
                  onRatingChanged: null, // No se permite cambiar la calificación

                ),
              ],
            ),
            SizedBox(height: 5.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.directions_car, color: Colors.green),
                SizedBox(width: 5.0),
                Text(
                  "Carreras tomadas: ${Provider.of<AppData>(context, listen: false).countTrips.toString()}",
                  style: TextStyle(
                    fontSize: 16.0,
                    color: primary,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            SizedBox(height: 40.0),
          ],
        ),
      ),
    );
  }

  void openwhatsappNumber(String numero) async {
    String whatsapp = "+591" + "driverPhone";
    var whatsappURl_android = "whatsapp://send?phone=" + numero + "&text=Hola";
    var whatappURL_ios = "https://wa.me/$whatsapp?text=${Uri.parse("hello")}";
    if (Platform.isIOS) {
      if (await canLaunch(whatappURL_ios)) {
        await launch(whatappURL_ios, forceSafariVC: false);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsapp no instalado")));
      }
    } else {
      if (await canLaunch(whatsappURl_android)) {
        await launch(whatsappURl_android);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsapp no instalado")));
      }
    }
  }
}

class InfoCard extends StatelessWidget {
  final String text;
  final IconData icon;
  final void Function()? onPressed;

  InfoCard({required this.text, required this.icon, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Card(
        color: Colors.white,
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
        child: ListTile(
          leading: Icon(
            icon,
            color: Colors.black87,
          ),
          title: Text(
            text,
            style: TextStyle(
              color: Colors.black87,
              fontSize: 16.0,
              fontFamily: "Brand Bold",
            ),
          ),
        ),
      ),
    );
  }
}
