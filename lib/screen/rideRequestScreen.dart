import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:viaje_seguro/Models/Oferta.dart';
import 'package:viaje_seguro/Models/history.dart';
import 'package:viaje_seguro/screen/ratingScreen.dart';
import 'package:viaje_seguro/screen/searchScreen.dart';
import 'package:viaje_seguro/screen/setDestinoScreen.dart';
import '../Assitants/jobRepository.dart';
import '../Assitants/mapKitAssistant.dart';
import '../Assitants/requestAssistant.dart';
import '../Models/address.dart';
import '../Models/driverData.dart';
import '../Models/rideRequests.dart';
import '../Models/userModel.dart';
import '../helpers/UpdateChecker.dart';
import '../helpers/constants.dart';
import '../Assitants/assistantMethods.dart';
import '../Models/directDetails.dart';
import '../pages/home_page.dart';
import '../providers/appData.dart';
import '../providers/user.dart';
import '../variables_constantes.dart';
import '../widgets/CollectFareDialog.dart';
import '../widgets/CountdownTimer.dart';
import '../widgets/menu.dart';
import '../widgets/progressDialog.dart';
import '../helpers/style.dart';

class RideRequestScreen extends StatefulWidget {
  static const String idScreen = "rideRequestScreen";

  const RideRequestScreen({Key? key}) : super(key: key);

  @override
  _RideRequestScreenState createState() => _RideRequestScreenState();
}

class _RideRequestScreenState extends State<RideRequestScreen> {
  static const ESTADO = "estado_solicitud";
  static const RIDEID = "ride_request_doc_id";

  late SharedPreferences prefs;
  String docId = "";
  String deseo = "";
  String statusRide = "";
  String driverId = "";
  String subtipoVehiculo = '';
  LatLng? _previousLocation;
  double precioSolicitud = 0.0;
  double precioMinimo = 0.0;
  double _waitingTime = 0;
  double radius = 15; // en km

  TextEditingController textController = TextEditingController();

  /*MAPAS*/
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42, -122.0058),
    zoom: 14.4746,
  );
  Set<Polyline> polYlineSet = {};

  /**parametros a resetear **/
  bool _showOffersList = false;
  bool _isInitializedPositionStreamSuscription = false;
  bool isVisiblePositioned1 = false;
  bool isVisiblePositioned2 = false;
  bool isVisiblePositioned3 = false;
  bool isVisiblePositioned4 = false;
  bool _isButtonDisabled = false;
  bool _mapTapDisabled = false; // inicialmente el mapa no está bloqueado
  bool solicitudEnviada = false;
  bool isTaxiALaUbicacion = false;
  Timer? timer;
  bool isPlayerAlarm = false;
  bool isPendingRide = false;

  BitmapDescriptor? customIcon;

  late UserModel currentUser;
  late JobRepository jobRepository;

  //late Historial _historial;
  late LatLng p2;

  ///***importante***///
  DriverData? datosConductor; //se setea en getUserRide

  ///*LISTAS*/
  List<Oferta> _offers = [];
  late Stream<List<Oferta>> stream;
  Map<String, dynamic>? rideInfoMapCopy; // variable para guardar la copia de rideInfoMap
  Map<String, double>? categoryPrices = {};
  Map<String, int>? categoryPricesShow = {};
  late DirectionDetails tripDirectionDetails;
  GoogleMapController? mapController;
  final Completer<GoogleMapController> _controllerGoogleMap = Completer();

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  List<Marker> _markers = [];
  final Geoflutterfire _geoFlutterFire = Geoflutterfire();

  final GeolocatorPlatform _geolocator = GeolocatorPlatform.instance;
  late GeoFirePoint currentPosition;

  /*STREAM*/
  StreamSubscription<List<DocumentSnapshot>>? _geoQuerySubscription;
  late StreamSubscription<List<Oferta>> _offersSubscription;
  late StreamSubscription<DocumentSnapshot<Map<String, dynamic>>> rideInfoSubscription;
  StreamSubscription<DocumentSnapshot<Map<String, dynamic>>>? historialSubscription;

  ///**referencias**//
  final CollectionReference _historialCollection = FirebaseFirestore.instance.collection('historial');
  final CollectionReference _rideRequestsCollection = FirebaseFirestore.instance.collection('rideRequests');

  ///bandera a resetear
  bool isSearchButtonVisible = true;
  bool isBackButtonVisible = false;

  Map<Oferta, Timer?> offerTimers = {};
  bool isOfferAccepted = false;

  @override
  void initState() {
    super.initState();
    checkForUpdates(); // Verificar actualizaciones al cargar la vista
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(80, 80)), 'images/car_android.png').then((icon) {
      setState(() {
        customIcon = icon;
      });
    });
    categoryPrices!['moto'] = 0;
    categoryPrices!['torito'] = 0;
    categoryPrices!['taxi'] = 0;
    categoryPrices!['xl_+_4_personas'] = 0;
    categoryPricesShow!['moto'] = 0;
    categoryPricesShow!['torito'] = 0;
    categoryPricesShow!['taxi'] = 0;
    categoryPricesShow!['xl_+_4_personas'] = 0;

    jobRepository = JobRepository();
    tripDirectionDetails = DirectionDetails();
    init();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> checkForUpdates() async {
    // Lógica para verificar actualizaciones
    bool needsUpdate = await UpdateChecker.checkForUpdates();
    if (needsUpdate) {
      showUpdateDialog(); // Mostrar diálogo de actualización
    }
    print(needsUpdate);
  }

  void showUpdateDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Actualización disponible'),
          content: Text('Hay una nueva versión disponible. ¿Deseas actualizar?'),
          actions: <Widget>[
            TextButton(
              child: Text('Actualizar'),
              onPressed: () {
                Navigator.pop(context);
                launchAppStore(); // Abrir la tienda de aplicaciones para actualizar
              },
            ),
          ],
        );
      },
    );
  }

  void launchAppStore() async {
    const url = 'https://play.google.com/store/apps/details?id=sinet.startup.inDriver&hl=es_BO&gl=US'; // Reemplaza con la URL de tu tienda de aplicaciones

    if (await canLaunch(url)) {
      await launch(url);
      SystemNavigator.pop(); // Cerrar la aplicación después de redirigir a la tienda
    } else {
      print('No se pudo abrir la URL');
    }
  }

  Future<void> init() async {
    Position position = await _getLocation();
    currentPosition = GeoFirePoint(position.latitude, position.longitude);
    currentUser = await getCurrentUser();
    await _getMarkersFromFirestore();
    //currentUser = await getCurrentUser();
    //print(currentUser);
    getUserRides();
    checkPendingRequest();
    AssistantMehods.getCarrerasTomadas(context, currentUser.id!);
    codigoPais = await AssistantMehods.getCountryFromCoordinates(currentPosition.latitude, currentPosition.longitude);
    print(codigoPais);
  }

  Future<void> checkPendingRequest() async {
    RideRequest rideRequest;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? pendingDocId = prefs.getString(RIDEID);
    if (pendingDocId != null && pendingDocId != '') {
      // Si hay una solicitud pendiente, recuperar los detalles y reanudar el proceso
      print(pendingDocId);
      setState(() {
        docId = pendingDocId;
        //_isButtonDisabled = false;
      });

      // Obtener los datos de la solicitud pendiente
      DocumentSnapshot<Object?> snapshot = await _rideRequestsCollection.doc(docId).get();

      if (snapshot.exists) {
        Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
        rideRequest = RideRequest.fromMap(data['rideInfoMap']);

        solicitudEnviada = true;
        timer = Timer(Duration(minutes: 5), () async {
          print('timer');
          if (solicitudEnviada) {
            // Eliminar la solicitud si nadie la ha aceptado después de 15 minutos

            _rideRequestsCollection.doc(docId).delete();
            _offersSubscription.cancel();
            rideInfoSubscription.cancel();
            resetApp();
          }
        });

        setState(() {
          precioSolicitud = double.parse(rideRequest.tarifaSugerida);
          precioMinimo = precioSolicitud;
        });
        double latitude = double.parse(rideRequest.pickUpLocMap['latitude']!);
        double longitude = double.parse(rideRequest.pickUpLocMap['longitude']!);

        Position cPosition = Position(
          latitude: latitude,
          longitude: longitude,
          timestamp: null,
          accuracy: 0.0,
          altitude: 0.0,
          heading: 0.0,
          speed: 0.0,
          speedAccuracy: 0.0,
        );

        LatLng position = LatLng(double.parse(rideRequest.dropOffLocMap['latitude']!), double.parse(rideRequest.dropOffLocMap['longitude']!));

        _cancelSubscription();
        await locatePosition();
        await setInfoDropOff(position.latitude, position.longitude, context);
        Marker marker = Marker(
          markerId: MarkerId(position.toString()),
          position: position,
          infoWindow: InfoWindow(title: 'Destino'),
        );

        setState(() {
          _markers.clear();
          _markers.add(marker);
        });

        _calculateRoute(cPosition, position);

        ///escucho rideRequest
        rideInfoSubscription = listenRideInfo(docId);

        ///escuchar oferta de conductores
        _offersSubscription = listenOffersDrivers(docId);

        setState(() {
          //isVisiblePositioned1 = true;
        });
        //_calculatePriceAndTime("económico");

        //muestro positioned2
        setState(() {
          _mapTapDisabled = true; // establecer la bandera de bloqueo después de manejar el evento
          isVisiblePositioned1 = false;
          isVisiblePositioned3 = false;
          isVisiblePositioned4 = false;
          isVisiblePositioned2 = true;
        });

        // y continuar con el proceso de solicitud como lo haces normalmente.
      }
    }
  }

  Future<void> locatePosition() async {
    Position position = await _getLocation();
    currentPosition = GeoFirePoint(position.latitude, position.longitude);
    LatLng latLatPosition = LatLng(currentPosition.latitude, currentPosition.longitude);
    //setea pickup
    String address = await AssistantMehods.searchCoordinateAddress(LatLng(currentPosition.latitude, currentPosition.longitude), context);

    CameraPosition cameraPosition = new CameraPosition(target: latLatPosition, zoom: 14);
    mapController!.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  Future<Position> _getLocation() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    return position;
  }

  void _cancelSubscription() {
    _geoQuerySubscription?.cancel();
    _geoQuerySubscription = null;
  }

  Future<void> _getMarkersFromFirestore() async {
    Position position = await _getLocation();
//    currentPosition = GeoFirePoint(position.latitude, position.longitude);
    GeoFirePoint center = _geoFlutterFire.point(latitude: position.latitude, longitude: position.longitude);
    var collectionReference = _firestore.collection('availableDrivers');
    var geoQuery = _geoFlutterFire.collection(collectionRef: collectionReference).within(center: center, radius: radius, field: 'position', strictMode: true);
    List<Marker> markers = [];
    _geoQuerySubscription = geoQuery.listen((List<DocumentSnapshot> documentList) {
      print('listener conductores');
      setState(() {
        _markers.clear();
      });
      documentList.forEach((doc) {
        var location = doc.get('position');
        var rot = MapKitAssistant.getMarkerRotation(
          _previousLocation?.latitude ?? 0.toDouble(),
          _previousLocation?.longitude ?? 0.toDouble(),
          position.latitude,
          position.longitude,
        );
        Marker marker = Marker(
          markerId: MarkerId(doc.id),
          position: LatLng(location['geopoint'].latitude, location['geopoint'].longitude),
          icon: customIcon!,
          // Reemplazar con la imagen que desees utilizar
          rotation: rot,
          consumeTapEvents: true, // Deshabilitar eventos de toque
        );
        markers.add(marker);
      });
      setState(() {
        _markers = markers;
      });
    });
  }

  double _calculatePriceAndTime(String tipoVehiculo) {
    double? precioBase;
    double? precioPorKm;
    double? tiempoEsperaPorMinuto = 1.0; // Debes definir un valor para el tiempo de espera por minuto
    double precioEstimado;

    // Definir los precios y tarifas según el tipo de vehículo
    switch (tipoVehiculo) {
      case 'moto':
        precioBase = 1;
        precioPorKm = 1.5;
        break;
      case 'torito':
        precioBase = 4;
        precioPorKm = 3;
        break;
      case 'taxi':
        precioBase = 4;
        precioPorKm = 2.5;
        break;
      case 'premium':
        precioBase = 4;
        precioPorKm = 3.5;
        break;
    }

    if (tripDirectionDetails != null) {
      double distanciaKm = tripDirectionDetails!.distanceValue! / 1000;
      double tiempoMin = tripDirectionDetails!.durationValue!.toDouble();

      // Calcular el precio estimado y el tiempo estimado
      precioEstimado = precioBase! + (precioPorKm! * distanciaKm);
      double tiempoEstimado = tiempoMin + (_waitingTime * tiempoEsperaPorMinuto);

      // Actualizar el estado si lo deseas
      // setState(() {
      //   _estimatedPrice = precioEstimado;
      //   _estimatedTime = Duration(minutes: tiempoEstimado.round());
      // });

      // Devolver el precio estimado
      return precioEstimado;
    } else {
      // Si tripDirectionDetails es null, retornar 0 como precio estimado
      return 0;
    }
  }

  Future<void> setInfoDropOff(double latitude, double longitude, context) async {
    String placeDetailsUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=$mapKey";
    print(placeDetailsUrl);
    var res = await RequestAssistant.getRequest(placeDetailsUrl);

    if (res == "failed") {
      //Navigator.pop(context, "obtainDirection");
      return;
    }
    if (res["status"] == "OK") {
      Address address = Address();
      // address.placeName = res["result"]["name"];
      // address.latitude = res["result"]["geometry"]["location"]["lat"];
      // address.longitude = res["result"]["geometry"]["location"]["lng"];

      String st1 = res["results"][1]["address_components"][0]["long_name"];
      String st2 = res["results"][1]["address_components"][1]["short_name"];
      String placeAddress = st1 + ", " + st2;

      address.longitude = longitude;
      //userPickUpAddress.longitude = -63.206994;
      address.latitude = latitude;
      //userPickUpAddress.latitude = -17.827060;
      address.placeName = placeAddress;

      Provider.of<AppData>(context, listen: false).updateDropOffLocationAddress(address);

      //Navigator.pop(context, "obtainDirection");
    }
  }

  Future<void> _handleTap(LatLng position) async {
    if (!_mapTapDisabled) {
      setState(() {
        isBackButtonVisible = true;
        isSearchButtonVisible = false;
      });

      _cancelSubscription();
      await locatePosition();
      await setInfoDropOff(position.latitude, position.longitude, context);
      Marker marker = Marker(
        markerId: MarkerId(position.toString()),
        position: position,
        infoWindow: InfoWindow(title: 'Destino'),
      );

      setState(() {
        _markers.clear();
        _markers.add(marker);
      });
      Position cPosition = await _getLocation();
      _calculateRoute(cPosition, position);
      setState(() {
        isVisiblePositioned1 = true;
      });
      //_calculatePriceAndTime("económico");
    }
    _mapTapDisabled = true; // establecer la bandera de bloqueo después de manejar el evento
  }

  Future<void> _taxiALaUbicacion(Position currentPosition) async {
    if (!_mapTapDisabled) {
      _cancelSubscription();
      await locatePosition();
      //await setInfoDropOff(currentPosition.latitude, currentPosition.longitude, context);
      Marker marker = Marker(
        markerId: MarkerId(currentPosition.toString()),
        position: LatLng(currentPosition.latitude, currentPosition.longitude),
      );

      setState(() {
        _markers.clear();
        _markers.add(marker);
      });
      // Position cPosition = await _getLocation();
      // _calculateRoute(cPosition, LatLng(currentPosition.latitude, currentPosition.longitude));
      setState(() {
        isVisiblePositioned1 = true;
      });
      //_calculatePriceAndTime("económico");
    }
    _mapTapDisabled = true; // establecer la bandera de bloqueo después de manejar el evento
  }

  Future<void> _searchHandleTap(LatLng destino) async {
    setState(() {
      isBackButtonVisible = true;
      isSearchButtonVisible = false;
    });

    LatLng position = destino;
    if (!_mapTapDisabled) {
      _cancelSubscription();

      Marker marker = Marker(
        markerId: MarkerId(position.toString()),
        position: position,
        infoWindow: InfoWindow(title: 'Destino'),
      );

      setState(() {
        _markers.clear();
        _markers.add(marker);
      });
      Position cPosition = await _getLocation();
      _calculateRoute(cPosition, position);
      setState(() {
        isVisiblePositioned1 = true;
      });
      //_calculatePriceAndTime("económico");
    }
    _mapTapDisabled = true; // establecer la bandera de bloqueo después de manejar el evento
  }

  Future<void> _calculateRoute(Position origin, LatLng destination) async {
    var details = await AssistantMehods.obtainPlaceDirectionDetails(origin, destination);

    setState(() {
      tripDirectionDetails = details;
      categoryPrices!['moto'] = _calculatePriceAndTime("moto");
      categoryPricesShow!['moto'] = redondeoPorDefecto(categoryPrices!['moto']!);
      categoryPrices!['torito'] = _calculatePriceAndTime("torito");
      categoryPricesShow!['torito'] = redondeoPorDefecto(categoryPrices!['torito']!);
      categoryPrices!['taxi'] = _calculatePriceAndTime("taxi");
      categoryPricesShow!['taxi'] = redondeoPorDefecto(categoryPrices!['taxi']!);
      categoryPrices!['xl_+_4_personas'] = _calculatePriceAndTime("premium");
      categoryPricesShow!['xl_+_4_personas'] = redondeoPorDefecto(categoryPrices!['xl_+_4_personas']!);
    });

    PolylinePoints polylinePoints = PolylinePoints();
    List<PointLatLng> decodePolyLinePointsResult = polylinePoints.decodePolyline(details.encodePoints!);

    List<LatLng> polylineCoordinates = [];
    if (decodePolyLinePointsResult.isNotEmpty) {
      decodePolyLinePointsResult.forEach((PointLatLng pointLatlng) {
        polylineCoordinates.add(LatLng(pointLatlng.latitude, pointLatlng.longitude));
      });
    }

    setState(() {
      Polyline polyline = Polyline(
        color: Colors.pink,
        polylineId: PolylineId("PolylineID"),
        jointType: JointType.round,
        points: polylineCoordinates,
        width: 5,
        startCap: Cap.roundCap,
        endCap: Cap.roundCap,
        geodesic: true,
      );
      polYlineSet.clear();
      polYlineSet.add(polyline);
      centerMapOnPolyline(polylineCoordinates);
    });
  }

  LatLngBounds getLatLngBounds(List<LatLng> polylineCoordinates) {
    double minLat = double.infinity;
    double maxLat = -double.infinity;
    double minLng = double.infinity;
    double maxLng = -double.infinity;

    for (LatLng coordinate in polylineCoordinates) {
      if (coordinate.latitude < minLat) minLat = coordinate.latitude;
      if (coordinate.latitude > maxLat) maxLat = coordinate.latitude;
      if (coordinate.longitude < minLng) minLng = coordinate.longitude;
      if (coordinate.longitude > maxLng) maxLng = coordinate.longitude;
    }

    return LatLngBounds(
      southwest: LatLng(minLat, minLng),
      northeast: LatLng(maxLat, maxLng),
    );
  }

  void centerMapOnPolyline(List<LatLng> polylineCoordinates) {
    if (polylineCoordinates.isEmpty) return;

    LatLngBounds bounds = getLatLngBounds(polylineCoordinates);
    mapController!.animateCamera(CameraUpdate.newLatLngBounds(bounds, 50.0));
  }

  void enviarSolicitudTaxiALaUbicacion() async {
    setState(() {
      isBackButtonVisible = true;
    });
    // Obtener la ubicación actual del pasajero
    isTaxiALaUbicacion = true;
    Position currentLocation = await _getLocation();

    // Llamar a la función enviarSolicitud con la ubicación actual y tipo de vehículo
    _taxiALaUbicacion(currentLocation);
  }

  Future<void> solicitarTransportePublico() async {
    // if (destino != null) {
    //   // Aquí puedes realizar acciones relacionadas con la solicitud de transporte público.
    //   // Por ejemplo, puedes mostrar un diálogo de confirmación o iniciar el proceso de solicitud.
    //
    //   // También puedes utilizar la posición de destino (destino) para cualquier otro propósito necesario.
    //   // Ejemplo: envío de la ubicación a un servidor para buscar rutas de transporte público.
    // } else {
    //   // Si no se ha seleccionado un destino, puedes mostrar un mensaje de error o proporcionar alguna retroalimentación al usuario.
    //   print("Por favor selecciona un destino antes de solicitar el transporte público.");
    // }
    print(currentPosition);
    LatLng posicion = LatLng(currentPosition.latitude, currentPosition.longitude);
    print(posicion.toString());
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MapSample(initialPosition: posicion),
      ),
    );
    resetApp();
  }
  Future<void> solicitarDelivery() async {
    // if (destino != null) {
    //   // Aquí puedes realizar acciones relacionadas con la solicitud de transporte público.
    //   // Por ejemplo, puedes mostrar un diálogo de confirmación o iniciar el proceso de solicitud.
    //
    //   // También puedes utilizar la posición de destino (destino) para cualquier otro propósito necesario.
    //   // Ejemplo: envío de la ubicación a un servidor para buscar rutas de transporte público.
    // } else {
    //   // Si no se ha seleccionado un destino, puedes mostrar un mensaje de error o proporcionar alguna retroalimentación al usuario.
    //   print("Por favor selecciona un destino antes de solicitar el transporte público.");
    // }
    // print(currentPosition);
    // LatLng posicion = LatLng(currentPosition.latitude, currentPosition.longitude);
    // print(posicion.toString());
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => HomePage(),
      ),
    );
    resetApp();
  }
  @override
  Widget build(BuildContext context) {
    UserModel? userModel = Provider.of<UserProvider>(context).userModel;
    if (userModel != null) {
    } else {
      // Manejo en caso de que userModel sea nulo
    }
    return WillPopScope(
      child: Scaffold(
          appBar: AppBar(
            title: Text('Pedir taxi'),
            //backgroundColor: primary,
            actions: [
              _showOffersList
                  ? IconButton(
                      icon: Icon(Icons.arrow_drop_up),
                      onPressed: () {
                        setState(() {
                          isVisiblePositioned3 = !isVisiblePositioned3;
                          isSearchButtonVisible = !isSearchButtonVisible;
                        });
                        if (!isVisiblePositioned3) {
                          setState(() {
                            isVisiblePositioned2 = true;
                          });
                        } else {
                          setState(() {
                            isVisiblePositioned2 = false;
                          });
                        }
                      },
                    )
                  : isBackButtonVisible
                      ? IconButton(
                          icon: Icon(Icons.arrow_back),
                          onPressed: () {
                            if (solicitudEnviada) {
                              cancelarCarrera();
                            } else {
                              resetApp();
                            }
                          },
                        )
                      : Row(),
            ],
          ),
          drawer: userModel != null ? MenuWidget(modelUser: userModel!, cancelSubscriptionCallback: _cancelSubscription) : CircularProgressIndicator(),
          body: Stack(
            children: [
              GoogleMap(
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                compassEnabled: true,
                rotateGesturesEnabled: true,
                polylines: polYlineSet,
                initialCameraPosition: _kGooglePlex,
                markers: Set<Marker>.of(_markers),
                onMapCreated: (GoogleMapController controller) async {
                  _controllerGoogleMap.complete(controller);
                  mapController = controller;
                  await locatePosition();
                },
                onTap: (position) {
                  print('handle');
                  _handleTap(position);
                },
              ),

              ///boton buscador
              Visibility(
                visible: isSearchButtonVisible,
                child: Positioned(
                  top: 10,
                  left: 16.0,
                  child: Column(
                    children: [
                      FloatingActionButton(
                        heroTag: "boton_buscar",
                        onPressed: () async {
                          var res = await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => SearchScreen(),
                            ),
                          );

                          if (res != null && res is List<dynamic> && res.length == 2) {
                            String direction = res[0];
                            Address address = res[1];
                            // Aquí puedes usar los datos recibidos como direction y address.
                            // Por ejemplo, imprimirlos o pasarlos a otra función para procesarlos.
                            print("Direction: $direction");
                            print("Address: ${address.placeName}");
                            LatLng destino = LatLng(address.latitude!, address.longitude!);
                            _searchHandleTap(destino);
                          }
                        },
                        child: Icon(Icons.search),
                      ),
                    ],
                  ),
                ),
              ),

              ///boton bus
              Visibility(
                visible: isSearchButtonVisible,
                child: Positioned(
                  bottom: 16.0,
                  left: 16.0,
                  child: Column(
                    children: [
                      Container(
                        width: 80.0, // Ajusta el ancho del botón
                        height: 80.0, // Ajusta la altura del botón
                        child: FloatingActionButton(
                          heroTag: "boton_solicitud_bus",
                          onPressed: () async {
                            _geoQuerySubscription?.cancel();
                            setState(() {
                              _markers.clear();
                              polYlineSet.clear();
                            });
                            //detener escucha conductore
                            solicitarTransportePublico();
                            // Aquí puedes agregar la lógica para manejar la solicitud de bus a la ubicación
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.directions_bus, // Cambia el ícono a un bus
                                size: 40.0, // Ajusta el tamaño del ícono
                              ),
                              Text('Bus'), // Agrega un texto dentro del botón
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
          ///boton delivery
          Visibility(
            visible: isSearchButtonVisible,
            child: Positioned(
              bottom: 110.0,
              left: 16.0,
              child: Column(
                children: [
                  Container(
                    width: 80.0, // Ajusta el ancho del botón
                    height: 80.0, // Ajusta la altura del botón
                    child: FloatingActionButton(
                      heroTag: "Delivery",
                      onPressed: () async {
                        _geoQuerySubscription?.cancel();
                        setState(() {
                          _markers.clear();
                          polYlineSet.clear();
                        });
                        //detener escucha conductore
                        solicitarDelivery();
                        // Aquí puedes agregar la lógica para manejar la solicitud de bus a la ubicación
                      },
                      child: ClipOval(
                        child: Image.asset(
                          "images/delivery.jpg",
                          width: 70.0, // Ajusta el ancho de la imagen
                          height: 70.0, // Ajusta la altura de la imagen
                          fit: BoxFit.cover, // Esto hará que la imagen se ajuste al botón
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),


          isVisiblePositioned1
                  ? Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(width: 10),
                              Expanded(
                                child: Container(
                                  color: Colors.white,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(10, 5, 10, 0), // Ajusta el padding según tu preferencia
                                            child: Row(
                                              children: [
                                                Image.asset('images/icono_deseo.png', height: 15, width: 15), // Reemplaza con la ruta de tu imagen
                                                SizedBox(width: 5), // Espacio entre el icono y el texto
                                                Text('Nota o deseo'), // Reemplaza con el texto deseado
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 5), // Espacio entre el primer Row y el TextField
                                      Row(
                                        children: [
                                          Expanded(
                                            child: TextField(
                                              maxLines: 1,
                                              textInputAction: TextInputAction.newline,
                                              controller: textController,
                                              decoration: InputDecoration(
                                                hintText: 'Ingresa texto aquí',
                                                filled: true,
                                                fillColor: Colors.black12,
                                              ),
                                              onChanged: (value) {
                                                print(value);
                                                deseo = value;
                                              },
                                              onEditingComplete: () {
                                                print('terminar');
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(width: 10),
                            ],
                          ),
                          AnimatedSize(
                            curve: Curves.bounceIn,
                            duration: Duration(milliseconds: 160),
                            child: Container(
                              height: 230,
                              decoration: BoxDecoration(
                                color: primary,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(16.0),
                                  topRight: Radius.circular(16.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black,
                                    blurRadius: 16.0,
                                    spreadRadius: 0.5,
                                    offset: Offset(0.7, 0.7),
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 10, 15, 10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center, // Alinea los elementos al centro
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          flex: 9,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              buildCategoryItem("Moto taxi", "images/moto.png", "moto"),
                                              buildDivider(),
                                              buildCategoryItem("Torito", "images/torito.png", "torito"),
                                              buildDivider(),
                                              buildCategoryItem("Taxi", "images/ubergo.png", "taxi"),
                                              buildDivider(),
                                              buildCategoryItem("XL + 4 personas", "images/xl_+_4_pasajeros.png", "xl_+_4_personas"),
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10),
                                        Expanded(
                                          flex: 3,
                                          child: Container(
                                            height: 150,
                                            child: Column(
                                              children: [
                                                buildInfoItem("Distancia", tripDirectionDetails.distanceText ?? ""),
                                                buildInfoItem("Tiempo", tripDirectionDetails.durationText ?? ""),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ))
                  : isVisiblePositioned2
                      ?

                      ///buscando chofer
                      Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(16.0),
                                topRight: Radius.circular(16.0),
                              ),
                              color: secundary,
                              boxShadow: [
                                BoxShadow(
                                  spreadRadius: 0.5,
                                  blurRadius: 16.0,
                                  color: Colors.black54,
                                  offset: Offset(0.7, 0.7),
                                ),
                              ],
                            ),
                            height: 150,
                            child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Column(
                                  children: [
                                    Center(
                                      child: LayoutBuilder(
                                        builder: (BuildContext context, BoxConstraints constraints) {
                                          double availableWidth = constraints.maxWidth;
                                          double buttonSize = availableWidth * 0.15; // Adjust the button size based on available width

                                          return Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: <Widget>[
                                              FloatingActionButton(
                                                heroTag: "btn1",
                                                onPressed: minus,
                                                child: Icon(
                                                  Icons.remove,
                                                  color: red,
                                                ),
                                                backgroundColor: primary,
                                                mini: true,
                                                // Make the button smaller
                                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap, // Remove padding
                                              ),
                                              SizedBox(width: buttonSize * 0.2), // Add spacing between buttons
                                              Expanded(
                                                child: Text(
                                                  precioSolicitud.toStringAsFixed(2) + " Bs.",
                                                  textAlign: TextAlign.center,
                                                  maxLines: 1, // Display in a single line
                                                  overflow: TextOverflow.ellipsis, // Handle long text with ellipsis
                                                  style: TextStyle(fontSize: buttonSize * 0.8, color: terciary // Adjust text size based on button size
                                                      ),
                                                ),
                                              ),
                                              SizedBox(width: buttonSize * 0.2), // Add spacing between buttons
                                              FloatingActionButton(
                                                heroTag: "btn2",
                                                onPressed: add,
                                                child: Icon(
                                                  Icons.add,
                                                  color: red,
                                                ),
                                                backgroundColor: primary,
                                                mini: true,
                                                // Make the button smaller
                                                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap, // Remove padding
                                              ),
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    _isButtonDisabled
                                        ? ElevatedButton(
                                            child: Text('Subir tarifa'),
                                            onPressed: () {
                                              setState(() {
                                                _isButtonDisabled = false;
                                              });
                                              _rideRequestsCollection.doc(docId).update({'rideInfoMap.tarifa_sugerida': precioSolicitud.toString()});
                                            },
                                            onLongPress: () {
                                              print('Long press');
                                            },
                                            style: ButtonStyle(
                                              backgroundColor: MaterialStateProperty.all(_isButtonDisabled ? green : grey),
                                            ))
                                        : Expanded(
                                            child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              // Image.asset(
                                              //   "images/google-maps.gif",
                                              //   width: 100,
                                              // ),
                                              ElevatedButton(
                                                  child: Text('Cancelar'),
                                                  onPressed: () async {
                                                    // rideInfoSubscription.cancel();
                                                    // _offersSubscription.cancel();
                                                    // eliminarCarrera();
                                                    // resetApp();
                                                    cancelarCarrera();
                                                  },
                                                  onLongPress: () {
                                                    print('Long press');
                                                  },
                                                  style: ButtonStyle(
                                                    backgroundColor: MaterialStateProperty.all(primary),
                                                    //padding: MaterialStateProperty.all(EdgeInsets.all(50)),
                                                    //textStyle: MaterialStateProperty.all(TextStyle(fontSize: 5))),
                                                  )),
                                            ],
                                          )),
                                  ],
                                )),
                          ),
                        )
                      : isVisiblePositioned3
                          ?

                          ///oferta conductores
                          Positioned(
                              top: 0,
                              left: 0,
                              right: 0,
                              bottom: 0,
                              child: isVisiblePositioned3
                                  ?

                                  ///ofertas conductores
                                  ListView.builder(
                                      itemCount: _offers.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        Oferta offer = _offers[index];

                                        if (offer.isExpired) {
                                          //_offers.removeAt(index);
                                          return SizedBox.shrink(); // No mostrar la oferta si ha expirado
                                        }

                                        return Card(
                                          elevation: 4,
                                          child: Padding(
                                            padding: EdgeInsets.all(16.0),
                                            child: Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: 60.0,
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      CircleAvatar(
                                                        radius: 20.0,
                                                        backgroundImage:
                                                            NetworkImage("https://img.freepik.com/vector-premium/icono-circulo-usuario-anonimo-ilustracion-vector-estilo-plano-sombra_520826-1931.jpg"),
                                                      ),
                                                      SizedBox(height: 8.0),
                                                      Text(
                                                        '${offer.conductorNombre}',
                                                        maxLines: 1,
                                                        overflow: TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                          fontWeight: FontWeight.bold,
                                                          fontSize: 12.0,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(width: 12.0),
                                                Expanded(
                                                  flex: 3,
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        'Vehículo:',
                                                        style: TextStyle(
                                                          fontWeight: FontWeight.bold,
                                                          color: Colors.grey,
                                                        ),
                                                      ),
                                                      Text(
                                                        '${offer.conductorModeloVehiculo}',
                                                        style: TextStyle(
                                                          fontSize: 16.0,
                                                          fontWeight: FontWeight.bold,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                      SizedBox(height: 6.0),
                                                      Row(
                                                        children: [
                                                          Text(
                                                            'Color:',
                                                            style: TextStyle(
                                                              fontWeight: FontWeight.bold,
                                                              color: Colors.grey,
                                                            ),
                                                          ),
                                                          SizedBox(width: 6.0),
                                                          Text(
                                                            '${offer.conductorColorVehiculo}',
                                                            style: TextStyle(
                                                              fontSize: 16.0,
                                                              fontWeight: FontWeight.bold,
                                                              color: Colors.black,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        children: [
                                                          Text(
                                                            'Precio:',
                                                            style: TextStyle(
                                                              fontWeight: FontWeight.bold,
                                                              color: Colors.grey,
                                                            ),
                                                          ),
                                                          SizedBox(width: 6.0),
                                                          Text(
                                                            '${offer.tarifaOfertada.toStringAsFixed(2)}',
                                                            style: TextStyle(
                                                              fontSize: 16.0,
                                                              fontWeight: FontWeight.bold,
                                                              color: Colors.red,
                                                            ),
                                                          ),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(width: 12.0),
                                                Expanded(
                                                  flex: 2,
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Row(
                                                        children: [
                                                          SizedBox(width: 6.0),
                                                          CountdownTimer(
                                                            initialSeconds: offer.durationInSeconds,
                                                            onTimeout: () {
                                                              setState(() {
                                                                offer.setExpired();
                                                              });
                                                              if (_offers.every((offer) => offer.isExpired)) {
                                                                setState(() {
                                                                  isBackButtonVisible = true;
                                                                  _showOffersList = false;
                                                                  isVisiblePositioned2 = true;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(height: 8.0),
                                                      ElevatedButton(
                                                        onPressed: () {
                                                          timer?.cancel();
                                                          acceptOffer(offer);
                                                        },
                                                        child: Text(
                                                          'Aceptar',
                                                          style: TextStyle(
                                                            fontWeight: FontWeight.bold,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    )
                                  : Container(
                                      child: Text("no hay conductore"),
                                    ),
                            )
                          : isVisiblePositioned4
                              ? //Display AssiGned Driver Info
                              Positioned(
                                  bottom: 0.0,
                                  left: 0.0,
                                  right: 0.0,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(16.0),
                                        topRight: Radius.circular(16.0),
                                      ),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          spreadRadius: 0.5,
                                          blurRadius: 16.0,
                                          color: Colors.black54,
                                          offset: Offset(0.7, 0.7),
                                        ),
                                      ],
                                    ),
                                    height: 210,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 0.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          SizedBox(
                                            height: 20.0,
                                          ),
                                          //fila codigo conductor
                                          Expanded(
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  "CODIGO CONDUCTOR",
                                                  style: TextStyle(fontSize: 14.0, color: Colors.black38),
                                                ),
                                                //codigo conductor
                                                Expanded(
                                                  child: Text(
                                                    datosConductor == null ? "" : datosConductor!.code.toString(),
                                                    style: TextStyle(fontSize: 40.0, color: Colors.red, fontFamily: 'Brand Bold'),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5.0,
                                          ),

                                          //ride status
                                          Container(
                                            height: 40,
                                            decoration: BoxDecoration(
                                              color: secundary,
                                              borderRadius: BorderRadius.circular(20), // Define el radio de las esquinas
                                            ),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  statusRide,
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(fontSize: 15.0, fontFamily: "Brand Bold", color: Colors.white),
                                                ),
                                              ],
                                            ),
                                          ),

                                          SizedBox(
                                            height: 10.0,
                                          ),

                                          Container(
                                            height: 60,
                                            color: Colors.white,
                                            child: Row(
                                              children: [
                                                //logo zmovil
                                                Container(
                                                  child: Expanded(
                                                    child: Image.asset(
                                                      "images/zdrive.png",
                                                      height: 40,
                                                    ),
                                                    flex: 2,
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 8,
                                                  child: Container(
                                                    alignment: Alignment.topLeft,
                                                    child: Column(
                                                      children: [
                                                        Expanded(
                                                          child: Column(
                                                            children: [
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: [
                                                                  Text(
                                                                    'Conductor: ',
                                                                    style: TextStyle(color: Colors.red, fontFamily: "Brand Bold"),
                                                                  ),
                                                                  Text(
                                                                    (datosConductor?.name ?? '').toUpperCase(),
                                                                    style: TextStyle(fontSize: 15.0, fontFamily: "Brand Bold", color: Colors.black),
                                                                  ),
                                                                ],
                                                              ),
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: [
                                                                  Text(
                                                                    'Vehiculo: ',
                                                                    style: TextStyle(color: Colors.red, fontFamily: "Brand Bold"),
                                                                  ),
                                                                  Text(
                                                                    (datosConductor?.carModel ?? '') + " - " + (datosConductor?.carColor ?? ''),
                                                                    style: TextStyle(fontSize: 15.0, fontFamily: "Brand Bold", color: Colors.black),
                                                                  ),
                                                                ],
                                                              ),
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: [
                                                                  Text(
                                                                    'Placa: ',
                                                                    style: TextStyle(color: Colors.red, fontFamily: "Brand Bold"),
                                                                  ),
                                                                  Text(
                                                                    (datosConductor?.carNumber ?? '').toUpperCase(),
                                                                    style: TextStyle(fontSize: 15.0, fontFamily: "Brand Bold", color: Colors.black),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    openwhatsapp('');
                                                  },
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        height: 50.0,
                                                        width: 50.0,
                                                        child: Image.asset("images/whatsapp.png"),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                          SizedBox(
                                            height: 10.0,
                                          ),
                                          //botones whatsapp, detalles, cancelar.
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : Container(),
            ],
          )),
      onWillPop: () async {
        //await _handleCancelarButton();

        if (statusRide == "") {
          if (solicitudEnviada) {
            cancelarCarrera();
          }
          if (isBackButtonVisible) {
            //rideInfoSubscription.cancel();
            //_offersSubscription.cancel();
            //eliminarCarrera();
            resetApp();
          }
        } else {
          print(statusRide);
        }
        return false; // Permite que el retroceso se produzca
      },
    );
  }

  Widget buildCategoryItem(String title, String imagePath, String category) {
    return GestureDetector(
      onTap: () {
        enviarSolicitud(category);
      },
      child: Container(
        width: double.infinity,
        color: primary,
        height: 50,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            children: [
              Image.asset(
                imagePath,
                height: 70.0,
                width: 80.0,
              ),
              SizedBox(
                width: 16.0,
              ),
              Text(
                title,
                style: TextStyle(
                  fontSize: 15.0,
                  fontFamily: "Brand Bold",
                ),
              ),
              Expanded(child: Container()),
              Text(
                categoryPrices != null ? (categoryPricesShow![category]!.toString() + " Bs.") : " Bs.",
                style: TextStyle(fontFamily: "Brand Bold"),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildDivider() {
    return Divider(height: 2.0, thickness: 2.0);
  }

  Widget buildInfoItem(String label, String value) {
    return Expanded(
      flex: 2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 90,
            padding: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              color: secundary,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              children: [
                Text(
                  label,
                  style: TextStyle(
                    fontSize: 13.0,
                    color: primary,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 5.0),
                Text(
                  value,
                  style: TextStyle(
                    fontSize: 18.0,
                    color: terciary,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  int redondeoPorDefecto(double numero) {
    int parteEntera = numero.toInt();
    double parteDecimal = numero - parteEntera.toDouble();

    if (parteDecimal >= 0.5) {
      return parteEntera + 1;
    } else {
      return parteEntera;
    }
  }

  void cancelarCarrera() {
    timer?.cancel();
    rideInfoSubscription.cancel();
    _offersSubscription.cancel();
    eliminarCarrera();
    resetApp();
  }

  Future<void> eliminarCarrera() async {
    if (docId != "") {
      _rideRequestsCollection.doc(docId).delete();
    }
  }

  Future<void> resetApp() async {
    textController.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(ESTADO, '');
    prefs.setString(RIDEID, '');
    setState(() {
      precioSolicitud = 0.0;
      _isButtonDisabled = false;
      categoryPricesShow!['moto'] = 0;
      categoryPricesShow!['torito'] = 0;
      categoryPricesShow!['taxi'] = 0;
      categoryPricesShow!['xl_+_4_personas'] = 0;
      isTaxiALaUbicacion = false;
      isBackButtonVisible = false;
      isSearchButtonVisible = true;
      driverId = "";
      _offers = [];
      _mapTapDisabled = false;
      _showOffersList = false;
      _isInitializedPositionStreamSuscription = false;
      isVisiblePositioned1 = false;
      isVisiblePositioned2 = false;
      isVisiblePositioned3 = false;
      isVisiblePositioned4 = false;
      subtipoVehiculo = "";
      deseo = '';
      polYlineSet.clear();
      tripDirectionDetails = DirectionDetails();
      statusRide = "";
      isPlayerAlarm = false;
    });
    _getMarkersFromFirestore();
  }

  bool sonListasIguales(List<Oferta> lista1, List<Oferta> lista2) {
    if (lista1.length != lista2.length) {
      return false;
    }

    // Crear listas de conductorId y tarifaOfertada
    List<String> conductorIds1 = lista1.map((oferta) => oferta.conductorId).toList();
    List<String> conductorIds2 = lista2.map((oferta) => oferta.conductorId).toList();
    List<double> tarifas1 = lista1.map((oferta) => oferta.tarifaOfertada).toList();
    List<double> tarifas2 = lista2.map((oferta) => oferta.tarifaOfertada).toList();

    // Ordenar las listas de conductorIds y tarifas
    conductorIds1.sort();
    conductorIds2.sort();
    tarifas1.sort();
    tarifas2.sort();

    // Comparar si las listas de conductorIds y tarifas son iguales
    return ListEquality().equals(conductorIds1, conductorIds2) && ListEquality().equals(tarifas1, tarifas2);
  }

  Future<void> getUserRides() async {
    final userRef = FirebaseFirestore.instance.collection('users').doc(auth.currentUser!.uid);
    final userSnapshot = await userRef.get();
    final allUserRides = Map<String, dynamic>.from(userSnapshot.data()?['rides'] ?? {});
    final falseRides = allUserRides.entries.where((entry) => entry.value == false).map((entry) => entry.key).toList();
    if (!falseRides.isEmpty) {
      docId = falseRides.first;
      // Obtener la referencia al documento del viaje cancelado en la colección "historial"
      final canceledRideRef = FirebaseFirestore.instance.collection('historial').doc(docId);
      final canceledRideSnapshot = await canceledRideRef.get();

      // Verificar si el documento existe y obtener sus datos
      if (canceledRideSnapshot.exists) {
        Map<String, dynamic>? canceledRideData = canceledRideSnapshot.data();
        Map<String, dynamic>? rideInfoMap = canceledRideData!['rideInfoMap'];
        driverId = (rideInfoMap!['driver_id']);
        // Obtener la referencia al documento del conductor
        getInfoDriver(driverId);
        String? distanciaConductorPasajero = canceledRideData?['distancia_conductor_pasajero'];
        if (distanciaConductorPasajero != null) {
          setState(() {
            statusRide = "Taxi en camino - $distanciaConductorPasajero";
          });
        }
        isPendingRide = true;
        listenerHistorial(docId);

        _cancelSubscription();
        setState(() {
          _showOffersList = false;
          _mapTapDisabled = true;
          isVisiblePositioned4 = true;
          isVisiblePositioned3 = false;
          isVisiblePositioned1 = false;
          isVisiblePositioned2 = false;
          isSearchButtonVisible = false;
        });
      }
    }
  }

  String generateUniqueId() {
    // Obtiene la fecha y hora actual en milisegundos
    DateTime now = DateTime.now();
    int milliseconds = now.millisecondsSinceEpoch;

    // Genera un número aleatorio de 4 dígitos
    int random = Random().nextInt(9999);

    // Combina la fecha y hora con el número aleatorio para obtener un ID único
    String uniqueId = '$milliseconds$random';

    return uniqueId;
  }

  Map<String, dynamic> buildRideInfoMap(Address pickUp, Address dropOff, String tipoVehiculo) {
    Map pickUpLocMap = {
      "latitude": pickUp.latitude.toString(),
      "longitude": pickUp.longitude.toString(),
    };
    Map dropOffLocMap = {
      "latitude": isTaxiALaUbicacion ? '0.0' : dropOff.latitude.toString(),
      "longitude": isTaxiALaUbicacion ? '0.0' : dropOff.longitude.toString(),
    };
    return {
      "driver_id": "waiting",
      "payment_method": "cash",
      "pickup": pickUpLocMap,
      "dropOff": dropOffLocMap,
      "created_at": DateTime.now().toString(),
      "rider_id": currentUser.id,
      "rider_name": currentUser.name,
      "rider_phone": currentUser.phone,
      "pickup_address": pickUp.placeName,
      "dropoff_address": isTaxiALaUbicacion ? '' : dropOff.placeName,
      "distanceText": tripDirectionDetails!.distanceText,
      "durationText": tripDirectionDetails!.durationText,
      "tipo_vehiculo": tipoVehiculo,
      "subtipo_vehiculo": subtipoVehiculo,
      "carreras_tomadas": Provider.of<AppData>(context, listen: false).countTrips.toString(),
      "tarifa_sugerida": precioSolicitud.toString(),
      "tarifaOfertada": precioSolicitud.toString(),
      "deseo": deseo,
      'precio_aceptado': precioSolicitud,
      "status": 'solicitud_enviada',
      "conductores": [],
      "tipo_carrera": isTaxiALaUbicacion ? 'taxi_a_la_ubicacion' : 'normal',
    };
  }

  StreamSubscription<DocumentSnapshot<Map<String, dynamic>>> listenRideInfo(String docId) {
    return jobRepository.getRideInfo(docId).listen((DocumentSnapshot<Map<String, dynamic>> snapshot) {
      print('listener rideInfoSubscription');
      // Aquí puedes procesar los datos del documento
      Map<String, dynamic>? data = snapshot.data();
      if (data != null) {
        String? estado = data['rideInfoMap']?['status'];
        if (estado != null) {
          if (estado == 'aceptada') {
            setState(() {
              isBackButtonVisible = false;
            });
            driverId = data!['rideInfoMap']['driver_id'];
            print("driver_id: " + driverId);
            //recuperar informacion del conductor
            getInfoDriver(driverId);

            timer?.cancel();
            // Aquí puedes procesar los datos del documento
            _offersSubscription.cancel();
            rideInfoSubscription.cancel();
            // Guardar una copia de rideInfoMap
            rideInfoMapCopy = Map<String, dynamic>.from(data);
            setState(() {
              _showOffersList = false;
              isVisiblePositioned4 = true;
              isVisiblePositioned3 = false;
              isVisiblePositioned1 = false;
              isVisiblePositioned2 = false;
            });
            // Guarder la información del viaje en el historial
            //_historial = Historial(docId: docId, rideInfoMapCopy: rideInfoMapCopy!);
            actualizarHistorial(false);
            _rideRequestsCollection.doc(docId).delete();
            listenerHistorial(docId);
            rideInfoSubscription.cancel();
          }
        } else {
          print("'status' es nulo en 'rideInfoMap'");
        }
      } else {
        rideInfoSubscription.cancel();
        print("Datos nulos en la instantánea");
      }
    });
  }

  StreamSubscription<List<Oferta>> listenOffersDrivers(String docId) {
    return jobRepository.getOffersDrivers(docId).listen((List<Oferta> conductores) {
      print("longitud _conductores: " + conductores.length.toString());
      bool isUpdated = false;
      Oferta? ofertaUpdated; // Cambiado a tipo nullable
      Oferta? ofertaExisting; // Cambiado a tipo nullable

      // Si las listas tienen la misma longitud, verifica si hay ofertas con tarifas diferentes
      if (_offers.length == conductores.length) {
        for (int i = 0; i < _offers.length; i++) {
          Oferta ofertaExistente = _offers[i];

          // Buscar la oferta actualizada correspondiente
          print(conductores.length);
          // Buscar la oferta actualizada correspondiente
          var ofertasActualizadas = conductores.where((oferta) => oferta.conductorId == ofertaExistente.conductorId);
          if (ofertasActualizadas.isNotEmpty) {
            Oferta ofertaActualizada = ofertasActualizadas.first;

            if (ofertaExistente.tarifaOfertada != ofertaActualizada.tarifaOfertada) {
              print('Oferta actualizada: ${ofertaExistente.conductorId}');
              print('Tarifa actualizada: ${ofertaActualizada.tarifaOfertada}');
              ofertaExisting = ofertaExistente;
              ofertaUpdated = ofertaActualizada;
              isUpdated = true;

              // Realiza las acciones necesarias para la oferta actualizada
            }
          }
        }
      }

      if (_offers.length == conductores.length && sonListasIguales(_offers, conductores)) {
        print('Las listas son iguales');
      } else {
        setState(() {
          //_offers = conductores;
        });

        // Eliminación y Adición de Ofertas
        for (Oferta nuevaOferta in conductores) {
          var conductorId = nuevaOferta.conductorId;
          var ofertaExistente = _offers.any((oferta) => oferta.conductorId == conductorId);

          if (!ofertaExistente) {
            print('Nueva oferta ingresada: $conductorId');
            _offers.add(nuevaOferta);
            // Realiza las acciones necesarias para la nueva oferta
          }
        }

        if (isUpdated && ofertaExisting != null && ofertaUpdated != null) {
          _offers.remove(ofertaExisting);
          _offers.add(ofertaUpdated);
        }

        print('conductores: ' + conductores.length.toString());
        print('Terminó el bucle');
        print('conductores: ' + _offers.length.toString());

        //logia para mostrar/ocultar ofertas
        if (_offers.isNotEmpty) {
          setState(() {
            _showOffersList = true;
            isSearchButtonVisible = false;

            isVisiblePositioned3 = true;
            isVisiblePositioned1 = false;
            isVisiblePositioned2 = false;
          });
        } else {
          setState(() {
            _showOffersList = false;
            isVisiblePositioned3 = false;
            isVisiblePositioned1 = false;
            isVisiblePositioned2 = true;
          });
        }

        // Ofertas eliminadas
        for (Oferta ofertaSalida in _offers) {
          if (!conductores.contains(ofertaSalida)) {
            print('Oferta eliminada: ${ofertaSalida.toString()}');
            // Realiza las acciones necesarias para la oferta que salió
          }
        }
      }
    });
  }

  void enviarSolicitud(String tipoVehiculo) async {
    setState(() {
      isBackButtonVisible = false;
      solicitudEnviada = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // Establecer la variable solicitudEnviada en true
    var dropOff;

    timer = Timer(Duration(minutes: 5), () async {
      print('timer');
      if (solicitudEnviada) {
        // Eliminar la solicitud si nadie la ha aceptado después de 15 minutos

        _rideRequestsCollection.doc(docId).delete();
        _offersSubscription.cancel();
        rideInfoSubscription.cancel();
        resetApp();
      }
    });

    setState(() {
      print('precio solicitud: ' + precioSolicitud.toString());
      precioSolicitud = categoryPrices![tipoVehiculo]!;
      precioMinimo = precioSolicitud;
      print('precio solicitud: ' + precioSolicitud.toString());
    });

    Address pickUp = Provider.of<AppData>(context, listen: false).pickUpLocation;
    if (!isTaxiALaUbicacion) {
      dropOff = Provider.of<AppData>(context, listen: false).dropOffLocation;
    } else {
      setState(() {
        precioSolicitud = 0;
        precioMinimo = 0;
      });
      dropOff = Provider.of<AppData>(context, listen: false).pickUpLocation;
    }

    // Genera un identificador único para la solicitud
    String uniqueRequestId = generateUniqueId(); // Implementa tu propia lógica para generar un ID único

    // Calcula el hash del punto de recogida y el ID de solicitud
    GeoFirePoint point = _geoFlutterFire.point(latitude: pickUp.latitude!, longitude: pickUp.longitude!);
    docId = point.hash + uniqueRequestId;

    Map<String, dynamic> rideInfoMap = buildRideInfoMap(pickUp, dropOff, tipoVehiculo);

    // GeoFirePoint point = _geoFlutterFire.point(latitude: pickUp.latitude!, longitude: pickUp.longitude!);
    // docId = point.hash;

    Map<String, dynamic> data = {'position': point.data, 'rideInfoMap': rideInfoMap};
    await _rideRequestsCollection.doc(docId).set(data);
    prefs.setString(ESTADO, 'solicitud_enviada');
    prefs.setString(RIDEID, docId);

    ///escucho rideRequest
    rideInfoSubscription = listenRideInfo(docId);

    ///escuchar oferta de conductores
    _offersSubscription = listenOffersDrivers(docId);

    //muestro positioned2
    setState(() {
      isVisiblePositioned1 = false;
      isVisiblePositioned3 = false;
      isVisiblePositioned4 = false;
      isVisiblePositioned2 = true;
    });
  }

  void acceptOffer(Oferta offer) {
    // Verificar si el temporizador de la oferta aún está activo
    if (!offer.isExpired) {
      // Cancelar el temporizador de la oferta aceptada
      offer.cancelTimer();
      _offersSubscription.cancel();
      // Marcar que se ha aceptado una oferta
      isOfferAccepted = true;

      // Actualizar el estado de la carrera a "aceptada" en la base de datos
      _rideRequestsCollection.doc(docId).update({
        'rideInfoMap.status': 'aceptada',
        'rideInfoMap.driver_id': offer.conductorId,
        'rideInfoMap.precio_aceptado': offer.tarifaOfertada,
      });

      // Realizar acciones relacionadas con la aceptación de la oferta
      print('Oferta aceptada: ${offer.toString()}');
    } else {
      // El temporizador ha caducado para esta oferta, realiza acciones alternativas si es necesario
      print('El temporizador de la oferta ha caducado');
    }
  }

  void _updateOfferTimers(List<Oferta> newOffers) {
    // Actualizar los temporizadores de las ofertas directamente en las ofertas
    for (var offer in newOffers) {
      if (!offer.isTimerActive) {
        offer.startTimer(() {
          if (!isOfferAccepted) {
            // Acciones si el pasajero no acepta la oferta en 15 segundos
            print('Temporizador de aceptación de oferta expirado para oferta: ${offer.toString()}');

            // Eliminar la oferta expirada de la base de datos
            _deleteExpiredOffer(offer, docId);

            // Remover la oferta expirada de la lista de ofertas
            setState(() {
              _offers.remove(offer);
            });
            if (_offers.length == 0) {
              setState(() {
                isBackButtonVisible = true;
                _showOffersList = false;
                isVisiblePositioned2 = true;
              });
            }
          }
        });
      }
    }
  }

// Función para eliminar una oferta expirada de la base de datos
  void _deleteExpiredOffer(Oferta offer, String rideRequestId) async {
    try {
      // Obtiene la referencia al documento de la solicitud de viaje
      DocumentReference rideRequestRef = FirebaseFirestore.instance.collection('rideRequests').doc(rideRequestId);

      // Obtiene los conductores actuales desde Firestore
      DocumentSnapshot rideRequestSnapshot = await rideRequestRef.get();
      Map<String, dynamic> rideData = rideRequestSnapshot.data() as Map<String, dynamic>;
      Map<String, dynamic> rideInfoMap = rideData['rideInfoMap'] as Map<String, dynamic>;
      List<dynamic> conductores = rideInfoMap['conductores'] as List<dynamic>;

      // Encuentra y elimina la oferta expirada del array de conductores
      conductores.removeWhere((conductor) => conductor['conductor_id'] == offer.conductorId);

      // Actualiza el campo 'conductores' en Firestore
      await rideRequestRef.update({
        'rideInfoMap.conductores': conductores,
      });

      print('Oferta expirada eliminada de la base de datos: ${offer.conductorId}');
    } catch (error) {
      print('Error al eliminar la oferta expirada: $error');
    }
  }

  Future<UserModel> getCurrentUser() async {
    final User? user = auth.currentUser;
    if (user == null) {
      throw Exception("No hay usuario autenticado");
    }

    final DocumentSnapshot<Map<String, dynamic>> snapshot = await _firestore.collection('users').doc(user.uid).get();
    return UserModel.fromSnapshot(snapshot);
  }

  void minus() {
    if (precioSolicitud > precioMinimo) {
      setState(() {
        isBackButtonVisible = true;
        if (precioSolicitud != 0) _isButtonDisabled = true;
        precioSolicitud--;
      });
    } else {
      AssistantMehods.displayToastMessage('Tarifa no puede ser menor que tarifa sugerida', context);
    }
  }

  void add() {
    setState(() {
      isBackButtonVisible = true;
      _isButtonDisabled = true;
      precioSolicitud++;
    });

    print("precio solicitud: " + precioSolicitud.toString());
  }

  void openwhatsapp(String mensaje) async {
    try {
      String whatsapp = "+591" + datosConductor!.phone;
      print(whatsapp);

      var whatsappURl_android = "whatsapp://send?phone=" + whatsapp + "&text=" + mensaje;
      var whatappURL_ios = "https://wa.me/$whatsapp?text=${Uri.parse("hello")}";
      if (Platform.isIOS) {
        // for iOS phone only
        if (await canLaunchUrl(Uri.parse(whatappURL_ios))) {
          await launchUrl(
            Uri.parse(whatappURL_ios),
            mode: LaunchMode.externalApplication,
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("WhatsApp no instalado")));
        }
      } else {
        // android , web
        if (await canLaunchUrl(Uri.parse(whatsappURl_android))) {
          await launchUrl(
            Uri.parse(whatsappURl_android),
            mode: LaunchMode.externalApplication,
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("WhatsApp no instalado")));
        }
      }
    } catch (e) {
      print("Error al abrir WhatsApp: $e");
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Error al abrir WhatsApp: $e")));
    }
  }

  void listenerHistorial(String canceledRideId) {
    print('iniciando historial');
    var distanciaInKilometros = 0.0;
    DocumentReference docHistorialRef = FirebaseFirestore.instance.collection('historial').doc(canceledRideId);

    StreamSubscription<Position>? subscription; // Declaración de la suscripción

    // Inicializa la suscripción en el estado "onride"
// Añade el parámetro p2 para tener el punto de recogida
    void initializeOnRideSubscription(LatLng dropOffLocation) {
      const locationOptions = LocationSettings(
        accuracy: LocationAccuracy.bestForNavigation,
        distanceFilter: 0,
      );

      subscription = _geolocator.getPositionStream(locationSettings: locationOptions).listen(
        (Position position) async {
          print('listener subscription');
          double distance = AssistantMehods.calculateDistance(LatLng(position.latitude, position.longitude), dropOffLocation);
          distanciaInKilometros = distance / 1000;

          setState(() {
            statusRide = "Ir al destino" + " - " + distanciaInKilometros.toStringAsFixed(2) + " km";
          });
        },
      );

      setState(() {
        _isInitializedPositionStreamSuscription = true;
      });
    }

    historialSubscription = docHistorialRef.snapshots().listen((DocumentSnapshot snapshot) async {
      print('listener historial');
      if (snapshot.exists) {
        var snapshotData = snapshot.data() as Map<String, dynamic>?;

        var positionMap = snapshotData?['position'] as Map<String, dynamic>?;
        var rideInfoMap = snapshotData?['rideInfoMap'] as Map<String, dynamic>?;
        var pickUpMap = rideInfoMap?['pickup'] as Map<String, dynamic>?;
        var dropOffMap = rideInfoMap?['dropOff'] as Map<String, dynamic>?;
        var geopoint = positionMap?['geopoint'] as GeoPoint?;

        if (snapshotData != null) {
          String status = snapshotData['status'];

          if (status == 'aceptada') {
            p2 = LatLng(double.parse(pickUpMap?['latitude'] ?? '0.0'), double.parse(pickUpMap?['longitude'] ?? '0.0'));
            LatLng p1 = LatLng(geopoint?.latitude ?? 0.0, geopoint?.longitude ?? 0.0);
            double distance = AssistantMehods.calculateDistance(p1, p2);
            distanciaInKilometros = distance / 1000;
            //print(distanciaInKilometros);
            setState(() {
              statusRide = "Taxi en camino" + " - " + distanciaInKilometros.toStringAsFixed(2) + " km";
            });
          } else if (status == 'onride' && !_isInitializedPositionStreamSuscription) {
            p2 = LatLng(double.parse(dropOffMap?['latitude'] ?? '0.0'), double.parse(dropOffMap?['longitude'] ?? '0.0'));
            initializeOnRideSubscription(p2); // Inicializa la suscripción solo una vez
            print("hola4");
          } else if (status == 'arrived') {
            if (!isPlayerAlarm) {
              playAAC();
              isPlayerAlarm = true;
            }

            setState(() {
              statusRide = "El conductor ha llegado";
            });
          } else if (status == 'finished') {
            if (isPendingRide) {
              if (historialSubscription != null) {
                historialSubscription!.cancel(); // Cancela la suscripción si existe
                historialSubscription = null; // Limpia la referencia
              }
              isPendingRide = false;
            }
            if (_isInitializedPositionStreamSuscription) {
              subscription?.cancel(); // Cancela la suscripción si es necesario
              if (historialSubscription != null) {
                historialSubscription!.cancel(); // Cancela la suscripción si existe
                historialSubscription = null; // Limpia la referencia
              }
              _isInitializedPositionStreamSuscription = false;
            }

            print("hola5");
            setState(() {
              statusRide = "Carrera finalizada";
            });
            endTheTrip(canceledRideId);
            actualizarHistorial(true);
            int conteo = Provider.of<AppData>(context, listen: false).countTrips;
            print('conteo');
            print(conteo);
            Provider.of<AppData>(context, listen: false).updateCountTrips(conteo + 1);
            rideInfoMap!['driver_code'] = datosConductor?.code.toString();
            Provider.of<AppData>(context, listen: false).updateTripKey(canceledRideId);
            Provider.of<AppData>(context, listen: false).updateTripHistoryData(HistoryModel.fromMap(rideInfoMap!));
            print(Provider.of<AppData>(context, listen: false).countTrips);
          }

          // Resto de tu lógica aquí
        } else {
          print('El documento existe pero no contiene datos');
        }
      } else {
        print('El documento ha sido eliminado');
      }
    }) as StreamSubscription<DocumentSnapshot<Map<String, dynamic>>>?;
  }

  Future<void> playAAC() async {
    AudioPlayer audioPlayer = AudioPlayer();
    await audioPlayer.play(AssetSource('../sounds/bocina_taxi.mp3'), mode: PlayerMode.mediaPlayer);
  }

  Future<void> endTheTrip(String historialId) async {
    // Show progress dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => ProgressDialog(
        message: "Por favor espere",
      ),
    );

    // try {
    // Get ride history data
    final historyRef = _historialCollection.doc(historialId);
    final historySnapshot = await historyRef.get();
    final data = historySnapshot.data() as Map<String, dynamic>?;
    if (data == null) {
      throw "No se encontró ningún documento con este ID";
    }

    // Calculate fare amount
    double fareAmount = (data['rideInfoMap']['precio_aceptado']);

    // Update driver code in rideInfoMap
    Map<String, dynamic> rideInfoMap = Map<String, dynamic>.from(data['rideInfoMap']);
    rideInfoMap['driver_code'] = datosConductor?.code.toString(); // Cambia 'tu_valor_aqui' por el valor que desees

    // Update Firestore document
    await historyRef.update({
      'rideInfoMap': rideInfoMap,
    });

    // Update user data in Firestore
    final driverRef = FirebaseFirestore.instance.collection('users').doc(auth.currentUser!.uid);
    final driverSnapshot = await driverRef.get();
    final Map<String, dynamic> userRides = Map<String, dynamic>.from(driverSnapshot.data()?['rides'] ?? {});
    userRides[historialId] = true;
    await driverRef.update({'rides': userRides});

    // Close progress dialog and show CollectFareDialog
    //Navigator.pop(context);
    var result = '';
    driverId = rideInfoMap?['driver_id'];
    if (rideInfoMap?['tipo_carrera'] == 'normal') {
      result = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => CollectFareDialog(paymentMethod: "cash", fareAmount: fareAmount),
      );
    } else {
      result = 'close';
      Navigator.pop(context);
    }

    // Handle the result
    if (result == "close") {
      print(driverId);
      final ratingResult = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => RatingScreen(
          driverId: driverId,
        ),
      ));

      // Handle the ratingResult accordingly
      if (ratingResult == "close") {
        resetApp();
      }
    } else {
      // Handle other scenarios if necessary
    }
  }

  Future<void> actualizarHistorial(bool isFinishedRide) async {
    print('actualizar historial');
    // Obtener la referencia al documento del usuario actual
    final userRef = FirebaseFirestore.instance.collection('users').doc(currentUser.id);
    // Obtener la lista de carreras actual del usuario
    final userSnapshot = await userRef.get();
    final Map<String, dynamic> userRides = Map<String, dynamic>.from(userSnapshot.data()?['rides'] ?? {});
    // Agregar la nueva carrera al Map de carreras
    userRides[docId] = isFinishedRide; // donde docId es el ID de la solicitud de viaje que se acaba de enviar
    // Guardar el nuevo Map de carreras en el documento del usuario
    userRef.update({'rides': userRides});
    //await AssistantMehods.resetApp(context);
    //AssistantMehods.getCarrerasTomadas(context, currentUser.id!);
  }

  Future<void> getInfoDriver(String conductorId) async {
    final driverRef = FirebaseFirestore.instance.collection('drivers').doc(conductorId);
    final driverSnapshot = await driverRef.get();

    // Verificar si el documento existe y obtener sus datos
    if (driverSnapshot.exists) {
      Map<String, dynamic>? driverData = driverSnapshot.data();

      // Ahora tienes los datos del conductor que aceptó el viaje cancelado
      setState(() {
        datosConductor = DriverData.fromJson(driverData!);
        print('Datos del conductor:' + (datosConductor?.name ?? 'No name available'));
      });
    }
  }

/**a partir de aqui es para transporte publico
 *
 *
 */

// // Ejemplo de uso:
//   PuntoGeografico origen = PuntoGeografico(latitud: 17.89189564093928, longitud: 63.28616011887789);
//   PuntoGeografico destino = PuntoGeografico(latitud: 17.89737001301244, longitud: 63.29333234578371);
//
//   List<Grupo> grupos = [
//     Grupo(
//       descripcion: "Descripción del Grupo",
//       nombre: "Nombre del Grupo",
//       rutas: [
//         Ruta(
//           inicio: PuntoGeografico(latitud: 17.89189564093928, longitud: 63.28616011887789),
//           fin: PuntoGeografico(latitud: 17.89737001301244, longitud: 63.29333234578371),
//           polylineCoordinates: [
//             // ... coordenadas de la ruta ...
//           ],
//         ),
//         // ... otras rutas ...
//       ],
//     ),
//     // ... otros grupos ...
//   ];
//
//   List<Grupo> gruposValidos = findClosestRoute(origen, dropOff, grupos);
}
