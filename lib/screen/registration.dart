import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:viaje_seguro/screen/rideRequestScreen.dart';
import 'package:viaje_seguro/screen/splash.dart';
import '../helpers/screen_navigation.dart';
import '../helpers/style.dart';
import '../providers/user.dart';
import '../widgets/custom_text.dart';
import '../widgets/loading.dart';
import 'login.dart';

class RegistrationScreen extends StatefulWidget {
  static const String idScreen = "registrarion";
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    UserProvider authProvider = Provider.of<UserProvider>(context);
    //AppStateProvider app = Provider.of<AppStateProvider>(context);

    return Scaffold(
      key: _key,
      backgroundColor: secundary,
      body: authProvider.status == Status.Authenticating? Loading() :
      SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              color: white,
              height: 100,
            ),

            Container(
              color: Colors.white,
              width: (MediaQuery.of(context).size.height / 100) * 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15.0), // Ajusta el radio según lo desees
                    child: Image.asset(
                      "images/zdrive.png",
                      width: 100,
                      height: 100,
                    ),
                  ),
                  SizedBox(
                    height: 1.0,
                  ),
                  Text(
                    "Registro pasajero",
                    style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),

            Container(
              height: 40,
              color: white,
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.name,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: white),
                        labelText: "Nombre completo",
                        hintText: "eg: Santos Enoque",
                        icon: Icon(Icons.person, color: white,)
                    ),
                  ),),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.email,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: white),
                        labelText: "Correo electronico",
                        hintText: "santos@enoque.com",
                        icon: Icon(Icons.email, color: white,)
                    ),
                  ),),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.phone,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: white),
                        labelText: "Numero de celular",
                        hintText: "+91 75698547",
                        icon: Icon(Icons.phone, color: white,)
                    ),
                  ),),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: white),
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Padding(padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.password,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        labelStyle: TextStyle(color: white),
                        labelText: "Contraseña",
                        hintText: "ingrese letras o numeros",
                        icon: Icon(Icons.lock, color: white,)
                    ),
                  ),),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: GestureDetector(
                onTap: ()async{
                  await _requestLocationPermission();
                  if(!await authProvider.signUp()){
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text("Registration failed!"))
                    );
                    return;
                  }

                  authProvider.clearController();
                  Navigator.pop(context);
                  //changeScreenReplacement(context, Splash());
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: primary,
                      borderRadius: BorderRadius.circular(5)
                  ),
                  child: Padding(padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CustomText(text: "Registrar", color: secundary, size: 22,)
                      ],
                    ),),
                ),
              ),
            ),

            GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CustomText(text: "Ingrese aqui", size: 20, color: terciary,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Future<void> _requestLocationPermission() async {
    final LocationPermission permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.whileInUse || permission == LocationPermission.always) {
      //_getUserLocation();
    } else {}
  }
}
