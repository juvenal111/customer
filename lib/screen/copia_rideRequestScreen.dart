// import 'dart:convert';
// import 'package:rxdart/rxdart.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:geoflutterfire/geoflutterfire.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:http/http.dart' as http;
// import '../Assitants/assistantMethods.dart';
// import '../Models/directDetails.dart';
//
// class RideRequestScreen extends StatefulWidget {
//   final String title;
//
//   RideRequestScreen({required this.title});
//
//   @override
//   _RideRequestScreenState createState() => _RideRequestScreenState();
// }
//
// enum VehicleCategory {
//   standard,
//   premium,
//   bike,
// }
//
// class _RideRequestScreenState extends State<RideRequestScreen> {
//   late GoogleMapController mapController;
//   LatLng? _destinationLocation;
//   LatLng? _pickupLocation;
//   late DirectionDetails tripDirectionDetails;
//   Set<Polyline> polYlineSet = {};
//   Map<PolylineId, Polyline> _polylines = {};
//   late Set<Marker> _markers = {};
//   String _vehicleCategory = "";
//   late Geoflutterfire geo;
//   late Stream<List<DocumentSnapshot>> stream;
//   final Geoflutterfire _geoFlutterFire = Geoflutterfire();
//   final Geolocator _geolocator = Geolocator();
//   BehaviorSubject<GeoFirePoint> _center = BehaviorSubject<GeoFirePoint>();
//   late Stream<List<DocumentSnapshot>> _stream;
//
//
//   @override
//   void initState() {
//     super.initState();
//     geo = Geoflutterfire();
//     _requestLocationPermission();
//     _stream = Stream.empty();
//     stream = Stream.empty();
//     //_center = BehaviorSubject<Position>();
//   }
//
//
//   Future<void> _requestLocationPermission() async {
//     final LocationPermission permission = await Geolocator.requestPermission();
//     if (permission == LocationPermission.whileInUse || permission == LocationPermission.always) {
//       _getUserLocation();
//     } else {
//     }
//   }
//
//   Future<void> _getUserLocation() async {
//     Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
//     GeoFirePoint center = _geoFlutterFire.point(latitude: position.latitude, longitude: position.longitude);
//     _center.sink.add(center);
//     _getAvailableDrivers(center);
//   }
//
//   void _getAvailableDrivers(GeoFirePoint center) {
//     var collectionReference = FirebaseFirestore.instance.collection('rideRequests');
//     double radius = 15; // Radio en kilómetros
//     stream = geo.collection(collectionRef: collectionReference).within(center: center, radius: radius, field: 'position', strictMode: true);
//     setState(() {
//       _stream = stream;
//     });
//   }
//
//
//
//   Future<void> _getCurrentLocation() async {
//     try {
//       Position newPosition = await Geolocator.getCurrentPosition(
//         desiredAccuracy: LocationAccuracy.high,
//       );
//       _pickupLocation = LatLng(newPosition.latitude, newPosition.longitude);
//     } catch (e) {
//       print(e);
//     }
//   }
//
//   // Función para solicitar una carrera
//   void requestRide(String pickupLocation, String dropoffLocation) async {
//     // Crear un documento en la colección "rides"
//     DocumentReference rideReference = FirebaseFirestore.instance.collection('rideRequests').doc();
//
//     // Almacenar la información de la carrera en el documento
//     rideReference.set({
//       'pickupLocation': pickupLocation,
//       'dropoffLocation': dropoffLocation,
//       'price': 0.0,
//       'status': 'requested',
//       'driverLocation': null,
//       'driverDistance': null,
//       'originDistance': null,
//       'rideTime': null,
//     });
//   }
//
//   Future<void> saveRidePrice(String rideId, double ridePrice) async {
//     DocumentReference rideReference = FirebaseFirestore.instance.collection('rideRequests').doc(rideId);
//     await rideReference.update({
//       'ridePrice': ridePrice,
//     });
//   }
//
//   Future<double> getRouteDistance(double originLat, double originLng, double destinationLat, double destinationLng) async {
//     String url = 'https://maps.googleapis.com/maps/api/directions/json?' + 'origin=$originLat,$originLng' + '&destination=$destinationLat,$destinationLng' + '&mode=driving' + '&key=YOUR_API_KEY';
//
//     final response = await http.get(Uri.parse(url));
//
//     if (response.statusCode == 200) {
//       Map<String, dynamic> data = json.decode(response.body);
//
//       if (data['status'] == 'OK') {
//         List<dynamic> legs = data['routes'][0]['legs'];
//         double distance = legs[0]['distance']['value'].toDouble();
//         return distance;
//       } else {
//         throw Exception('Failed to get route distance');
//       }
//     } else {
//       throw Exception('Failed to get route distance');
//     }
//   }
//
//   Future<void> locatePosition() async {
//     Position newPosition = await Geolocator.getCurrentPosition(
//       desiredAccuracy: LocationAccuracy.high,
//     );
//     LatLng latLatPosition = LatLng(newPosition.latitude, newPosition.longitude);
//
//     CameraPosition cameraPosition = new CameraPosition(target: latLatPosition, zoom: 14);
//     mapController.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
//   }
//
//   static final CameraPosition _kGooglePlex = CameraPosition(
//     target: LatLng(37.42, -122.0058),
//     zoom: 14.4746,
//   );
//
//   void _handleMapTap(LatLng tappedPoint) {
//     setState(() {
//       _markers.clear();
//       _markers.add(
//         Marker(
//           markerId: MarkerId(tappedPoint.toString()),
//           position: tappedPoint,
//           infoWindow: InfoWindow(title: 'Destino'),
//         ),
//       );
//       _destinationLocation = tappedPoint;
//       _calculateRoute(_pickupLocation!, _destinationLocation!);
//     });
//   }
//
//   //toma valores de firestore para calcular tarifa sugerida por el sistema
//   Future<double> calculateRidePrice(double distance, double time) async {
//     double pricePerKilometer = await FirebaseFirestore.instance.collection("settings").doc("pricing").get().then((snapshot) => snapshot.data()!["price_per_kilometer"]);
//     double pricePerMinute = await FirebaseFirestore.instance.collection("settings").doc("pricing").get().then((snapshot) => snapshot.data()!["price_per_minute"]);
//
//     return distance * pricePerKilometer + time * pricePerMinute;
//   }
//
//   //dibuja ruta
//   Future<void> _calculateRoute(LatLng origin, LatLng destination) async {
//     var details = await AssistantMehods.obtainPlaceDirectionDetails(origin, destination);
//     setState(() {
//       tripDirectionDetails = details;
//     });
//     PolylinePoints polylinePoints = PolylinePoints();
//     List<PointLatLng> decodePolyLinePointsResult = polylinePoints.decodePolyline(details.encodePoints);
//
//     List<LatLng> polylineCoordinates = [];
//     if (decodePolyLinePointsResult.isNotEmpty) {
//       decodePolyLinePointsResult.forEach((PointLatLng pointLatlng) {
//         polylineCoordinates.add(LatLng(pointLatlng.latitude, pointLatlng.longitude));
//       });
//     }
//
//     setState(() {
//       Polyline polyline = Polyline(
//         color: Colors.pink,
//         polylineId: PolylineId("PolylineID"),
//         jointType: JointType.round,
//         points: polylineCoordinates,
//         width: 5,
//         startCap: Cap.roundCap,
//         endCap: Cap.roundCap,
//         geodesic: true,
//       );
//       polYlineSet.clear();
//       polYlineSet.add(polyline);
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     Future<void> _addMarker(double lat, double lng, String driverName) async{
//       LatLng latLng = LatLng(lat, lng);
//       MarkerId markerId = MarkerId('$driverName');
//       Marker marker = Marker(
//         markerId: markerId,
//         position: latLng,
//         infoWindow: InfoWindow(
//           title: driverName,
//         ),
//       );
//       // Future.delayed(Duration(seconds: 3)).then((_) {
//       //   Navigator.of(context).pop(true);
//       // });
//
//       if(mounted){
//         setState(() {
//           _markers.add(marker);
//         });
//
//       }
//
//     }
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Flutter Google Maps Demo'),
//       ),
//       body: SafeArea(
//         child: Stack(
//           children: [
//             StreamBuilder(
//               stream: _center.stream,
//               builder: (BuildContext context, AsyncSnapshot<GeoFirePoint> snapshot) {
//                 if (!snapshot.hasData) {
//                   return Center(
//                     child: CircularProgressIndicator(),
//                   );
//                 }
//                 return GoogleMap(
//                     myLocationButtonEnabled: true,
//                     myLocationEnabled: true,
//                   onMapCreated: (GoogleMapController controller) {
//                     mapController = controller;
//                     locatePosition();
//                   },
//                   initialCameraPosition: _kGooglePlex,
//                   markers: _markers,
//                 );
//               },
//             ),
//             StreamBuilder<List<DocumentSnapshot<Object?>>>(
//               stream: _stream,
//               builder: (BuildContext context, AsyncSnapshot<List<DocumentSnapshot<Object?>>> snapshot) {
//                 if (!snapshot.hasData) {
//                   return SizedBox.shrink();
//                 }
//
//                 // Limpia los marcadores existentes en el mapa
//                 _markers.clear();
//
//                 // Agrega marcadores para cada conductor disponible dentro del radio
//                 snapshot.data!.forEach((document) async {
//                   double lat = document['position']['geopoint'].latitude;
//                   double lng = document['position']['geopoint'].longitude;
//                   String driverName = document['origen'];
//                   MarkerId markerId = MarkerId('$driverName');
//                   LatLng latLng = LatLng(lat, lng);
//                   Marker marker = Marker(
//                     markerId: markerId,
//                     position: latLng,
//                     infoWindow: InfoWindow(
//                       title: driverName,
//                     ),
//                   );
//                   // Future.delayed(Duration(seconds: 3)).then((_) {
//                   //   Navigator.of(context).pop(true);
//                   // });
//
//                     setState(() {
//                       _markers.add(marker);
//                     });
//                 });
//
//                 return SizedBox.shrink();
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
