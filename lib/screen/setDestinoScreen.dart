import 'dart:async';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:viaje_seguro/helpers/style.dart';
import 'package:viaje_seguro/services/grupoServicios.dart';
import '../Assitants/assistantMethods.dart';
import '../Assitants/mapKitAssistant.dart';
import '../Models/grupoRuta.dart';
import '../Models/marcador.dart';
import '../services/rutaServicios.dart';

class MapSample extends StatefulWidget {
  final LatLng initialPosition;

  MapSample({required this.initialPosition});

  @override
  _MapSampleState createState() => _MapSampleState();
}

class _MapSampleState extends State<MapSample> {
  late LatLng _lastMapPosition;
  bool disableCameraMove = false;
  List<Ruta> rutas = []; // Agrega esta línea para almacenar las rutas
  List<Grupo> rutasGrupo = []; // Agrega esta línea para almacenar las rutas
  List<Grupo> gruposValidosLV = []; // Agrega esta línea para almacenar las rutas
  Set<Marker> markers = {}; // Agrega este conjunto de marcadores
  Set<Polyline> polYlineSet = {};
  List<Marker> _markers = [];
  late Position userPosition;
  bool mostrarListaRuta = false;

// Define una lista de colores disponibles
  List<int> coloresDisponibles = List.generate(360, (index) => index); // Lista de 0 a 359
  int indiceColor = 0;
  Map<String, double> grupoColor = {};

  List<Map<String, dynamic>> lineas = [];
  final Geoflutterfire _geoFlutterFire = Geoflutterfire();
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  StreamSubscription<List<DocumentSnapshot>>? _geoQuerySubscription;
  LatLng? _previousLocation;
  double radius = 15; // en km
  Map<String, LatLng> previousLocations = {};
  late final Uint8List? markerIcon;
  late final BitmapDescriptor descriptor;

  @override
  void initState() {
    super.initState();
    _lastMapPosition = widget.initialPosition;
    init();
  }

  Future<void> init() async {
    markerIcon = await getBytesFromAsset('images/bus.png', 100);
    descriptor = BitmapDescriptor.fromBytes(markerIcon!);
    userPosition = (await AssistantMehods.getCurrentLocation())!;
    rutasGrupo = await obtenerGruposDesdeFirestore(); // Aquí debes obtener tus rutas de alguna manera
    lineas = await GrupoServicios.obtenerLineasDesdeFirestore();
    print(rutasGrupo.length);
  }

  Future<Uint8List?> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))?.buffer.asUint8List();
  }

  Future<List<Grupo>> obtenerGruposDesdeFirestore() async {
    print(rutasGrupo);
    List<Grupo> grupos = [];

    QuerySnapshot gruposQuery = await FirebaseFirestore.instance.collection('rutas').get();

    gruposQuery.docs.forEach((grupoDoc) {
      Map<String, dynamic> grupoData = grupoDoc.data() as Map<String, dynamic>;
      // Obtén el ID del documento
      String grupoId = grupoDoc.id;
      String nombre = grupoData['nombre'];
      String descripcion = grupoData['descripcion'];

      List<Ruta> rutas = [];

      List<dynamic> rutasData = grupoData['rutas'] ?? [];
      rutasData.forEach((rutaData) {
        GeoPoint inicio = rutaData['inicio']['posicion'];
        GeoPoint fin = rutaData['fin']['posicion'];
        Ruta ruta = Ruta(
          nombre: rutaData['nombre'],
          inicio: Marcador(id: rutaData['inicio']['id'], posicion: LatLng(inicio.latitude, inicio.longitude)),
          fin: Marcador(id: rutaData['fin']['id'], posicion: LatLng(fin.latitude, fin.longitude)),
        );
        List<dynamic> polylineCoordinatesData = rutaData['polylineCoordinates'];
        List<LatLng> polylineCoordinates = polylineCoordinatesData.map((geoPointData) {
          GeoPoint geoPoint = geoPointData as GeoPoint;
          return LatLng(geoPoint.latitude, geoPoint.longitude);
        }).toList();

        ruta.polylineCoordinates = polylineCoordinates;

        //ruta.polylineCoordinates = rutaData['polylineCoordinates'];
        rutas.add(ruta);
      });

      // Crea el grupo con el ID
      Grupo grupo = Grupo(id: grupoId, nombre: nombre, descripcion: descripcion, rutas: rutas);
      grupos.add(grupo);
    });

    return grupos;
  }

  void _onCameraMove(CameraPosition position) {
    if (!disableCameraMove) {
      _lastMapPosition = position.target;
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeStyle = Theme.of(context);
    return Scaffold(
      appBar: AppBar(title: const Text('Selecciona una ubicación')),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            onCameraMove: _onCameraMove,
            polylines: polYlineSet,
            markers: Set<Marker>.of(_markers),
            initialCameraPosition: CameraPosition(
              target: _lastMapPosition,
              zoom: 14.0,
            ),
          ),
          disableCameraMove
              ? Container()
              : Center(
                  child: Icon(Icons.location_on, size: 50.0),
                ),
          Visibility(
            visible: !mostrarListaRuta,
            child: Positioned(
              top: 10,
              left: 16.0,
              child: Stack(
                alignment: Alignment.center, // Esto centrará todos los hijos en el centro del Stack
                children: [
                  Container(
                    width: 150.0, // Ajusta el ancho del botón
                    height: 150.0, // Ajusta la altura del botón
                    child: FloatingActionButton(
                      onPressed: () async {
                        setState(() {
                          disableCameraMove = true;
                        });
                        if (userPosition != null) {}
                        List<Grupo> gruposValidos = findClosestRoute(
                          PuntoGeografico(latitud: userPosition.latitude, longitud: userPosition.longitude),
                          PuntoGeografico(latitud: _lastMapPosition.latitude, longitud: _lastMapPosition.longitude),
                          rutasGrupo,
                        );
                        //recupero las rutas
                        if (gruposValidos.length > 0) {
                          gruposValidos.forEach((element) async {
                            //List<Ruta> nuevasRutas = await RutaServicios.obtenerRutasPorIdentificador(element.id);
                            //print(nuevasRutas.length);
                            print(element.rutas.length);
                            // Agrega las nuevas rutas a la lista existente
                            //rutas.addAll(element.rutas);
                            //element.rutas = nuevasRutas;

                            // Escoge un color aleatorio y único para este grupo
                            int colorIndex = Random().nextInt(coloresDisponibles.length);
                            int color = coloresDisponibles.removeAt(colorIndex);
                            // Convierte el índice de color a un valor de matiz válido (0-360)
                            double hue = color.toDouble();
                            print(element.id);
                            grupoColor[element.id] = hue;
                            await _dibujarRutasEnMapa(element.rutas, hue);
                            print(polYlineSet.first.polylineId);
                            print(polYlineSet.last.polylineId);
                            print(polYlineSet.length);
                            //List<Marcador> marcadores = obtenerMarcadoresDesdeRutas(nuevasRutas);
                          });
                          //_dibujarRutasEnMapa(gruposValidos.first.rutas, hue);
                          Set<Marker> nuevosMarcadores = Set<Marker>();
                          //_limpiarMarcadores();
                          Marcador origen = Marcador(id: DateTime.now().toString(), posicion: LatLng(userPosition.latitude, userPosition.longitude));
                          Marcador destino = Marcador(id: DateTime.now().toString(), posicion: LatLng(_lastMapPosition.latitude, _lastMapPosition.longitude));
                          nuevosMarcadores.add(
                            Marker(
                              markerId: MarkerId(origen.id),
                              position: origen.posicion,
                              icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
                            ),
                          );
                          nuevosMarcadores.add(
                            Marker(
                              markerId: MarkerId(destino.id),
                              position: destino.posicion,
                              icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
                            ),
                          );

                          setState(() {
                            _markers.addAll(nuevosMarcadores);
                          });
                          //_mostrarListaYDibujar(gruposValidos);
                          mostrarListaRuta = true;
                          gruposValidosLV = gruposValidos;
                          print(grupoColor);
                        } else {
                          AssistantMehods.displayToastMessage('No existe transporte publico en tu zona', context);
                          Navigator.pop(context);

                          print('no existe transporte publico');
                        }
                      },
                      materialTapTargetSize: MaterialTapTargetSize.padded,
                      child: ClipOval(
                        child: Image.asset(
                          "images/bus.jpg",
                          width: 140.0, // Ajusta el ancho de la imagen
                          height: 140.0, // Ajusta la altura de la imagen
                          fit: BoxFit.cover, // Esto hará que la imagen se ajuste al botón
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 10,
                    child: Text(
                      'Buscar bus',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ), // Puedes ajustar el tamaño del texto según tus necesidades
                    ),
                  )
                ],
              ),
            ),
          ),

          ///boton buscador
          Visibility(
            visible: mostrarListaRuta,
            child: Positioned(
              bottom: 10,
              left: 16.0,
              child: Column(
                children: [
                  FloatingActionButton(
                    heroTag: "boton_buscar",
                    onPressed: () async {
                      _mostrarListaYDibujar(gruposValidosLV);
                    },
                    child: Icon(Icons.details),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

// Método para mostrar la lista y permitir dibujar
  void _mostrarListaYDibujar(List<Grupo> grupoValidos) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 300,
          color: white,
          child: Column(
            children: [
              // Agrega un botón para cerrar la lista y volver al mapa
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Cerrar Lista'),
              ),
              // Agrega el ListView para mostrar las líneas aquí
              Expanded(
                child: ListView.separated(
                  separatorBuilder: (BuildContext context, int index) => Divider(), // Añade un separador entre los elementos
                  itemCount: grupoValidos.length, // Define la cantidad de elementos
                  itemBuilder: (BuildContext context, int index) {
                    Map<String, dynamic> lineaEncontrada = lineas.firstWhere(
                      (linea) => linea['grupos_de_rutas'] == grupoValidos[index].id,
                    );

                    if (lineaEncontrada != null) {
                      // Aquí tienes el documento que corresponde a la ruta buscada
                      print(lineaEncontrada);
                    } else {
                      // No se encontró ninguna línea con la ruta buscada
                      print('No se encontró ninguna línea con la ruta buscada');
                    }
                    return Container(
                      //color: HSVColor.fromAHSV(1.0, grupoColor[grupoValidos[index].id]!, 1.0, 1.0).toColor(),
                      child: ListTile(
                        leading: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.directions_bus, // Cambia el ícono a un bus
                              size: 40.0, // Ajusta el tamaño del ícono
                            ),
                            Text('Bus'), // Agrega un texto dentro del botón
                          ],
                        ),
                        title: Row(
                          children: [
                            Text(
                              'linea: ${lineaEncontrada['numero']}',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(lineaEncontrada['tipo'] == 'Ninguno' ? '' : lineaEncontrada['tipo'] + ': '),
                            Text(
                              '${lineaEncontrada['identificador']}',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: 20,
                                  height: 20,
                                  decoration: BoxDecoration(
                                    color: HSVColor.fromAHSV(1.0, grupoColor[grupoValidos[index].id]!, 1.0, 1.0).toColor(),
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Text('Color de Ruta'),
                              ],
                            ),
                            Text('Tiempo de llegada al destino: 20 min'),
                          ],
                        ),
                        onTap: () async {
                          String idRuta = grupoValidos[index].id;
                          print(idRuta);
                          setState(() {
                            // Map<String, dynamic> lineaEncontrada = lineas.firstWhere(
                            //       (linea) => linea['grupos_de_rutas'] == grupoValidos[index].id,
                            // );

                            _limpiarMarcadores();
                            _limpiarPolylines();
                            _dibujarRutasEnMapa(grupoValidos[index].rutas, grupoColor[grupoValidos[index].id]!);
                            escucharBus(lineaEncontrada); // _controllerNumeroLinea.text = linea['numero'];
                            // selectedIdentifier = linea['tipo'];
                            // _controllerIdentifierInput.text = linea['identificador'];
                          });
                          Navigator.pop(context);
                        },
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

// Crear un mapa para almacenar las ubicaciones anteriores de cada marcador

  Future<void> _getMarkersBusFromFirestore(String lineaNumero) async {
    Position position = userPosition;
    GeoFirePoint center = _geoFlutterFire.point(latitude: position.latitude, longitude: position.longitude);
    var collectionReference = _firestore.collection('activeDrivers');
    var collectionReferenceDriver = _firestore.collection('busDriver');
    var geoQuery = _geoFlutterFire.collection(collectionRef: collectionReference).within(center: center, radius: radius, field: 'position', strictMode: true);
    List<Marker> markers = [];
    _geoQuerySubscription = geoQuery.listen((List<DocumentSnapshot> documentList) async {
      // Asegúrate de que el listener también sea asíncrono
      print('listener conductores');
      setState(() {
        _markers.clear();
      });
      for (var doc in documentList) {
        print(doc.id);
        //DocumentSnapshot<dynamic> driverDocument = await collectionReferenceDriver.doc(doc.id).get();
        //print(driverDocument.data()['numero']);
        Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
        if (data['driver_data'] != null && data['driver_data']['numero'] == lineaNumero) {
          var location = doc.get('position');
          var previousLocation = previousLocations[doc.id] ?? LatLng(0, 0);
          var rot = MapKitAssistant.getMarkerRotation(
            previousLocation.latitude,
            previousLocation.longitude,
            location['geopoint'].latitude,
            location['geopoint'].longitude,
          );
          Marker marker = Marker(
            markerId: MarkerId(doc.id),
            position: LatLng(location['geopoint'].latitude, location['geopoint'].longitude),
            icon: descriptor,
            rotation: rot,
            consumeTapEvents: true,
          );
          markers.add(marker);
          // Actualizar la ubicación anterior para este marcador
          previousLocations[doc.id] = LatLng(location['geopoint'].latitude, location['geopoint'].longitude);
        }
      }
      setState(() {
        _markers = markers;
      });
    });
  }

  escucharBus(lineaEncontrada) {
    _getMarkersBusFromFirestore(lineaEncontrada['numero']);
  }

  List<Marcador> obtenerMarcadoresDesdeRutas(List<Ruta> rutas) {
    Set<Marcador> marcadores = {};

    for (Ruta ruta in rutas) {
      marcadores.add(ruta.inicio);
      marcadores.add(ruta.fin);
    }

    return marcadores.toList();
  }

  List<Grupo> findClosestRoute(PuntoGeografico origin, PuntoGeografico destination, List<Grupo> allGrupos) {
    List<Grupo> validGrupos = [];
    double tuUmbralDeseado = 0.5;

    for (var grupo in allGrupos) {
      bool cumpleCondicion = grupo.rutas.any((ruta) {
        bool cumpleOrigen = ruta.polylineCoordinates.any((coordinate) {
          return calculateDistance(origin, PuntoGeografico(latitud: coordinate.latitude, longitud: coordinate.longitude)) < tuUmbralDeseado;
        });

        bool cumpleDestino = ruta.polylineCoordinates.any((coordinate) {
          return calculateDistance(destination, PuntoGeografico(latitud: coordinate.latitude, longitud: coordinate.longitude)) < tuUmbralDeseado;
        });

        return cumpleOrigen || cumpleDestino;
      });

      if (cumpleCondicion) {
        //gruposValidos.add(grupo);
        validGrupos.add(Grupo(
          id: grupo.id,
          descripcion: grupo.descripcion,
          nombre: grupo.nombre,
          rutas: grupo.rutas,
        ));
      }
      //List<Ruta> validRutas = validOriginRutas.toSet().union(validDestinationRutas.toSet()).toList();

      // if (validRutas.isNotEmpty) {
      //
      // }
    }

    validGrupos.forEach((grupo) {
      grupo.rutas.forEach((ruta) {
        double distanceToOrigin = calculateDistance(PuntoGeografico(latitud: ruta.inicio.posicion.latitude, longitud: ruta.inicio.posicion.longitude), origin);
        double distanceToDestination = calculateDistance(PuntoGeografico(latitud: ruta.fin.posicion.latitude, longitud: ruta.fin.posicion.longitude), destination);

        ruta.distanceToOrigin = distanceToOrigin;
        ruta.distanceToDestination = distanceToDestination;
      });
    });

    validGrupos.sort((a, b) {
      double totalDistanceA = a.rutas.fold(0, (prev, ruta) => prev + ruta.distanceToOrigin + ruta.distanceToDestination);
      double totalDistanceB = b.rutas.fold(0, (prev, ruta) => prev + ruta.distanceToOrigin + ruta.distanceToDestination);
      return totalDistanceA.compareTo(totalDistanceB);
    });

    return validGrupos;
  }

  double calculateDistance(PuntoGeografico puntoA, PuntoGeografico puntoB) {
    const int earthRadius = 6371;

    double lat1 = toRadians(puntoA.latitud);
    double lon1 = toRadians(puntoA.longitud);
    double lat2 = toRadians(puntoB.latitud);
    double lon2 = toRadians(puntoB.longitud);

    double dLat = lat2 - lat1;
    double dLon = lon2 - lon1;

    double a = pow(sin(dLat / 2), 2) + cos(lat1) * cos(lat2) * pow(sin(dLon / 2), 2);

    double c = 2 * atan2(sqrt(a), sqrt(1 - a));

    return earthRadius * c;
  }

  double toRadians(num degree) {
    return degree * (pi / 180);
  }

  Future<void> _dibujarRutasEnMapa(List<Ruta> rutas, double hue) async {
    Set<Polyline> nuevasPol = {}; // Crea un nuevo conjunto

    for (Ruta ruta in rutas) {
      nuevasPol.add(Polyline(
        polylineId: PolylineId(ruta.nombre + Random().nextInt(1000).toString()),
        visible: true,
        points: ruta.polylineCoordinates,
        color: HSVColor.fromAHSV(1.0, hue, 1.0, 1.0).toColor(),
        // Utiliza el matiz como color
        width: 5,
        geodesic: true,
      ));
    }

    // Actualiza el estado una sola vez después de agregar todas las nuevas polilíneas
    setState(() {
      polYlineSet.addAll(nuevasPol);
    });
  }

  void _dibujarMarcadores(List<Marcador> marcadores) {
    Set<Marker> nuevosMarcadores = Set<Marker>();
    _limpiarMarcadores();
    for (Marcador marcador in marcadores) {
      nuevosMarcadores.add(
        Marker(
          markerId: MarkerId(marcador.id),
          position: marcador.posicion,
          draggable: true,
          onDragEnd: (newPosition) {
            //_onMarkerDragEnd(marcador, newPosition);
          },
        ),
      );
    }

    setState(() {
      _markers.addAll(nuevosMarcadores);
    });
  }

  void _updateRoute(Ruta ruta) {
    setState(() {
      polYlineSet.removeWhere((polyline) => polyline.polylineId.value == ruta.nombre);

      polYlineSet.add(Polyline(
        polylineId: PolylineId(ruta.nombre),
        visible: true,
        points: ruta.polylineCoordinates,
        color: Colors.blue,
      ));
    });
  }

  void _limpiarPolylines() {
    setState(() {
      polYlineSet.clear();
    });
  }

  void _limpiarMarcadores() {
    setState(() {
      _markers.clear();
    });
  }
}
