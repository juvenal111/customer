import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating_null_safety/smooth_star_rating_null_safety.dart';

import '../helpers/style.dart';
import '../providers/user.dart'; // Importar Provider si lo estás usando

class RatingView extends StatelessWidget {
  static const String idScreen = "ratingView";

  @override
  Widget build(BuildContext context) {
    // Obtener el modelo del usuario (asegúrate de haberlo inicializado previamente)
    final userProvider = Provider.of<UserProvider>(context);
    final userModel = userProvider.userModel;

    double starCounter = userModel?.rating ?? 0.0; // Obtener la calificación del modelo del usuario

    String title = ''; // Título de la calificación
    // Redondear el valor de starCounter a un número entero
    int roundedStarCounter = starCounter.round();

    if (roundedStarCounter == 1) {
      title = "Muy malo";
    } else if (roundedStarCounter == 2) {
      title = "Malo";
    } else if (roundedStarCounter == 3) {
      title = "Bueno";
    } else if (roundedStarCounter == 4) {
      title = "Muy bueno";
    } else if (roundedStarCounter == 5) {
      title = "Excelente";
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Mi Calificación'),
        backgroundColor: primary,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Tu calificación actual:',
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Text(
              starCounter.toStringAsFixed(2),
              style: TextStyle(
                fontSize: 32,
                color: primary,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20),
            SmoothStarRating(
              rating: starCounter,
              color: primary,
              allowHalfRating: false,
              starCount: 5,
              size: 50,
              onRatingChanged: null, // No se permite cambiar la calificación
            ),
            SizedBox(height: 20),
            Text(
              title,
              style: TextStyle(
                fontSize: 24,
                color: Colors.green,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
