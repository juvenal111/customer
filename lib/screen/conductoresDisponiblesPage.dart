import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../helpers/style.dart';
import '../widgets/requestItem.dart';

class ConductoresDisponiblesPage extends StatefulWidget {
  final String keyCarrera;

  //List<ListaConductor> listaConductores;
  ConductoresDisponiblesPage({required this.keyCarrera});

  @override
  _ConductoresDisponiblesPageState createState() =>
      _ConductoresDisponiblesPageState();
}

class _ConductoresDisponiblesPageState
    extends State<ConductoresDisponiblesPage> {
  // // DatabaseReference rideRequestRef;
  // // DatabaseReference driveRequestRef;
  // List<ListaConductor> listaC;
  // TabController tabController;
  int selectedIndex = 0;
  // GoogleMapController newGoogleMapController;
  // Timer _timer;
  var geoLocator = Geolocator();

  String driverStatusText = "Desconectado ahora - Conéctate";

  Color driverStatusColor = Colors.black;
  String tipoSucripcion = "";
  String? keyCarrera;
  @override
  void initState() {
    keyCarrera = widget.keyCarrera;
    // TODO: implement initState
    super.initState();
    //getEstadoConductor();
    init();
  }



  Future init() async {
    // escucharConductoresStream.cancel();
    // listenerConductores();
    // listenerChangeDriver();
  }

  @override
  void dispose() {
//    driveStreamSubscription.cancel();
      //driveStreamSubscription.
    super.dispose();
    //Geofire.stopListener();
    //  newGoogleMapController.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Atras'),
        backgroundColor: red,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          color: secundary,
          tooltip: 'Atras',
          onPressed: () {
            //showDialogWithFields();
            Navigator.pop(context, 'holaConductoresDisponibles');
            setState(() {
              // banderaBack = true;
              // banderaHandle = true;
            });
            // resetApp();
          },
        ),

      ),
      body: CustomScrollView(
        slivers: <Widget>[
          // const SliverAppBar(
          //   pinned: true,
          //   expandedHeight: 250.0,
          //   flexibleSpace: FlexibleSpaceBar(
          //     title: Text('Demo'),
          //   ),
          // ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                ListView.separated(
                  padding: EdgeInsets.all(0),
                  itemBuilder: (context, index) {
                    return RequestItem(
                      //listaConductor: listaConductores[index],
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      Divider(
                    thickness: 3.0,
                    height: 3.0,
                  ),
                  itemCount: 0,
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                ),
                // :
                // Row(),
              ],
            ),
          ),
          //_doctorsList()
        ],
      ),


    );
  }


}
