import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:viaje_seguro/screen/registration.dart';
import 'package:viaje_seguro/screen/rideRequestScreen.dart';
import 'package:viaje_seguro/screen/splash.dart';
import 'package:viaje_seguro/widgets/pantallaCarga.dart';
import '../helpers/screen_navigation.dart';
import '../helpers/style.dart';
import '../providers/user.dart';
import '../widgets/custom_text.dart';
import '../widgets/loading.dart';

class LoginScreen extends StatefulWidget {
  static const String idScreen = "login";

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _key = GlobalKey<ScaffoldState>();
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<UserProvider>(context);

    return Scaffold(
      key: _key,
      backgroundColor: secundary,
      body: authProvider.status == Status.Authenticating
          ? PantallaCarga()
          : SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              color: white,
              height: 100,
            ),
            Container(
              color: white,
              width: (MediaQuery.of(context).size.height / 100) * 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15.0), // Ajusta el radio según lo desees
                    child: Image.asset(
                      "images/zdrive.png",
                      width: 230,
                      height: 230,
                    ),
                  ),
                  SizedBox(
                    height: 1.0,
                  ),
                  Text(
                    "Login pasajero",
                    style: TextStyle(fontSize: 24.0, fontFamily: "Brand Bold"),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),

            ),
            Container(
              height: 60,
              color: white,
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(border: Border.all(color: white), borderRadius: BorderRadius.circular(5)),
                child: Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.email,
                    style: TextStyle(color: Colors.white), // Cambia el color del texto ingresado a blanco
                    decoration: InputDecoration(
                        hintStyle: TextStyle(color: white),
                        border: InputBorder.none,
                        hintText: "Correo electronico",
                        icon: Icon(
                          Icons.email,
                          color: white,
                        )),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12),
              child: Container(
                decoration: BoxDecoration(border: Border.all(color: white), borderRadius: BorderRadius.circular(5)),
                child: Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: authProvider.password,
                    obscureText: _obscureText,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      hintStyle: TextStyle(color: Colors.white),
                      border: InputBorder.none,
                      hintText: "Contraseña",
                      icon: Icon(
                        Icons.lock,
                        color: Colors.white,
                      ),
                      suffixIcon: Listener(
                        onPointerDown: (details) {
                          setState(() {
                            _obscureText = false;
                          });
                        },
                        onPointerUp: (details) {
                          setState(() {
                            _obscureText = true;
                          });
                        },
                        child: Icon(
                          Icons.remove_red_eye,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: GestureDetector(
                onTap: () async {
                  await _requestLocationPermission();
                  if (!await authProvider.signIn()) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Login failed!")));
                    return;
                  }

                  //limpiar controlador de texto login
                  authProvider.clearController();
                  //Navigator.pushNamed(context, Splash.idScreen);

                },
                child: Container(
                  decoration: BoxDecoration(color: primary, borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CustomText(
                          text: "Ingresar",
                          color: secundary,
                          size: 22,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                changeScreen(context, RegistrationScreen());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CustomText(
                    text: "Registro aqui",
                    size: 20,
                    color: white,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _requestLocationPermission() async {
    final LocationPermission permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.whileInUse || permission == LocationPermission.always) {
      //_getUserLocation();
    } else {}
  }
}
