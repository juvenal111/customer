import 'package:maps_toolkit/maps_toolkit.dart';

class MapKitAssistant {
  static double getMarkerRotation(sLat, sLng, dLat, dLng) {
    var rot = SphericalUtil.computeHeading(LatLng(sLat, sLng), LatLng(dLat, dLng));
    return rot.toDouble();
  }
  static double calculateDistance(double sLat, double sLng, double dLat, double dLng) {
    double distance = SphericalUtil.computeDistanceBetween(LatLng(sLat, sLng), LatLng(dLat, dLng)).toDouble();
    return distance;
  }
  static Duration estimateTravelTime(double distanceInMeters, double averageSpeedKmh) {
    // Convertir la velocidad promedio a metros por segundo
    var speedMps = averageSpeedKmh * 1000 / 3600;
    // Calcular el tiempo en segundos
    var timeSeconds = distanceInMeters / speedMps;
    // Crear un objeto Duration con el tiempo en segundos
    return Duration(seconds: timeSeconds.round());
  }
}
