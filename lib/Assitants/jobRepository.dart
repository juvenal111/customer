import 'dart:async';
import 'dart:collection';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:rxdart/rxdart.dart';
import 'package:viaje_seguro/Models/Oferta.dart';
import '../Models/rideRequests.dart';

class JobRepository {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;



  Stream<DocumentSnapshot<Map<String, dynamic>>> getRideInfo(String rideId) {
    final rideRef = FirebaseFirestore.instance.collection('rideRequests').doc(rideId);
    return rideRef.snapshots();
  }


  Stream<List<Oferta>> getOffersDrivers(String rideRequestId) {
    return _firestore
        .collection('rideRequests')
        .doc(rideRequestId)
        .snapshots()
        .map((docSnapshot) {
      final rideInfoMap = docSnapshot.get('rideInfoMap') as Map<String, dynamic>;

      if (rideInfoMap != null && rideInfoMap.containsKey('conductores')) {
        final conductores = rideInfoMap['conductores'] as List<dynamic>;
        return conductores.map((conductor) => Oferta.fromJson(conductor)).toList();
      } else {
        return []; // O algún valor predeterminado si no hay conductores
      }
    });
  }


  Future<bool> getEstado(String driverId) async {
    DocumentSnapshot docSnapshot = await _firestore.collection('drivers').doc(driverId).get();
    return docSnapshot.get('status');
  }

  Stream<List<dynamic>> getDrivers(String docId) {
    return _firestore.collection('rideRequests').doc(docId).snapshots().map((docSnapshot) => docSnapshot.get('rideInfoMap.conductores'));
  }
}
