import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:viaje_seguro/Assitants/requestAssistant.dart';
import 'package:viaje_seguro/Models/userModel.dart';
import 'package:http/http.dart' as http;
import '../Models/address.dart';
import '../Models/directDetails.dart';
import '../Models/history.dart';
import '../providers/appData.dart';
import '../variables_constantes.dart';
import 'dart:math' as Math;

class AssistantMehods {
  static double calculateDistance(LatLng p1, LatLng p2) {
    print('calculate distance:');
    print(p1);
    print(p2);
    double distanceInMeters = Geolocator.distanceBetween(p1.latitude, p1.longitude, p2.latitude, p2.longitude);
    print(distanceInMeters);
    return distanceInMeters;
  }

  static Future<String> searchCoordinateAddress(LatLng position, context) async {
    String placeAddress = "";
    String st1, st2, st3, st4;

    String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude},${position.longitude}&key=$mapKey";
    var response = await RequestAssistant.getRequest(url);
    print(url);
    if (response != "failed") {
      //placeAddress = response["results"][0]["formatted_address"];

      st1 = response["results"][1]["address_components"][0]["long_name"];
      st2 = response["results"][1]["address_components"][1]["short_name"];

//      st3 = response["results"][0]["address_components"][5]["long_name"];
//      st4 = response["results"][0]["address_components"][6]["long_name"];
      //placeAddress = st1 + ", " + st2 + ", " + st3 + ", " + st4;

      placeAddress = st1 + ", " + st2;

      Address userPickUpAddress = new Address();
      userPickUpAddress.longitude = position.longitude;
      //userPickUpAddress.longitude = -63.206994;
      userPickUpAddress.latitude = position.latitude;
      //userPickUpAddress.latitude = -17.827060;
      userPickUpAddress.placeName = placeAddress;

      Provider.of<AppData>(context, listen: false).updatePickUpLocationAddress(userPickUpAddress);
    }
    return placeAddress;
  }

  static Future<String> getCountryFromCoordinates(double latitude, double longitude) async {
    final String url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longitude&key=$mapKey';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = jsonDecode(response.body);
      final components = data['results'][0]['address_components'];

      for (var component in components) {
        if (component['types'].contains('country')) {
          return component['short_name'];
        }
      }
    }

    // Si la solicitud HTTP falla o si no se encuentra un componente de país, devuelve una cadena vacía.
    return '';
  }

  static Future<DirectionDetails> obtainPlaceDirectionDetails(Position initialPosition, LatLng finalPosition) async {
    String directionUrl =
        "https://maps.googleapis.com/maps/api/directions/json?origin=${initialPosition.latitude},${initialPosition.longitude}&destination=${finalPosition.latitude},${finalPosition.longitude}&key=$mapKey";
    //https://maps.googleapis.com/maps/api/directions/json?origin=-17.7842289,%20-63.1879632&destination=-17.7966436,%20-63.19136520000001&key=AIzaSyBVjYvx_la0Npt3Pnku4FKt_MZffCT6Dlo
    print(directionUrl);
    bool banderaRepetir = false;
    var res = await RequestAssistant.getRequest(directionUrl);
    ;

    DirectionDetails directionDetails = DirectionDetails(
        distanceValue: (res["routes"][0]["legs"][0]["distance"]["value"]).toDouble(),
        durationValue: res["routes"][0]["legs"][0]["duration"]["value"],
        distanceText: res["routes"][0]["legs"][0]["distance"]["text"],
        durationText: res["routes"][0]["legs"][0]["duration"]["text"],
        encodePoints: res["routes"][0]["overview_polyline"]["points"]);
    // directionDetails.encodePoints = res["routes"][0]["overview_polyline"]["points"];
    //
    // directionDetails.distanceText = res["routes"][0]["legs"][0]["distance"]["text"];
    // directionDetails.distanceValue = res["routes"][0]["legs"][0]["distance"]["value"];
    //
    // directionDetails.durationText = res["routes"][0]["legs"][0]["duration"]["text"];
    // directionDetails.durationValue = res["routes"][0]["legs"][0]["duration"]["value"];

    return directionDetails;
  }

  static int calculateFares(DirectionDetails directionDetails) {
    //in terms usd
    int tarifaBase = 10;
    double distanceTraveledFare = 0.0;
    double timeTraveledFare = (directionDetails.durationValue! / 60) * 0.45;
    distanceTraveledFare = (directionDetails.distanceValue! / 1000) * 1.19;
    double totalFareAmount = timeTraveledFare + distanceTraveledFare + tarifaBase;
    double totalLocalAmount = totalFareAmount * 1;
    return totalLocalAmount.round();
  }

  static int calculateFaresMoto(DirectionDetails directionDetails) {
    //in terms usd
    int tarifaBase = 1;
    double distanceTraveledFare = 0.0;
    double timeTraveledFare = (directionDetails.durationValue! / 60) * 0.2;
    distanceTraveledFare = (directionDetails.distanceValue! / 1000) * 0.8;
    double totalFareAmount = timeTraveledFare + distanceTraveledFare + tarifaBase;
    double totalLocalAmount = totalFareAmount * 1;
    return totalLocalAmount.round();
  }

  static void getCarrerasTomadas(context, String uid) async {
    String userId = uid;
    CollectionReference userCollection = FirebaseFirestore.instance.collection("users");
    DocumentSnapshot userSnapshot = await userCollection.doc(userId).get();
    UserModel userModel = UserModel.fromSnapshot(userSnapshot);
    int rideCount = 0;
// Verificar si el atributo rides existe y no es nulo
    if (userModel.rides != null) {
      // Contar los registros con valor true en el atributo rides
      rideCount = userModel.rides!.values.where((value) => value == true).length;

      // Imprimir el resultado
      print('Número de rides: $rideCount');
    } else {
      print('El atributo rides es nulo o no existe.');
    }

    //int rideCount = userModel?.rides?.length ?? 0;

    if (userSnapshot.exists) {
      Map<String, dynamic>? historyData = userModel.rides;

      if (historyData != null) {
        // Actualizar el contador total de viajes
        int tripCounter = rideCount;
        Provider.of<AppData>(context, listen: false).updateTripsCounter(tripCounter);

        // Obtener las claves de los viajes
        List<String> tripHistoryKeys = historyData.keys.toList();
        Provider.of<AppData>(context, listen: false).updateTripKeys(tripHistoryKeys);

        // Obtener los datos de historial de viajes
        obtainTripRequestHistoryData(context);
      }
    }
  }

  static void obtainTripRequestHistoryData(context) {
    List<String> keys = Provider.of<AppData>(context, listen: false).tripHistoryKeys;
    CollectionReference<Map<String, dynamic>> historyCollection = FirebaseFirestore.instance.collection('historial');

    for (String key in keys) {
      historyCollection.doc(key).get().then((DocumentSnapshot<Map<String, dynamic>> snapshot) {
        if (snapshot.exists) {
          Map<String, dynamic> historyData = snapshot.data() as Map<String, dynamic>;
          Map<String, dynamic> rideInfoMap = historyData['rideInfoMap'];
          HistoryModel history = HistoryModel.fromMap(rideInfoMap); // Usar el constructor fromMap
          Provider.of<AppData>(context, listen: false).updateTripHistoryData(history);
        }
      });
    }
  }

  static void displayToastMessage(String message, BuildContext context) {
    Fluttertoast.showToast(msg: message);
  }


  static Future<Position?> getCurrentLocation() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Si el usuario niega el permiso, puedes mostrar un mensaje o tomar una acción adecuada.
        return null;
      }
    }

    Position? position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    return position;
  }
}
