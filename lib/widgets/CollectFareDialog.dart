import 'package:flutter/material.dart';
import 'package:viaje_seguro/helpers/style.dart';

class CollectFareDialog extends StatelessWidget {
  final String paymentMethod;
  final double fareAmount;

  const CollectFareDialog({
    Key? key,
    required this.paymentMethod,
    required this.fareAmount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      backgroundColor: Colors.transparent,
      child: Container(
        margin: const EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 22.0),
            const Text(
              'Tarifa de Viaje',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 22.0),
            const Divider(),
            const SizedBox(height: 16.0),
            Center(
              child: Text(
                fareAmount.toStringAsFixed(2),
                style: const TextStyle(
                  fontSize: 55.0,
                  fontFamily: 'Brand-Bold',
                  color: black,
                ),
              ),
            ),
            const SizedBox(height: 16.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                'Este es el monto total del viaje, tarifa aceptada por ambos',
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 16.0,
                  color: Colors.black,
                ),
              ),
            ),
            const SizedBox(height: 30.0),
            _buildCashButton(context),
          ],
        ),
      ),
    );
  }

  Widget _buildCashButton(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(10,0,10,10),
      child: TextButton(
        onPressed: () async {
          Navigator.pop(context, "close");
          Navigator.pop(context);
        },
        style: TextButton.styleFrom(
          backgroundColor: secundary,
          padding: const EdgeInsets.all(16.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              'Pagar efectivo',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            //const Icon(Icons.attach_money, color: Colors.white, size: 26.0),
          ],
        ),
      ),
    );
  }
}
