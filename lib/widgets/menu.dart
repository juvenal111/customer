import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:viaje_seguro/screen/ratingView.dart';
import 'package:viaje_seguro/widgets/simpleDialogItem.dart';

import '../Models/userModel.dart';
import '../providers/appData.dart';
import '../providers/user.dart';
import '../screen/historyScreen.dart';
import '../screen/login.dart';
import '../screen/profileScreen.dart';
import '../screen/splash.dart';
import 'Divider.dart';

class MenuWidget extends StatelessWidget {
  final UserModel modelUser;
  final VoidCallback cancelSubscriptionCallback; // Nuevo callback

  MenuWidget({required this.modelUser, required this.cancelSubscriptionCallback});

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UserProvider>(context);

    final SimpleDialog dialogReportarProblemas = SimpleDialog(
      title: Text('Numeros de soporte'),
      children: [
        SimpleDialogItem(
          color: Colors.orange,
          text: '+59163570398',
          onPressed: () {
            Navigator.pop(context);
            openwhatsappNumber(context, '+59163570398');
          },
        ),
        SimpleDialogItem(
          color: Colors.green,
          text: '+59167881940',
          onPressed: () {
            Navigator.pop(context);
            openwhatsappNumber(context, '+59167881940');
          },
        ),
      ],
    );
    return Container(
      color: Colors.white,
      width: 255.0,
      child: Drawer(
        child: ListView(
          children: [
            Container(
              height: 165.0,
              child: DrawerHeader(
                decoration: BoxDecoration(color: Colors.white),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("images/user_icon.png", height: 65.0, width: 65.0),
                    SizedBox(
                      width: 16.0,
                    ),
                    Text(
                      modelUser?.name ?? '',
                      style: TextStyle(fontSize: 16.0, fontFamily: "Brand Bold"),
                    ),
                    SizedBox(height: 6.0),
                    Text("Carreras tomadas: " + Provider.of<AppData>(context, listen: true).countTrips.toString()),
                  ],
                ),
              ),
            ),
            DividerWidget(),
            SizedBox(
              height: 12.0,
            ),
            /**VIAJES**/
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, HistoryScreen.idScreen, arguments: (route) => false);
              },
              child: ListTile(
                leading: Icon(Icons.history), // Icono para "Viajes"
                title: Text(
                  "Viajes",
                  style: TextStyle(fontSize: 15.0),
                ),
              ),
            ),

            /**REPORTAR**/
            GestureDetector(
              onTap: () {
                var retorno = showDialog<void>(context: context, builder: (context) => dialogReportarProblemas);
              },
              child: ListTile(
                leading: Icon(Icons.report_problem), // Icono para "Reportar"
                title: Text(
                  "Reportar",
                  style: TextStyle(fontSize: 15.0),
                ),
              ),
            ),
            /**CALIFICACION**/
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, RatingView.idScreen, arguments: (route) => false);
              },
              child: ListTile(
                leading: Icon(Icons.star), // Icono para "Calificación"
                title: Text(
                  "Calificación",
                  style: TextStyle(fontSize: 15.0),
                ),
              ),
            ),
            /**CUENTA****/
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, ProfileScreen.idScreen, arguments: (route) => false);
              },
              child: ListTile(
                leading: Icon(Icons.account_circle), // Icono para "Cuenta"
                title: Text(
                  "Cuenta",
                  style: TextStyle(fontSize: 15.0),
                ),
              ),
            ),
            /**CERRAR SESION****/
            GestureDetector(
              onTap: () async {

                cancelSubscriptionCallback();
                await userProvider.signOut();
                // Llamada al nuevo callback
                //Navigator.pushReplacementNamed(context, LoginScreen.idScreen);
                //Navigator.pushNamed(context, Splash.idScreen);

              },
              child: ListTile(
                leading: Icon(Icons.logout), // Icono para "Cerrar sesión"
                title: Text(
                  "Cerrar sesión",
                  style: TextStyle(fontSize: 15.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void openwhatsappNumber(BuildContext context, String numero) async {
    //print(driverPhone);
    String whatsapp = "+591" + "driverPhone";
    var whatsappURl_android = "whatsapp://send?phone=" + numero + "&text=Hola";
    var whatappURL_ios = "https://wa.me/$whatsapp?text=${Uri.parse("hello")}";
    if (Platform.isIOS) {
      // for iOS phone only
      if (await canLaunch(whatappURL_ios)) {
        await launch(whatappURL_ios, forceSafariVC: false);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: new Text("whatsapp no instalado")));
      }
    } else {
      // android , web
      if (await canLaunch(whatsappURl_android)) {
        await launch(whatsappURl_android);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: new Text("whatsapp no instalado")));
      }
    }
  }
}
