import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../helpers/style.dart';

class AddNoteDialog extends StatelessWidget {
  final myController = TextEditingController();

  AddNoteDialog();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      backgroundColor: Colors.transparent,
      child: Container(
        margin: EdgeInsets.fromLTRB(5, 0, 5, 5),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: double.infinity,
              height: 20,
              child: IconButton(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                alignment: Alignment.topRight,
                icon: Icon(
                  Icons.close,
                  color: Colors.red,
                  size: 20,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            SizedBox(
              height: 0,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [Text(
                "Agregar deseo",
                style: TextStyle(
                    color: secundary,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),],

            ),
            SizedBox(
              height: 16,
            ),
            Divider(
              height: 2.0,
              thickness: 2.0,
            ),
            SizedBox(height: 16.0),

           Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
              child: TextFormField(
                controller: myController,
                onChanged: (text) {
                  //print('Second text field: ${text}');
                },
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Ingrese deseo',
                ),
              ),
            ),

            SizedBox(height: 16.0),

            Padding(
              padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
              child: ElevatedButton(
                onPressed: () async {
                  Navigator.pop(context, myController.text);
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 5),
                  //textStyle: const TextStyle(fontSize: 30, fontWeight: FontWeight.bold)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text('Agregar deseo'), // <-- Text
                    SizedBox(
                      width: 5,
                    ),
                    Icon(
                      // <-- Icon
                      Icons.note_add,
                      size: 24.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
