import 'package:flutter/material.dart';
import '../Models/Oferta.dart';
import 'CountdownTimer.dart';

class OfertasList extends StatefulWidget {
  final List<Oferta> ofertas;

  OfertasList({required this.ofertas});

  @override
  _OfertasListState createState() => _OfertasListState();
}

class _OfertasListState extends State<OfertasList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: widget.ofertas.length,
      itemBuilder: (BuildContext context, int index) {
        Oferta oferta = widget.ofertas[index];

        return Card(
          elevation: 4,
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // ... Resto del contenido de la oferta

                // Temporizador de cuenta regresiva
                CountdownTimer(
                  initialSeconds: oferta.durationInSeconds,
                  onTimeout: () {
                    // El temporizador llegó a cero, puedes hacer lo que necesites aquí

                    // Eliminar la oferta expirada de la lista de ofertas
                    setState(() {
                      widget.ofertas.removeAt(index);
                    });

                    // Si es necesario, puedes realizar otras acciones aquí
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
