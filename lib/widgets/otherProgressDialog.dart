import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OtherProgressDialog extends StatefulWidget {
  final String message;

  OtherProgressDialog({required this.message});

  void show(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => this,
    );
  }

  void hide(BuildContext context) {
    Navigator.of(context).pop();
  }

  @override
  _OtherProgressDialogState createState() => _OtherProgressDialogState();
}

class _OtherProgressDialogState extends State<OtherProgressDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Row(
        children: [
          CircularProgressIndicator(),
          SizedBox(width: 16.0),
          Text(widget.message),
        ],
      ),
    );
  }
}