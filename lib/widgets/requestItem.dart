import 'package:flutter/material.dart';

import '../helpers/style.dart';

class RequestItem extends StatelessWidget {
  String distanceText = '';
  String durationText = '';

  RequestItem();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        print('hola request');
        // var res = await Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) => NotificationDialog(
        //             rideDetails: listaPasajero.rideDetails,
        //             nearbyAvailableRiders: listaPasajero.nearbyAvailableRiders))
        // );
        // if (res == "obtainDirection") {
        //   //displayRideDetailTaxiUbicacionContainer();
        //   //await getPlaceDirection();
        // }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.black38,
                blurRadius: 16.0,
                spreadRadius: 0.5,
                offset: Offset(0.7, 0.7),
              ),
            ],
          ),
          height: 120, //(MediaQuery.of(context).size.height / 100) * 23,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    child: Row(
                      children: [
                        //column profile
                        Expanded(
                          flex: 2,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.center,

//                        mainAxisSize: MainAxisSize.min,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(13)),
                                  child: Container(
                                    height: 55,
                                    width: 55,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: secundary,
                                    ),
                                    child: Image.asset(
                                      "images/pasajero.png",
                                      //height: 50,
                                      //width: 50,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Column(
                                  children: [
                                    Text(
                                      "asdf",
                                      //'${listaConductor.conductorDetails.name}',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.black38,
                                      ),
                                    ),
                                    Expanded(
                                      child: ElevatedButton(
                                          child: Text(
                                            'ACEPTAR',
                                            style: TextStyle(color: Colors.white, fontSize: 10.0),
                                          ),
                                          onPressed: () {
                                            Navigator.pop(context, [
                                              'accepted',
                                              // listaConductor
                                              //     .conductorDetails.key
                                            ]);
                                            // var res = await Navigator.push(
                                            //     context,
                                            //     MaterialPageRoute(
                                            //         builder: (context) =>
                                            //             TaxiALaUbicacionScreen(
                                            //                 position:
                                            //                 currentPosition)));
                                            // if (res == "obtainDirection") {
                                            //   displayRideDetailTaxiUbicacionContainer();
                                            //   //await getPlaceDirection();
                                            // }
                                          },
                                          onLongPress: () {
                                            print('Long press');
                                          },
                                          style: ButtonStyle(
                                            backgroundColor: MaterialStateProperty.all(green),
                                            //padding: MaterialStateProperty.all(EdgeInsets.all(50)),
                                            //textStyle: MaterialStateProperty.all(TextStyle(fontSize: 5))),
                                          )),
                                    ),
                                  ],
                                )),
                              ],
                            ),
                          ),
                        ),
                        //columns tarifa y otros datos
                        Expanded(
                          flex: 8,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(15.0, 0.0, 00.0, 0.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                //listaPasajero.rideDetails.carrera != "Taxi a la ubicacion" ?
                                //conductor - pasajero
                                Expanded(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 7,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "Conductor - Pasajero",
                                              style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                            ),
                                            Expanded(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Image.asset(
                                                          "images/distancia.png",
                                                          height: 15.0,
                                                          //width: 16.0,
                                                        ),
                                                        SizedBox(
                                                          width: 1,
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            "distamncoa",
                                                            //'${listaConductor.conductorDetails.distancia_text}',
                                                            style: Theme.of(context).textTheme.subtitle1,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(width: 2),
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Image.asset(
                                                          "images/tiempo.png",
                                                          height: 30.0,
                                                          //width: 16.0,
                                                        ),
                                                        SizedBox(
                                                          width: 1,
                                                        ),
                                                        Expanded(
                                                          child: Text(
                                                            "durationc",
                                                            // '${listaConductor.conductorDetails.duration_text}',
                                                            style: Theme.of(context).textTheme.subtitle1,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Container(
                                          //color: Colors.green,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                //color: Colors.yellow,
                                                child: Image.asset(
                                                  "images/pagar.png",
                                                  height: 25.0,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 1,
                                              ),
                                              Expanded(
                                                child: Container(
                                                  padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                                                  child: Text(
                                                    "tarifa",
                                                    // '${listaConductor.conductorDetails.tarifaOfertada}' + 'Bs.',
                                                    style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.red),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                //categoria tipo
                                Expanded(
                                  child: Row(
                                    children: [
                                      // Text(
                                      //   "•",
                                      //   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32.0),
                                      // ),

                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "Categoria:",
                                              style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                            ),
                                            Expanded(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                            "categoria",
                                                            // '${listaConductor.conductorDetails.categoria}',
                                                            style: Theme.of(context).textTheme.subtitle1,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  //SizedBox(width: 2),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "Tipo:",
                                              style: TextStyle(fontSize: 10.0, color: Colors.black38),
                                            ),
                                            Expanded(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child: Text(
                                                            "subtipo",
                                                            // '${listaConductor.conductorDetails.tipo}',
                                                            style: Theme.of(context).textTheme.subtitle1,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  //SizedBox(width: 2),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                // :
                                // Row(),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
