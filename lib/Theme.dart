import 'package:flutter/material.dart';

// ThemeData basicTheme() {
//
//   final FontWeight _bold = FontWeight.w700;
//   final FontWeight _semiBold = FontWeight.w600;
//   final FontWeight _regular = FontWeight.w400;
//
//   AppBarTheme _basicAppBarTheme(AppBarTheme base) {
//     return base.copyWith(
//         color: Colors.white,
//         iconTheme: IconThemeData(
//             color: Colors.black
//         )
//     );
//   }
//
//   TextTheme _basicTextTheme(TextTheme base) {
//     return base.copyWith(
//       headline6: TextStyle(
//           fontSize: 25.0, fontWeight: _bold, color: Colors.black),
//       subtitle1: TextStyle(
//           fontSize: 18.0, fontWeight: _bold, color: Colors.black),
//       headline5: TextStyle(
//           fontSize: 20.0, fontWeight: _semiBold, color: Colors.black),
//       bodyText1: TextStyle(
//           fontSize: 16.0, fontWeight: _semiBold, color: Colors.black),
//       bodyText2: TextStyle(
//           fontSize: 14.0, fontWeight: _regular, color: Colors.black),
//       headline4: TextStyle(
//           fontSize: 16.0, fontWeight: _bold, color: Colors.black),
//       headline3: TextStyle(
//           fontSize: 12.0, fontWeight: _semiBold, color: Colors.black),
//       headline2: TextStyle(
//           fontSize: 18.0, fontWeight: _semiBold, color: Colors.black),
//       headline1: TextStyle(
//           fontSize: 14.0, fontWeight: _semiBold, color: Color(0xFF707070)),
//       button: TextStyle(
//           fontSize: 16.0, fontWeight: _regular, color: Colors.white),
//     );
//   }
//
//
//
//   final ThemeData base = ThemeData.light();
//
//   return base.copyWith(
//       appBarTheme: _basicAppBarTheme(base.appBarTheme),
//       textTheme: _basicTextTheme(base.textTheme),
//       primaryColor: Color(0xFFE2993A),
//       bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.transparent, elevation: 20)
//   );
// }
//
ThemeData basicTheme() {
  final FontWeight _bold = FontWeight.w700;
  final FontWeight _semiBold = FontWeight.w600;
  final FontWeight _regular = FontWeight.w400;

  AppBarTheme _basicAppBarTheme(AppBarTheme base) {
    return base.copyWith(
        //color: Colors.white,
        iconTheme: IconThemeData(
            color: Colors.black
        )
    );
  }

  TextTheme _basicTextTheme(TextTheme base) {
    return base.copyWith(
      headline6: TextStyle(
          fontSize: 25.0, fontWeight: _bold, color: Colors.black),
      subtitle1: TextStyle(
          fontSize: 18.0, fontWeight: _bold, color: Colors.black),
      headline5: TextStyle(
          fontSize: 20.0, fontWeight: _semiBold, color: Colors.black),
      bodyText1: TextStyle(
          fontSize: 16.0, fontWeight: _semiBold, color: Colors.black),
      bodyText2: TextStyle(
          fontSize: 14.0, fontWeight: _regular, color: Colors.black),
      headline4: TextStyle(
          fontSize: 16.0, fontWeight: _bold, color: Colors.black),
      headline3: TextStyle(
          fontSize: 12.0, fontWeight: _semiBold, color: Colors.black),
      headline2: TextStyle(
          fontSize: 18.0, fontWeight: _semiBold, color: Colors.black),
      headline1: TextStyle(
          fontSize: 14.0, fontWeight: _semiBold, color: Color(0xFF707070)),
      button: TextStyle(
          fontSize: 16.0, fontWeight: _regular, color: Colors.white),
    );
  }
  final ThemeData base = ThemeData.light();

  return ThemeData(
      primarySwatch: Colors.yellow, // Agrega esta línea
      appBarTheme: _basicAppBarTheme(base.appBarTheme),
      textTheme: _basicTextTheme(base.textTheme),
      primaryColor: Color(0xFFE2993A),
      bottomSheetTheme: BottomSheetThemeData(backgroundColor: Colors.transparent, elevation: 20)
  );
}
