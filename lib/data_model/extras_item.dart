import 'dart:convert';

import 'package:flutter/foundation.dart';

class ExtrasItem {
  final int? id;
  final String name;
  final double price;
  final String? imageUrl;
  ExtrasItem({
    this.id,
    required this.name,
    required this.price,
    this.imageUrl
  });

  //FOR SERIALISATION needed for sharedPreferences of the list
  factory ExtrasItem.fromJson(Map<String, dynamic> jsonData) {
    return ExtrasItem(
        id: jsonData['id'] ?? 0,
        name: jsonData['name'],
        price: jsonData['price'],
        imageUrl: jsonData['image_url']
    );
  }

  static Map<String, dynamic> toMap(ExtrasItem extrasItem) => {
    'id': extrasItem.id,
    'name': extrasItem.name,
    'price': extrasItem.price,
    'image_url': extrasItem.imageUrl
  };

  static String? encodeExtrasList(List<ExtrasItem> extrasList) {
    return extrasList == null
        ? null
        : json.encode(
      extrasList
          .map<Map<String, dynamic>>(
              (extrasItem) => ExtrasItem.toMap(extrasItem))
          .toList(),
    );
  }

  static List<ExtrasItem> decodeExtrasList(String extrasList) {
    return extrasList == null
        ? <ExtrasItem>[]
        : (json.decode(extrasList) as List<dynamic>)
        .map<ExtrasItem>((item) => ExtrasItem.fromJson(item))
        .toList();
  }
}
