import 'package:cloud_firestore/cloud_firestore.dart';

import '../Models/company.dart';
import '../Models/product.dart';

class CompanyServices {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;




  Future<Map<String, List<Company>>> getCompaniesWithProducts() async {
    try {
      var querySnapshot = await FirebaseFirestore.instance.collection('company').get();

      if (querySnapshot.docs.isNotEmpty) {
        Map<String, List<Company>> companiesWithProducts = {};

        for (var doc in querySnapshot.docs) {
          Company companyData = Company.fromMap(doc.data() as Map<String, dynamic>);
          var products = await getProductsByCompanyId(doc.id);
          companyData.productos = products;

          if (companiesWithProducts.containsKey(companyData.tipo)) {
            // If there are companies associated with this type, add it to the existing list
            companiesWithProducts[companyData.tipo]!.add(companyData);
          } else {
            // If there are no companies associated with this type, create a new list
            companiesWithProducts[companyData.tipo] = [companyData];
          }
        }

        return companiesWithProducts;
      }

      return {}; // No documents in the collection
    } catch (e) {
      print('Error fetching company and product data: $e');
      // Handle the error according to your needs
      rethrow; // Re-throwing the exception for better error handling at a higher level
    }
  }


  Future<List<Product>> getProductsByCompanyId(String companyId) async {
    try {
      var querySnapshot = await _firestore
          .collection('productos')
          .where('company_id', isEqualTo: companyId)
          .get();

      if (querySnapshot.docs.isNotEmpty) {
        List<Product> products = [];

        for (var doc in querySnapshot.docs) {
          var productData = Product.fromMap(doc.data() as Map<String, dynamic>);
          products.add(productData);
        }

        return products;
      }

      return []; // No hay productos para la compañía
    } catch (e) {
      print('Error al obtener productos: $e');
      // Manejar el error según tus necesidades
      throw e;
    }
  }






  Future<Company?> getCompanyData(String companyId) async {
    try {
      var docSnapshot = await _firestore.collection('company').doc(companyId).get();

      if (docSnapshot.exists) {
        var companyData = Company.fromMap(docSnapshot.data() as Map<String, dynamic>);
        return companyData;
      }

      return null; // Documento no encontrado
    } catch (e) {
      print('Error al obtener datos de la compañía: $e');
      return null;
    }
  }
  Future<List<Company>> getAllCompanies() async {
    try {
      var querySnapshot = await _firestore.collection('company').get();

      if (querySnapshot.docs.isNotEmpty) {
        List<Company> companies = querySnapshot.docs.map((doc) {
          return Company.fromMap(doc.data() as Map<String, dynamic>);
        }).toList();

        return companies;
      }

      return []; // No hay documentos en la colección
    } catch (e) {
      print('Error al obtener datos de las compañías: $e');
      return []; // Manejar el error según tus necesidades
    }
  }
  Future<Map<String, List<Company>>> getCompaniesByType() async {
    try {
      var querySnapshot = await _firestore.collection('company').get();

      if (querySnapshot.docs.isNotEmpty) {
        Map<String, List<Company>> companiesByType = {};

        for (var doc in querySnapshot.docs) {
          var companyData = Company.fromMap(doc.data() as Map<String, dynamic>);

          // Agrupar las compañías por tipo
          if (companiesByType.containsKey(companyData.tipo)) {
            companiesByType[companyData.tipo]!.add(companyData);
          } else {
            companiesByType[companyData.tipo] = [companyData];
          }
        }

        return companiesByType;
      }

      return {}; // No hay documentos en la colección
    } catch (e) {
      print('Error al obtener datos de las compañías: $e');
      return {}; // Manejar el error según tus necesidades
    }
  }

}
