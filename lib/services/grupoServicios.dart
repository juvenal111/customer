import 'package:cloud_firestore/cloud_firestore.dart';

class GrupoServicios {
  static Future<List<Map<String, dynamic>>> obtenerLineasDesdeFirestore() async {
    QuerySnapshot<Map<String, dynamic>> querySnapshot =
    await FirebaseFirestore.instance.collection('lineas').get();

    return querySnapshot.docs.map((doc) {
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
      data['id'] = doc.id; // Agregamos el ID del documento al mapa de datos
      return data;
    }).toList();
  }
}
