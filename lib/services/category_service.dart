import 'package:cloud_firestore/cloud_firestore.dart';

Future<List<Map<String, dynamic>>> fetchCategoriesFromFirestore() async {
  List<Map<String, dynamic>> categories = [];

  try {
    QuerySnapshot querySnapshot = await FirebaseFirestore.instance.collection('categorias').get();

    querySnapshot.docs.forEach((doc) {
      categories.add({
        //'img': doc['img'] ?? '',
        'name': doc['category'],
        'description': doc['description'],
        'picture': doc['picture'],
      });
    });
  } catch (e) {
    print('Error fetching categories: $e');
  }

  return categories;
}
