import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../Models/grupoRuta.dart';
import '../Models/marcador.dart';


class RutaServicios {
  static Future<void> guardarListaRutasEnFirestore(Map<String, dynamic> datosLinea, List<Ruta> rutas) async {
    try {
      await FirebaseFirestore.instance.runTransaction((Transaction transaction) async {
        DocumentReference nuevaLineaRef = FirebaseFirestore.instance.collection('lineas').doc();
        transaction.set(nuevaLineaRef, datosLinea);
        String nuevaLineaId = nuevaLineaRef.id;

        String groupId = 'grupo_${DateTime.now().millisecondsSinceEpoch}';
        DocumentReference grupoRutasRef = FirebaseFirestore.instance.collection('rutas').doc(groupId);
        transaction.set(grupoRutasRef, {
          'nombre': 'Nombre del Grupo',
          'descripcion': 'Descripción del Grupo',
          'rutas': rutas
              .map((ruta) => {
            'nombre': ruta.nombre,
            'inicio': {
              'id': ruta.inicio.id,
              'posicion': GeoPoint(
                ruta.inicio.posicion.latitude,
                ruta.inicio.posicion.longitude,
              ),
            },
            'fin': {
              'id': ruta.fin.id,
              'posicion': GeoPoint(
                ruta.fin.posicion.latitude,
                ruta.fin.posicion.longitude,
              ),
            },
            'polylineCoordinates': ruta.polylineCoordinates.map((latLng) => GeoPoint(latLng.latitude, latLng.longitude)).toList(),
          })
              .toList(),
        });

        transaction.update(nuevaLineaRef, {
          'grupos_de_rutas': groupId,
        });
      });

    } catch (e) {
      print('Error al guardar las rutas: $e');
      throw e; // Lanza la excepción para que pueda ser manejada en el lugar donde se llama la función.
    }
  }
  static Future<List<Ruta>> obtenerRutasPorIdentificador(String identificador) async {
    try {
      DocumentSnapshot<Map<String, dynamic>> docSnapshot = await FirebaseFirestore.instance
          .collection('rutas')
          .doc(identificador)
          .get();

      List<Ruta> rutas = [];

      if (docSnapshot.exists) {
        List<dynamic> rutasData = docSnapshot.data()?['rutas'] ?? [];
        rutasData.forEach((rutaData) {
          GeoPoint inicio = rutaData['inicio']['posicion'];
          GeoPoint fin = rutaData['fin']['posicion'];
          Ruta ruta = Ruta(
            nombre: rutaData['nombre'],
            inicio: Marcador(id: rutaData['inicio']['id'], posicion: LatLng(inicio.latitude, inicio.longitude)),
            fin: Marcador(id: rutaData['fin']['id'], posicion: LatLng(fin.latitude, fin.longitude)),
          );
          List<dynamic> polylineCoordinatesData = rutaData['polylineCoordinates'];
          List<LatLng> polylineCoordinates = polylineCoordinatesData.map((geoPointData) {
            GeoPoint geoPoint = geoPointData as GeoPoint;
            return LatLng(geoPoint.latitude, geoPoint.longitude);
          }).toList();

          ruta.polylineCoordinates = polylineCoordinates;

          //ruta.polylineCoordinates = rutaData['polylineCoordinates'];
          rutas.add(ruta);
        });
      }

      return rutas;
    } catch (e) {
      throw Exception('Error al obtener rutas: $e');
    }
  }
  static Future<void> actualizarLineaYRutas(String idLineaEditId, Map<String, dynamic> datosLinea, List<Ruta> rutas, String grupoId) async {
    try {
      await FirebaseFirestore.instance.runTransaction((Transaction transaction) async {
        DocumentReference lineaRef = FirebaseFirestore.instance.collection('lineas').doc(idLineaEditId);

        transaction.update(lineaRef, datosLinea);

        // Actualizar rutas
        await actualizarRutas(rutas, grupoId);
      });
    } catch (e) {
      print('Error al actualizar la línea y rutas: $e');
      throw e;
    }
  }
  static Future<void> actualizarRutas(List<Ruta> rutas, String grupoId) async {
    try {
      WriteBatch batch = FirebaseFirestore.instance.batch();

      //DocumentReference grupoRutasRef = FirebaseFirestore.instance.collection('rutas').doc(groupId);
      DocumentReference rutaRef = FirebaseFirestore.instance.collection('rutas').doc(grupoId);
        // Actualizar los datos de la ruta
        Map<String, dynamic> rutaData = {
          'nombre': 'Nombre del Grupo',
          'descripcion': 'Descripción del Grupo',
          'rutas': rutas
              .map((ruta) => {
            'nombre': ruta.nombre,
            'inicio': {
              'id': ruta.inicio.id,
              'posicion': GeoPoint(
                ruta.inicio.posicion.latitude,
                ruta.inicio.posicion.longitude,
              ),
            },
            'fin': {
              'id': ruta.fin.id,
              'posicion': GeoPoint(
                ruta.fin.posicion.latitude,
                ruta.fin.posicion.longitude,
              ),
            },
            'polylineCoordinates': ruta.polylineCoordinates.map((latLng) => GeoPoint(latLng.latitude, latLng.longitude)).toList(),
          })
              .toList(),

        };

      batch.update(rutaRef, rutaData);

      // Realizar la transacción
      await batch.commit();
    } catch (e) {
      print('Error al actualizar rutas: $e');
      throw e;
    }
  }

}
