import 'package:firebase_auth/firebase_auth.dart';

import '../../models/user_account.dart';

class AuthService {
  final FirebaseAuth _auth;
  String? _verificationId;
  int? _resendToken;

  AuthService(this._auth);

  Stream<User?> get myUser => _auth.authStateChanges();

  UserAccount? _userAccountFromFirebaseUser(User? user) {
    return user != null
        ? UserAccount(
      uid: user.uid,
      email: user.email,
      phoneNumber: user.phoneNumber,
      displayName: user.displayName,
      emailVerified: user.emailVerified,
    )
        : null;
  }

  Future<String> emailSignIn(String email, String password) async {
    try {
      await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return "Signed In";
    } on FirebaseAuthException catch (e) {
      return e.message ?? "An error occurred during sign in.";
    } catch (e) {
      return "An error occurred during sign in.";
    }
  }

  Future<String> emailSignUp(String email, String password) async {
    try {
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      return "Signed up";
    } on FirebaseAuthException catch (e) {
      return e.message ?? "An error occurred during sign up.";
    } catch (e) {
      return "An error occurred during sign up.";
    }
  }

  Future<String> sendEmailVerification() async {
    try {
      await _auth.currentUser?.sendEmailVerification();
      return "Email containing code has been sent";
    } on FirebaseAuthException catch (e) {
      return e.message ?? "An error occurred while sending email verification.";
    } catch (e) {
      return "An error occurred while sending email verification.";
    }
  }

  Future<String> verifyWithPhone({required String phoneNo, required Function changeUI}) async {
    try {
      await _auth.verifyPhoneNumber(
        phoneNumber: phoneNo,
        verificationCompleted: (PhoneAuthCredential credential) async {
          await _auth.signInWithCredential(credential);
          print('completado');
        },
        verificationFailed: (FirebaseAuthException e) {
          print(e.message);
        },
        codeSent: (String verificationId, int? resendToken) {
          _verificationId = verificationId;
          _resendToken = resendToken!;
          changeUI();
          print('completado1');
        },
        codeAutoRetrievalTimeout: (String verificationId) {
          _verificationId = verificationId;
          print('completado2');
        },
      );
      return "Code Sent";
    } on FirebaseAuthException catch (e) {
      return e.message ?? "An error occurred during phone verification.";
    } catch (e) {
      return "An error occurred during phone verification.";
    }
  }

  Future<String> checkSMSCode(String smsCode) async {
    try {
      if (_verificationId != null) {
        PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(
          verificationId: _verificationId!,
          smsCode: smsCode,
        );

        await _auth.currentUser?.linkWithCredential(phoneAuthCredential);
        _auth.currentUser?.reload();
        return "Successful Link";
      } else {
        return "Verification ID is null.";
      }
    } on FirebaseAuthException catch (e) {
      return e.message ?? "An error occurred while checking SMS code.";
    } catch (e) {
      return "An error occurred while checking SMS code.";
    }
  }

  Future<void> signOut() async {
    try {
      await _auth.signOut();
      print("Sign Out successful");
    } on FirebaseAuthException catch (e) {
      print(e.message);
    } catch (e) {
      print("An error occurred during sign out.");
    }
  }
}
