import 'dart:async';
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

//import '../Models/user.dart';
import '../Models/userModel.dart';
import '../helpers/constants.dart';
import '../services/user.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class UserProvider with ChangeNotifier {
  static const LOGGED_IN = "loggedIn";
  static const ID = "id";
  final FirebaseAuth _auth = FirebaseAuth.instance;

  User? _user;
  Status _status = Status.Uninitialized;
  UserServices _userServices = UserServices();
  UserModel? _userModel;

//  getter
  Status get status => _status;

  User? get user => _user;

  UserModel? get userModel => _userModel;

  // public variables
  final formkey = GlobalKey<FormState>();

  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();

  // UserProvider.initialize() {
  //   _initialize();
  // }
  UserProvider() {
    _auth.authStateChanges().listen(_onAuthStateChanged);
  }
  void _onAuthStateChanged(User? user) {
    if (user == null) {
      _status = Status.Unauthenticated;
    } else {
      _user = user;
      _status = Status.Authenticated;
      fetchUserData();
      print("authenticated:");
    }
    notifyListeners();
  }
  Future<void> setUserModel(String uid) async {
    _userModel = await _userServices.getUserById(uid);

    if (_userModel != null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String userModelJson = json.encode(_userModel!.toJson()); // Make sure to import 'dart:convert'
      prefs.setString('userModel', userModelJson);
    }
  }


  int get rideCount => _userModel?.rides?.length ?? 0;

  void fetchUserData() {
    String? userId = _user?.uid;

    if (userId != null) {
      FirebaseFirestore.instance.collection('users').doc(userId).snapshots().listen((snapshot) {
        print('listener userProvideer');
        if (snapshot.exists) {
          // Obtener los datos del documento
          Map<String, dynamic> userData = snapshot.data() as Map<String, dynamic>;

          // Actualizar el userModel con los nuevos datos
          _userModel = UserModel.fromMap(userData);

          // Notificar a los listeners que se ha actualizado el userModel
          notifyListeners();
        }
      });
    }
  }





  Future<bool> signIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    DocumentSnapshot? passengerSnapshot;
    try {
      _status = Status.Authenticating;
      notifyListeners();

      await auth.signInWithEmailAndPassword(email: email.text.trim(), password: password.text.trim()).then((value) async {
        await prefs.setString(ID, value.user!.uid);
        await prefs.setBool(LOGGED_IN, true);

        // Obtener el userModel del servicio
        _userModel = await _userServices.getUserById(value.user!.uid);

        // Guardar userModel en las preferencias compartidas
        await prefs.setString(ID, value.user!.uid);
        //prefs.get(ID);

        passengerSnapshot = await FirebaseFirestore.instance.collection('users').doc(value.user!.uid).get();
        _status = Status.Authenticated;
        notifyListeners();

        //_initialize();
      });

      if (!passengerSnapshot!.exists) {
        // El usuario no es un pasajero, lanzar excepción
        await auth.signOut();
        throw Exception("Este usuario no tiene permisos para acceder como pasajero");
      } else {}

      await prefs.setBool(LOGGED_IN, true);

      return true;
    } catch (e) {
      _status = Status.Unauthenticated;
      notifyListeners();
      print(e.toString());
      return false;
    }
  }


  Future<bool> signUp() async {
    try{
      _status = Status.Authenticating;
      notifyListeners();
      await auth.createUserWithEmailAndPassword(email: email.text.trim(), password: password.text.trim()).then((result) async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString(ID, result.user!.uid);
        await prefs.setBool(LOGGED_IN, true);
        _userServices.createUser(
          id: result.user!.uid,
          name: name.text.trim(),
          email: email.text.trim(),
          phone: phone.text.trim(),
        );
        _status = Status.Authenticated;
        notifyListeners();

      });
      return true;
    }catch(e){
      _status = Status.Unauthenticated;
      notifyListeners();
      print(e.toString());
      return false;
    }
  }

  Future signOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    auth.signOut();
    //_status = Status.Unauthenticated;
    await prefs.setString(ID, "");
    await prefs.setBool(LOGGED_IN, false);
    notifyListeners();
    return Future.delayed(Duration.zero);
  }

  void clearController() {
    name.text = "";
    password.text = "";
    email.text = "";
    phone.text = "";
  }

  Future<bool> isUserLoggedIn() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final User? user = await _auth.currentUser;

    return user != null;
  }

  _initialize() async {
    print("iniciando userProvider");
    print("Estado actual: $_status");

    // if (await isUserLoggedIn()) {
    //   _status = Status.Authenticated;
    // } else {
    //   // El usuario no ha iniciado sesión
    // }
    // print(_status);
    // print(Status.Authenticated);
    // if (_status == Status.Authenticated) {
    //   // Obtener los datos del usuario al iniciar sesión
    //   fetchUserData();
    // }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool loggedIn = prefs.getBool(LOGGED_IN) ?? false;
    if (!loggedIn) {
      _status = Status.Unauthenticated;

    } else {
      User? currentUser = auth.currentUser;
      if (currentUser != null) {
        _user = currentUser;
        _status = Status.Authenticated;
        //
        //   // Intentar cargar _userModel desde SharedPreferences
        //   String? userModelJson = prefs.getString('userModel');
        //   if (userModelJson != null) {
        //     _userModel = UserModel.fromJson(json.decode(userModelJson)); // Make sure to import 'dart:convert'
        //   }
        //
        //   //_userModel = await _userServices.getUserById(currentUser.uid);
      }
      fetchUserData();
    }
    notifyListeners();
  }

}
