import 'package:flutter/cupertino.dart';
import 'package:viaje_seguro/providers/user.dart';
import '../Models/address.dart';
import '../Models/history.dart';
import '../Models/userModel.dart';


class AppData extends ChangeNotifier {
  late Address pickUpLocation, dropOffLocation;
  int _countTrips = 0;
  List<String> tripHistoryKeys = [];
  List<HistoryModel> tripHistoryDataList = [];
  UserProvider userProvider;

  AppData(this.userProvider);

  int get countTrips => _countTrips;

  void updateCountTrips(int newCount) {
    _countTrips = newCount;
    notifyListeners();
  }

  void updatePickUpLocationAddress(Address pickUpAddress) {
    pickUpLocation = pickUpAddress;
    notifyListeners();
  }

  void updateDropOffLocationAddress(Address dropOffAddress) {
    dropOffLocation = dropOffAddress;
    notifyListeners();
  }

  void updateTripsCounter(int tripCounter) {
    _countTrips = tripCounter;
    notifyListeners();
  }

  void updateTripKeys(List<String> newKeys) {
    tripHistoryKeys = newKeys;
    notifyListeners();
  }

  void updateTripKey(String newKey) {
    tripHistoryKeys.add(newKey);
    notifyListeners();
  }

  void updateTripHistoryData(HistoryModel eachHistory) {
    tripHistoryDataList.add(eachHistory);
    print('viajes: ' + tripHistoryDataList.length.toString());

    notifyListeners();
  }

  UserModel? getUserModel() {
    return userProvider.userModel;
    void resetAppData() {
      _countTrips = 0;
      tripHistoryKeys = [];
      tripHistoryDataList = [];
      notifyListeners();
    }
  }


}
